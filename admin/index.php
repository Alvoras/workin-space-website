<?php
// ACTIVATE ERRORE REPORTING WITHOUT MODIFYING PHP.INI
// error_reporting(E_ALL);
ini_set('display_errors', 0);

// Require config file
require_once "../private/config/conf.inc.php";

foreach ($config["libs"] as $script) {
    require_once '../'.$script;
}

foreach ($config["class"] as $script) {
    require_once '../'.$script;
}

$logger = new Logger();

// Instanciate the PDO object inside the Db class
$db = Db::getInstance();

// Init or resume session
$session = new Session();

$cookie = Cookie::getInstance();

// if there is a "p" parameter, store it into page, else display home page and don't compute the other parameters
if (count($_GET)) {
    // Check if there is no unexpected parameter
    foreach ($_GET as $key => $value) {
        try {
          Warden::isValidParameter($config, "get:$key");
        } catch (Exception $e) {
          $logger->log(LOG_WARNING, "[".$_SERVER["REMOTE_ADDR"]."] [GET:$key] Unexpected GET parameter");
          Path::adminDisplay($config, "admin-ohno");
        }
    }

  if (array_key_exists("p", $_GET)) {
    $page = strtolower($_GET["p"]);

    // Check for non-existent views
    try {
        Warden::isValidAdminView($config, $page);
    } catch (Exception $e) {
        $logger->log(LOG_INFO, "[".$_SERVER["REMOTE_ADDR"]."] [$page] Unexpected view queried");

        // Can be replaced for redirecting 404
        Path::adminDisplay($config, "admin-ohno");
    }

    if ($config["admin-views"][$page]["need-auth"] == true) {

      // If the user data are not found in the session
      if ( !(Warden::isConnected($db)) ) {
        $err = new Err("Veuillez vous reconnecter");
        $err->load();

        // Log ?
        $cookie->flush();
        $session->reset();
        header("Location: /p/admin-login");
      }
    }

    // MODEL QUERY FOR EVERY VIEWS HERE
    switch ($_GET["p"]) {
      case "admin-login":
        require_once INCLUDE_ADMIN_MODEL."/user/autoconnect.php";
        break;
      case "users":
        require_once INCLUDE_ADMIN_MODEL."/db/load_users.php";
        break;
      case "hardware":
        $_SESSION["asset_type"] = "hardware";

        require_once INCLUDE_ADMIN_MODEL."/db/fetch_site_data.php";
        require_once INCLUDE_ADMIN_MODEL."/db/load_assets.php";
        break;
      case "rooms":
        $_SESSION["asset_type"] = "room";

        require_once INCLUDE_ADMIN_MODEL."/db/fetch_site_data.php";
        require_once INCLUDE_ADMIN_MODEL."/db/load_assets.php";
        break;

    }

    // Require the view queried by the "p" get parameter
    Path::adminDisplay($config, $_GET["p"]);
  }

  if (array_key_exists("a", $_GET)) {
    $action = $_GET["a"];

    // Check if there is no unexpected action
    try {
      Warden::isValidAdminAction($config, $action);
    } catch (Exception $e) {
      $logger->log(LOG_INFO, "[".$_SERVER["REMOTE_ADDR"]."] [$action] Unexpected action parameter");

      Path::adminDisplay($config, "admin-ohno");
    }


    if (count($_POST)) {
      // Check if there is no unexpected POSTed parameter
      foreach ($_POST as $param => $val) {
        try {
          Warden::isValidParameter($config, "$action:$param");
        } catch (Exception $e) {
          $logger->log(LOG_WARNING, "[".$_SERVER["REMOTE_ADDR"]."] [$action:$param] Unexpected POST parameter");
          Path::adminDisplay($config, "admin-ohno");
        }
      }
    }


    require_once Path::format($config["actions"][$action]["path"]);
  }
}else{
  require_once INCLUDE_ADMIN_MODEL."/user/autoconnect.php";

  Path::adminDisplay($config, "admin-login");
}
