<?php
require_once "../script_toolset_include.php";

$REPUBLIQUE = 1;

$siteId = (empty($_POST))?$REPUBLIQUE:$_POST["siteId"];

// $yesStr = "Oui";
// $noStr = "Non";

$type = $_SESSION["asset_type"];

$includeDeleted = true;

$data = $db->getAssetsInSite($siteId, $includeDeleted, $type);

// foreach ($data as $key => $val) {
//   $data[$key]["is_deleted_str"] = ($val["is_deleted"] == 1)?$yesStr:$noStr;
// }

$data[0]["asset_type"] = $type;
$data[0]["view_mode"] = "general";

// dump($data);

echo json_encode($data);
