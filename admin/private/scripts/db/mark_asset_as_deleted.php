<?php
require_once "../script_toolset_include.php";

$ret = [];
$ret["success"] = false;

$idAsset = $_POST["assetId"];

$data = [];

if ($_POST["doDelete"] == "true") {
  $data["is_deleted"] = 1;
}else {
  $data["is_deleted"] = 0;
}

if ($db->update($data, "ASSETS", "id_asset = ?", [$idAsset])) {
  $ret["success"] = true;
  $ret["successMsg"] = "Produit bien mis à jour";
}else {
  $ret["errorMsg"] = "Échec lors de la mise à jour du produit";
}

echo json_encode($ret);
