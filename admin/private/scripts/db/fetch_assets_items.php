<?php
require_once "../script_toolset_include.php";

$siteId = $_POST["siteId"];

$yesStr = "Oui";
$noStr = "Non";

$type = $_SESSION["asset_type"];

$includeDeleted = true;

$data = $db->getAssetItemsInSite($siteId, $type);

foreach ($data as $key => $val) {
  $data[$key]["is_deleted_str"] = ($val["is_deleted"] == 1)?$yesStr:$noStr;
}

$data[0]["asset_type"] = $type;
$data[0]["view_mode"] = "items";

echo json_encode($data);
