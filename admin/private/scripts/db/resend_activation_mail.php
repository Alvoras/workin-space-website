<?php
require_once "../script_toolset_include.php";

$wrapper = new Wrapper();

$ret = [];
$ret["success"] = false;

$email = $_POST["email"];

$token = $wrapper->generateToken();

$data = [];
$data["token"] = $token;
$data["is_activated"] = 0;

if ($db->update($data, "USERS", "email = ?", [$email])) {
  $mail = new Mail($email);

  if ($mail->sendActivationMail($token, '')) {
    $ret["success"] = true;
    $ret["successMsg"] = "Mail d'activation envoyé avec succès";
  }else {
    $logger->log(LOG_INFO, "[$email] Failed to resend activation to $email");
    $ret["errorMsg"] = "Échec lors de l'envoi du mail d'activation";
  }
}else {
  $logger->log(LOG_INFO, "[Admin] Failed to update $email token");
  $ret["errorMsg"] = "Échec lors de la mise à jour de l'utilisateur. Veuillez réessayer";
}

echo json_encode($ret);
