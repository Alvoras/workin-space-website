<?php
require_once "../script_toolset_include.php";

$wrapper = new Wrapper();
$logger = new Logger();

$ret = [];
$ret["success"] = false;

$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$email = $_POST["email"];
$phone = $_POST["phone"];

// Generate temp password
// insert into db
// send mail

if( count($_POST) == 4
	&& !empty($_POST["firstname"])
	&& !empty($_POST["lastname"])
	&& !empty($_POST["email"])
	&& !empty($_POST["phone"])
	) {
	$firstname = trim($firstname);
	$lastname = trim($lastname);
	$email = trim($email);
	$phone = trim($phone);

	$firstname = htmlspecialchars($firstname);
	$lastname = htmlspecialchars($lastname);
	$email = htmlspecialchars($email);
	$phone = htmlspecialchars($phone);

  if( strlen($_POST["firstname"]) < 2 || strlen($_POST["firstname"]) >= 20){
    $ret["errorMsg"] = "Le nom doit faire entre 2 et 20 caractère";
    echo json_encode($ret);
    die();
		}

	if( strlen($_POST["lastname"]) < 2 || strlen($_POST["lastname"]) >= 20){
    $ret["errorMsg"] = "Le prénom doit faire entre 2 et 20 caractère";
    echo json_encode($ret);
    die();
	}

	if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) ){
    $ret["errorMsg"] = "L'email n'est pas valide";
    echo json_encode($ret);
    die();
	}

  if (!preg_match("#^0[1-68]([-. ]?[0-9]{2}){4}$#", $_POST['phone']))
  {
    $ret["errorMsg"] = "Le numéro de téléphone n'est pas valide";
    echo json_encode($ret);
    die();
  }

  //Check if this email doesn't already exists
	$req = $db->get("email", "USERS", "email = ?", [$email], Db::W_KEY);
	if(!empty($req)){
    $ret["errorMsg"] = "L'email est déjà utilisé par un autre utilisateur";
    echo json_encode($ret);
    die();
	}

  $tempPwd = $wrapper->generateTempPwd();

  $hPassword = $wrapper->wrapPwd($tempPwd);

  $token = $wrapper->generateToken();

  if (!$db->registerUser($hPassword, $lastname, $firstname, $email, $phone, $token)) {
    $ret["errorMsg"] = "Échec de l'insertion de l'utilisateur en base de donnée";
    echo json_encode($ret);
    die();
  }

  $mail = new Mail($email);
  if ($mail->sendActivationMailWithPwd($token, $firstname, $tempPwd)) {
		$_SESSION["location"] = "fp";
    $ret["success"] = true;
  }else {
    $logger->log(LOG_INFO, "[$email] Failed to send activation to $email");
    $ret["errorMsg"] = "Erreur lors de l'envoi du mail";
    echo json_encode($ret);
    die();
  }

  $ret["successMsg"] = "L'utilisateur a été inscrit et un mail d'activation lui a été envoyé";
  echo json_encode($ret);
  die();
}else {
  $logger->log(LOG_WARNING, "Formulaire corrompu");
  $ret["errorMsg"] = "Formulaire corrompu";
  echo json_encode($ret);
  die();
}

echo json_encode($ret);
