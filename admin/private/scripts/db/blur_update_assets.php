<?php
require_once "../script_toolset_include.php";
$ret = [];
$ret["success"] = false;

$assetType = $_SESSION["asset_type"];
$assetName = $_POST["assetName"];
$type = $_POST["type"];
$content = $_POST["content"];
$data = [];

$data[$type] = $content;

$table = ($assetType === "room")?"ROOM":"HARDWARE";

if ($db->update($data, $table, "name = ?", [$assetName])) {
  $ret["success"] = true;
  $ret["successMsg"] = "Produits bien mis à jour";
}else {
  $ret["errorMsg"] = "Échec lors de la mise à jour des produits";
}

echo json_encode($ret);
