<?php
require_once "../script_toolset_include.php";

$data = $_POST["query"];

$yesStr = "Oui";
$noStr = "Non";

// Si pas de & présent
// mode == email
// création d'un tableau email => $data
// sinon
// création d'un tableau foreach
// avec &xxx comme $key => $val

if (strpos($data, "&") === false) {
  $searchData = ["email" => $data];
}else if ($data === "&all") {
  $searchData = "&all";
}else {
  $searchData = [];
  $bufExploded = [];
  $exploded = explode('&', $data);
  foreach ($exploded as $key => $val) {
    $bufExploded[] = rtrim($val);
  }

  $bufExploded = array_filter($bufExploded);

  $exploded = [];

  foreach ($bufExploded as $key => $val) {
    $exploded = explode(' ', $val);
    $searchData[$exploded[0]] = $exploded[1];
  }
}

$userData = $db->searchUser($searchData);

foreach ($userData as $key => $val) {
  $subData = $db->getUserSubscriptionData($userData[$key]["email"]);

  $userData[$key]["sub_name"] = 'N/A';
  if (count($subData) > 0) {
    $subPrefix = ($subData[0]["is_commited"])?'*':'';
    $userData[$key]["sub_name"] = $subPrefix.$subData[0]["subscription_name"];
  }
  $userData[$key]["is_deleted_str"] = ($val["is_deleted"] == 1)?$yesStr:$noStr;
  $userData[$key]["is_activated_str"] = ($val["is_activated"] == 0)?$noStr:$yesStr;
}

echo json_encode($userData);
