<?php
require_once "../script_toolset_include.php";

$name = $_POST["name"];
$description = $_POST["description"];
$price = $_POST["price"];
$img = $_POST["img"];

if (count($_POST) == 4
  && !empty($_POST["name"])
  && !empty($_POST["description"])
  && !empty($_POST["price"])
  && !empty($_POST["img"])
  ) {
    $type = $_SESSION["asset_type"];

    $ret = [];
    $ret["success"] = false;

    if ($db->addAssetCategory($type, $name, $description, $price, $img)) {
      $ret["success"] = true;
      $ret["successMsg"] = "Nouvelle catégorie ajoutée avec succès";
    }else {
      $ret["errorMsg"] = "Échec lors de l'insertion en base de donnée de la nouvelle catégorie";
    }

    echo json_encode($ret);
    die();
}else {
  $ret["errorMsg"] = "Tous les champs sont requis";
  echo json_encode($ret);
  die();
}
