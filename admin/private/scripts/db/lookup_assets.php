<?php
require_once "../script_toolset_include.php";

$data = $_POST["query"];
$siteId = $_POST["siteId"];

$type = $_SESSION["asset_type"];

$yesStr = "Oui";
$noStr = "Non";

// Si pas de & présent
// mode == email
// création d'un tableau email => $data
// sinon
// création d'un tableau foreach
// avec &xxx comme $key => $val

if (strpos($data, "&") === false) {
  $searchData = [$type."_name" => $data];
}else if ($data === "&all") {
  $searchData = "&all";
}else {
  $searchData = [];
  $bufExploded = [];
  $exploded = explode('&', $data);
  foreach ($exploded as $key => $val) {
    $bufExploded[] = rtrim($val);
  }

  $bufExploded = array_filter($bufExploded);

  $exploded = [];

  foreach ($bufExploded as $key => $val) {
    $exploded = explode(' ', $val);
    $searchData[$exploded[0]] = $exploded[1];
  }
}

$assetsData = $db->searchAssets($searchData, $type, $siteId);

$assetsData[0]["ajax"] = true;
$assetsData[0]["asset_type"] = $type;
$assetsData[0]["view_mode"] = "items";

foreach ($assetsData as $key => $val) {
  $assetsData[$key]["is_deleted_str"] = ($val["is_deleted"] == 1)?$yesStr:$noStr;
}

echo json_encode($assetsData);
