<?php
require_once "../script_toolset_include.php";

$dFormatter = new DateFormatter();

$siteData = $db->getSitesData();

$dates = $dFormatter->getLast7DaysInterval();

$start = $dates[0]->format("Y-m-d H:i:s");
$end = $dates[1]->format("Y-m-d H:i:s");

$visits = $db->loadSitesVisits($start, $end);

$data = [];

foreach ($siteData as $key => $val) {
  $data[$siteData[$key]["id_site"]-1]["site_name"] = $val["site_name"];
  $data[$key]["qty"] = 0;
}

foreach ($visits as $key => $val) {
  $data[$val["id_site"]-1]["qty"] = intval($val["qty"]);
}

echo json_encode($data);
