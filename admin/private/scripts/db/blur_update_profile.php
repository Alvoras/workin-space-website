<?php
require_once "../script_toolset_include.php";
$ret = [];
$ret["success"] = false;

$email = $_POST["email"];
$type = $_POST["type"];
$content = $_POST["content"];
$data = [];
$data[$type] = $content;

if ($db->update($data, "USERS", "email = ?", [$email])) {
  $ret["success"] = true;
  $ret["successMsg"] = "Profil de l'utilisateur bien mis à jour";
}else {
  $ret["errorMsg"] = "Échec lors de la mise à jour du profil de l'utilisateur";
}

echo json_encode($ret);
