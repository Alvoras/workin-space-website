<?php
require_once "../script_toolset_include.php";

$ret = [];
$ret["success"] = false;

$email = $_POST["email"];

$data = [];

if ($_POST["doDelete"] == "true") {
  $data["is_deleted"] = 1;
}else {
  $data["is_deleted"] = 0;
}

if ($db->update($data, "USERS", "email = ?", [$email])) {
  $ret["success"] = true;
  $ret["successMsg"] = "Profil de l'utilisateur bien mis à jour";
}else {
  $ret["errorMsg"] = "Échec lors de la mise à jour du profil de l'utilisateur";
}

echo json_encode($ret);
