<?php
require_once "../script_toolset_include.php";

$dFormatter = new DateFormatter();

$siteId = $_POST["siteId"];
$type = $_SESSION["asset_type"];

$assetsData = $db->getAvailableAssetsInSite($siteId, $type);

$dates = $dFormatter->getLast7DaysInterval();

$start = $dates[0]->format("Y-m-d H:i:s");
$end = $dates[1]->format("Y-m-d H:i:s");

$rentalsData = $db->getSiteRentalCount($siteId, $type, $start, $end);

$data = [];

echo json_encode($rentalsData);
