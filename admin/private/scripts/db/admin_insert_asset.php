<?php
require_once "../script_toolset_include.php";

$ret["success"] = false;
$qty = $_POST["qty"];
$name = $_POST["name"];
$siteId = $_POST["siteId"];
$siteName = $_POST["siteName"];

$type = $_SESSION["asset_type"];

if( count($_POST) == 4
	&& !empty($_POST["qty"])
	&& !empty($_POST["name"])
	&& !empty($_POST["siteId"])
	&& !empty($_POST["siteName"])
) {
    for ($i=0; $i < $qty; $i++) {
      if(!$db->addAsset($siteId, $name, $type)){
        $ret["errorMsg"] = "L'insertion a dû s'arrêter à la boucle n°$i";
        break;
      }
    }

    if (!isset($ret["errorMsg"])) {
      $ret["successMsg"] = "$qty '$name' ont bien été entrés en base de donnée";
      $ret["success"] = true;

      $ret["data"] = $db->getInfo($name, $type);
    }

    echo json_encode($ret, JSON_FORCE_OBJECT);
}else {
  $ret["errorMsg"] = "Tous les champs sont requis";
  echo json_encode($ret);
  die();
}
