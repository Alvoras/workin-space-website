<?php
require_once __DIR__."/../../../private/config/conf.inc.php";

foreach ($config["libs"] as $script) {
    require_once __DIR__."/../../../$script";
}

foreach ($config["class"] as $class) {
    require_once __DIR__."/../../../$class";
}

$logger = new Logger();
$db = Db::getInstance();
$session = new Session();
$user = $session->loadUser();
