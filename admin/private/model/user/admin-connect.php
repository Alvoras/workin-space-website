<?php
require_once INCLUDE_MODEL."/model_toolset_include.php";

$dFormatter = new DateFormatter();
$wrapper = new Wrapper();
$cookie = Cookie::getInstance();
$errorData = [];
$email = $_POST["email"];

// We need to allow the checked box if it is set
if ( (count($_POST) === 2 ||count($_POST) === 3)
	&& !empty( $_POST ["email"] )
	&& !empty( $_POST ["password"] )
) {
  $isAccueil = $db->connectStaff($email, $_POST["password"], 'Accueil');

  if (!$isAccueil) {
    $isManager = $db->connectStaff($email, $_POST["password"], 'Manager');
  }

  if ($isAccueil || $isManager) {
    $data = $db->fetchUserInfo($email);

    $user = new User($data);

    // if $_POST["remember_me"] is set (which means that the box is cheked)
    // Then insert validator, selector and expire time in db
    if (isset($_POST["remember_me"])) {
			if (!$cookie->generateRememberMe($email)) {
		    $logger->log(LOG_ERROR, "[$email] Failed to generate remember me cookie");

				$err = new Err("Veuillez réessayer");
				$err->load();
				header('Location: /p/admin-login');
				die();
			}
    }

    //$session->append($data);
    $session->uploadUser($user);

    header('Location: /p/users');
    die();
	} else {
			$err = new Err("Identifiants invalides");
			$err->load();

			header('Location: /p/admin-login');
			die();
		}
} else {
	$err = new Err("Veuillez réessayer");
	$err->load();
	$logger->log(LOG_WARNING, "[$email] Bad connection form info");

	header('Location: /p/admin-login');
	die();
}

$logger->log(LOG_WARNING, "[$email] Unexpected redirection failure");

header('Location: /p/admin-login');
die();
