<?php
// Called when and url containing ?a=verify&token=ab1cd23f4&email=email@gmail.com
// is resolved
//
// Test the GET token against the token associated with the account using $_GET["email"]
//
// Set the SESSION["verify_token"] accordingly and redirect to ?p=fp

require_once INCLUDE_ADMIN_MODEL."/model_toolset_include.php";

$skip = false;

$token = $_GET["token"];
$email = $_GET["email"];

$error = [];

if ($db->evalToken($email, $token)) {
  $_SESSION["email"] = $email;

  $_SESSION["verify_token"] = true;
}else {
  $err = new Err("Mauvais token de validation");
  $err->load();
}

header("Location: /p/admin-login");
die();
