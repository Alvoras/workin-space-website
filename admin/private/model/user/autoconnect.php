<?php
require_once INCLUDE_ADMIN_MODEL."/model_toolset_include.php";

// Look for a remember me cookie
if ($cookie->isRemMeSet()) {
  // Validate the remember me cookie by looking up the database auth info
  if (Warden::validateRememberMe($db)) {
    // If the info are valids, then load up the user and redirect him to back end
    $selector = $cookie->getSelector();
    $email = $db->getEmailFromSelector($selector);

    $data = $db->fetchUserInfo($email);

    $user = new User($data);

    $session->uploadUser($user);

    header('Location: /p/users');
    die();
  }else {
    $logger->log(LOG_WARNING, "[$email] Corrupted remember me");

    $cookie->flush();
    $session->reset();
  }
}
