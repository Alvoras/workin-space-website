<?php
require_once INCLUDE_ADMIN_MODEL."/model_toolset_include.php";

$REPUBLIQUE = 1;

$yesStr = "Oui";
$noStr = "Non";

$type = $_SESSION["asset_type"];

$includeDeleted = true;

$data = $db->getAssetsInSite($REPUBLIQUE, $includeDeleted, $type);

foreach ($data as $key => $val) {
  $data[$key]["is_deleted_str"] = ($val["is_deleted"] == 1)?$yesStr:$noStr;
}

$_SESSION["assets_data"] = $data;
