<?php
require_once INCLUDE_ADMIN_MODEL."/model_toolset_include.php";

$userData = $db->loadUsers();

$yesStr = "Oui";
$noStr = "Non";

foreach ($userData as $key => $val) {
  $subData = $db->getUserSubscriptionData($userData[$key]["email"]);

  $userData[$key]["sub_name"] = 'N/A';
  if (count($subData) > 0) {
    $subPrefix = ($subData[0]["is_commited"])?'*':'';
    $userData[$key]["sub_name"] = $subPrefix.$subData[0]["subscription_name"];
  }
  $userData[$key]["is_deleted_str"] = ($val["is_deleted"] == 1)?$yesStr:$noStr;
  $userData[$key]["is_activated_str"] = ($val["is_activated"] == 0)?$noStr:$yesStr;
}

$_SESSION["user_data"] = $userData;
