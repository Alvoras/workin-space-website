function drawChart() {
  let script = "private/scripts/db/load_site_visits.php";
  let params = {
    script,
    async: false
  };
  let visits = ajaxFetchPHP(params);
  console.log(visits);
  visits = JSON.parse(visits);
  console.log(visits);

  let qty = [];
  let data = [];
  let days = [];


  for (val of visits) {
    qty.push(val["qty"]);
    data.push(val["site_name"]);
  }

  console.log(qty, data);

  let ctx = document.getElementById("freq-chart").getContext('2d');

  // let gradient = ctx.createLinearGradient(0, 0, 600, 600);
  // gradient.addColorStop(0, '#ffb88c');
  // gradient.addColorStop(1, '#ea758f');

  let myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: data,
          datasets: [{
              label: 'Nombre de visite sur les 7 derniers jours',
              data: qty,
              backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"]
              // borderWidth: 2,
              // borderColor: gradient,
              // fill: false
          }]
      },
      options: {
          maintainAspectRatio: false,
          scales: {
             yAxes: [{
                 ticks: {
                     beginAtZero: true,
                     userCallback: function(label, index, labels) {
                         // Return whole numbers
                         if (Math.floor(label) === label) {
                             return label;
                         }

                     },
                 }
             }],
         }
      }

  });
}
