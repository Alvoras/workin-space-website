function fetchUserEmail(el){
  let row = el.closest(".user-row");

  return $(row).data("email");
}

function updateUserProfile(data) {
  let script = "private/scripts/db/blur_update_profile.php";


  let notifyResult = (res) => {
    notify(res);
  };

  let params = {
    data,
    async: true,
    script,
    success: notifyResult,
    failure: notifyResult
  };

  ajaxExec(params);
}

function checkAddUserForm(formData) {
  let errorList = [];

  if (!validateInput(formData.lastname, "lastname")) {
    errorList.push("Le nom de famille n'est pas valide");
  }

  if (!validateInput(formData.firstname, "firstname")) {
    errorList.push("Le prénom n'est pas valide");
  }

  if (!validateInput(formData.email, "email")) {
    errorList.push("L'email n'est pas valide");
  }

  if (!validateInput(formData.phone, "phone")) {
    errorList.push("Le numéro de téléphone n'est pas valide");
  }

  return errorList;
}
