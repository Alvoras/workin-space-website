document._data = {};

$(document).ready(function(){
  drawChart();

  $("#users-listing-container").on("click", ".user-action-delete", function() {
    let that = $(this);
    let script = "private/scripts/db/mark_as_deleted.php";

    let email = fetchUserEmail(that);

    document._data.email = email;

    let doDelete = fetchDeleteState(that);

    let data = {
      email,
      doDelete
    };

    let notifyResult = (res) => {
      notify(res);
    };

    let successCb = (res) => {
      notify(res);

      let deletedCell = $("#users-listing-container").find(`.user-row[data-email="${document._data.email}"] .delete-cell .delete-cell-text`);

      if (deletedCell.html() === "Oui") {
        deletedCell.html("Non");
      }else {
        deletedCell.html("Oui");
      }
    }

    let params = {
      data,
      async: true,
      script,
      success: successCb,
      failure: notifyResult
    };

    ajaxExec(params);
  });


  $("#users-listing-container").on("click", ".reset-cell", function() {
    let that = $(this);
    let script = "private/scripts/db/resend_activation_mail.php";

    let email = fetchUserEmail(that);

    document._data.email = email;

    let doDelete = fetchDeleteState(that);

    let data = {
      email,
      doDelete
    };

    let notifyResult = (res) => {
      notify(res);
    };

    let successCb = (res) => {
      notify(res);

      let deletedCell = $("#users-listing-container").find(`.user-row[data-email="${document._data.email}"] .delete-cell .delete-cell-text`);

      if (deletedCell.html() === "Oui") {
        deletedCell.html("Non");
      }else {
        deletedCell.html("Oui");
      }
    }

    let params = {
      data,
      async: true,
      script,
      success: successCb,
      failure: notifyResult
    };

    ajaxExec(params);
  });

  $("#users-listing-container").on("focus", 'td[contenteditable="true"]', function(){
    let that = $(this);
    document._data.type = that.data("type");
    document._data.lastContent = that.html();
  });

  $("#user-add-button").click(function() {
    let formData = fetchInputVals($(".add-user-form-input"));

    let errorList = checkAddUserForm(formData);

    if (errorList.length > 0) {
      for (errorMsg of errorList) {
        $.growl.error({message: errorMsg});
      }

      return;
    }

    let script = "private/scripts/db/admin_insert_user.php";

    let data = {
      firstname: formData.firstname,
      lastname: formData.lastname,
      email: formData.email,
      phone: formData.phone
    };

    let notifyResult = (res) => {
      notify(res);
    };

    let successCb = (res) => {
      notify(res);

      let data = document._data.formData;

      let el = $(".user-row-dummy").clone().insertAfter("#head-row");
      let $el = $(el);

      $el.attr("class", "user-row highlight");
      setTimeout( () => {
        $el.removeClass("highlight");
      }, 200);
      $el.data("email", data.email);
      $el.find('td[data-type="firstname"]').html(data.firstname);
      $el.find('td[data-type="lastname"]').html(data.lastname);
      $el.find('td[data-type="email"]').html(data.email);
      $el.find('td[data-type="phone"]').html(data.phone);

      $(".add-user-form-input").val("");
    }

    document._data.formData = formData;

    let params = {
      data,
      async: true,
      script,
      success: successCb,
      failure: notifyResult
    };

    ajaxExec(params);
  });

  $("#users-listing-container").on("blur", 'td[contenteditable="true"]', function(){
    let that = $(this);
    let content = that.html();
    // If no change, pass
    if (document._data.lastContent === content) {
      return;
    }

    let type = that.data("type");
    let email = fetchUserEmail(that);

    let data = {
      type,
      content,
      email
    };

    updateUserProfile(data);
  });

  $("#search-button").click(function() {
    let query  = $("#search-bar").val();

    if (query.length < 1) return;

    let script = "private/scripts/db/lookup.php";

    let data = {
      query
    };

    let cb = (res) => {
      let data = JSON.parse(res);
      let mergedData = {};

      for (var i = 0; i < data.length; i++) {
        mergedData[i] = data[i]
        console.log(mergedData[i]);
      }

      let finalJson = JSON.parse(JSON.stringify(mergedData));

      let params = {
        userData: finalJson,
        ajax: true
      };

      let script = "public/views/include/users/listing-table.php";
      let parent = "#listing-table";

      ajaxRefreshElement(script, parent, params);
    }

    let params = {
      data,
      async: true,
      script,
      callback: cb
    };

    ajaxFetchPHP(params);
  });
});
