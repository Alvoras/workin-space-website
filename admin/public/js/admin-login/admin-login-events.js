$(document).ready(function() {
  displayErrors();

  $("#login-button").click(function() {
    let mode = "login";
    let data = fetchInputVals($(".login-form-input"));
    data["remember_me"] = $("#remember-me").is(":checked");

    let errorList = checkLoginForm(data, mode);

    if (errorList.length > 0) {
      for (errMsg of errorList) {
        $.growl.error({message:errMsg});
      }
      return;
    }

    let loginForm = document.getElementById('login-form');
    loginForm.submit();
  });
});
