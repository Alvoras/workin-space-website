function drawChart(siteId) {
  let script = "private/scripts/db/load_rentals_stat.php";

  let viewMode = fetchViewMode();

  console.log(siteId);

  let data = {
    siteId,
    viewMode
  }

  let params = {
    script,
    data,
    async: false
  };

  let rentals = ajaxFetchPHP(params);

  console.log(rentals);

  rentals = JSON.parse(rentals);

  let qty = [];
  data = [];

  for (val of rentals) {
    qty.push(val["qty"]);
    data.push(val["name"]);
  }

  let ctx = document.getElementById("freq-chart").getContext('2d');

  // let gradient = ctx.createLinearGradient(0, 0, 600, 600);
  // gradient.addColorStop(0, '#ffb88c');
  // gradient.addColorStop(1, '#ea758f');

  Chart.pluginService.register({
    afterDraw: function (chart) {
                    if (chart.data.datasets[0].data.length === 0) {
                        // No data is present
                        var ctx = chart.chart.ctx;
                        var width = chart.chart.width;
                        var height = chart.chart.height
                        chart.clear();

                        ctx.save();
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        ctx.font = "20px normal 'Helvetica Nueue'";
                        ctx.fillText('Aucune réservations trouvées ces 7 derniers jours', width / 2, height / 2);
                        ctx.restore();
                    }

                }
  });

  let myChart = new Chart(ctx, {
      type: 'pie',
      data: {
          labels: data,
          datasets: [{
              label: 'Nombre de produit loués sur les 7 derniers jours',
              data: qty,
              backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"]
              // borderWidth: 2,
              // borderColor: gradient,
              // fill: false
          }]
      },
      options: {
          responsive: true,
          maintainAspectRatio: false
      },
  });
}
