function updateAssets(data) {
  let script = "private/scripts/db/blur_update_assets.php";


  let notifyResult = (res) => {
    notify(res);
  };

  let params = {
    data,
    async: true,
    script,
    success: notifyResult,
    failure: notifyResult
  };

  ajaxExec(params);
}

function hasEmpty(data) {
  let ret = false;

  jQuery.each(data, function(key) {
    if(data[key].trim().length < 1){
      ret = true;
    }
  });

  return ret;
}

function fetchAssetName(el) {
  let row = el.closest(".asset-row");

  return $(row).data("asset-name");
}

function fetchAssetMode() {
  return $("#listing-table").data("type");
}

function fetchViewMode() {
  return $("#switch-view-container").data("view-mode");
}

function fetchSiteId() {
  return $(".selected-header-name").data("siteid");
}

function fetchAssetId(el) {
  let row = el.closest(".asset-row");

  return $(row).data("id-asset");
}

function fetchSiteIds() {
  let mapped = $(".site-header-name").map((key, val) => $(val).data("siteid"));

  let array = [];

  for (id of mapped) {
    array.push(id);
  }

  return array;
}

function fetchSiteNamesAndId() {
  let elements = $(".site-header-name");

  let ret = [];

  for (let i = 0; i < elements.length; i++) {
    ret[i] = [];
    ret[i].push($(elements[i]).data("site-name"));
    ret[i].push($(elements[i]).data("siteid"));
  }

  return ret;
}

function fetchAvailableAssetsNames() {
  let rows = $(".asset-row");

  let assetsName = rows.map((key, val) => $(val).data("asset-name"));

  // Spread operator + ES6's Set object (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set)
  // https://stackoverflow.com/questions/1960473/get-all-unique-values-in-an-array-remove-duplicates
  let parsedUnique = [...new Set(assetsName)];


  return parsedUnique;
}
