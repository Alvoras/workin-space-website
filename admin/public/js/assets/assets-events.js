document._data = {};

$(document).ready(function() {
  drawChart(1);

  $("#switch-view-button").click(function () {
    let that = $(this);
    let siteId = fetchSiteId();

    let viewMode = fetchViewMode();

    let newViewMode = (viewMode === "general")?"items":"general";

    let script = "";

    if (newViewMode === "general") {
      script = "private/scripts/db/fetch_assets_categories.php";
    }else {
      script = "private/scripts/db/fetch_assets_items.php";
    }

    let data = {
      siteId
    };

    let cb = (res) => {
      let data = JSON.parse(res);

      let params = {
        assetsData: data,
        ajax: true
      };

      let script = "public/views/include/assets/assets-listing.php";
      let parent = "#listing-table";

      let viewMode = fetchViewMode();

      let newViewMode = (viewMode === "general")?"items":"general";

      let newViewTitle = (newViewMode === "general")?"Vue d'ensemble":"Item par item";

      let oldViewTheme = (newViewMode === "general")?"white":"black";
      let newViewTheme = (oldViewTheme === "white")?"black":"white";

      // Update mode
      $("#switch-view-container").data("view-mode", newViewMode);

      let viewModeButton = $("#switch-view-button");
      viewModeButton.html(newViewTitle);
      viewModeButton.removeClass(`view-${oldViewTheme}-theme`);
      viewModeButton.addClass(`view-${newViewTheme}-theme`);

      ajaxRefreshElement(script, parent, params);

      let assetType = fetchAssetMode();

      let siteNamesAndId = fetchSiteNamesAndId();
      // let sitesId = fetchSiteIds();

      let assetNames = fetchAvailableAssetsNames();

      params = {
        data: {
          assetType,
          siteNamesAndId,
          assetNames
        },
        ajax: true
      };

      script = (newViewMode === "general")?"public/views/include/assets/add-asset-category-form.php":"public/views/include/assets/add-asset-item-form.php";
      parent = "#assets-listing-header";

      ajaxRefreshElement(script, parent, params);
    }

    let params = {
      data,
      async: true,
      script,
      callback: cb
    };

    ajaxFetchPHP(params);
  });

  $(".site-header-name").click(function() {
    let that = $(this);
    let siteId = that.data("siteid");

    if (typeof siteId === 'undefined') return;

    let viewMode = fetchViewMode();

    let script = "";

    if (viewMode === "general") {
      script = "private/scripts/db/fetch_assets_categories.php";
    }else {
      script = "private/scripts/db/fetch_assets_items.php";
    }

    let data = {
      siteId
    };

    let cb = (res) => {
      let data = JSON.parse(res);

      let siteId = document._data.lastSiteId;

      let params = {
        assetsData: data,
        ajax: true
      };

      let script = "public/views/include/assets/assets-listing.php";
      let parent = "#listing-table";

      $(".selected-header-name").removeClass("selected-header-name");
      $(`.site-header-name[data-siteid="${siteId}"]`).addClass("selected-header-name");

      drawChart(siteId);

      ajaxRefreshElement(script, parent, params);
    }

    document._data.lastSiteId = siteId;

    let params = {
      data,
      async: true,
      script,
      callback: cb
    };

    ajaxFetchPHP(params);
  });

  $("#assets-listing-container").on("click", ".asset-action-delete", function () {
    let that = $(this);
    let script = "private/scripts/db/mark_asset_as_deleted.php";

    let assetId = fetchAssetId(that);

    document._data.assetId = assetId;

    let doDelete = fetchDeleteState(that);

    let data = {
      assetId,
      doDelete
    };

    let notifyResult = (res) => {
      notify(res);
    };

    let successCb = (res) => {
      notify(res);

      let deletedCell = $("#assets-listing-container").find(`.asset-row[data-id-asset="${document._data.assetId}"] .delete-cell .delete-cell-text`);

      if (deletedCell.html() === "Oui") {
        deletedCell.html("Non");
      }else {
        deletedCell.html("Oui");
      }
    }

    let params = {
      data,
      async: true,
      script,
      success: successCb,
      failure: notifyResult
    };

    ajaxExec(params);
  });

  $("#assets-listing-container").on("focus", 'td[contenteditable="true"]', function(){
    let that = $(this);
    document._data.type = that.data("type");
    document._data.lastContent = that.html();
  });

  $("#assets-listing-container").on("blur", 'td[contenteditable="true"]', function(){
    let that = $(this);
    let content = that.html();
    // If no change, pass
    if (document._data.lastContent === content) {
      return;
    }

    let type = that.data("type");
    let assetName = fetchAssetName(that);

    let data = {
      type,
      content,
      assetName
    };

    updateAssets(data);
  });

  $("#assets-listing-container").on("click", "#asset-item-add-button", function(){
    let script = "private/scripts/db/admin_insert_asset.php";

    let formData = fetchInputVals($(".add-asset-form-input"));

    if (hasEmpty(formData)) {
      $.growl.error({message: "Tous les champs doivent être remplis"})
      return;
    }

    [formData.siteName, formData.siteId] = formData.siteData.split(':');

    let siteId = $(`.header-site-name:contains('${formData.siteName}')`).data("siteid");

    let data = {
      name: formData.name,
      siteId: formData.siteId,
      siteName: formData.siteName,
      qty: formData.qty
    };

    let notifyResult = (res) => {
      notify(res);
    };

    let successCb = (res) => {
      notify(res);

      let el = $(".asset-item-row-dummy").clone().insertAfter("#head-row");
      let $el = $(el);

      let data = document._data.formData;
      data.item = res.data;

      $el.attr("class", "asset-row");
      // $el.data("asset-name", data.name);
      // $el.data("id-asset", data.item.id_asset);
      $el.attr({
        "data-asset-name": data.name,
        "data-id-asset": data.item.id_asset
      })
      $el.find('td[data-type="name"]').html(data.name);
      $el.find('td[data-type="id"]').html(data.item.id_asset);
      $el.find('td[data-type="description"]').html(data.item.description);
      $el.find('td[data-type="price"]').html(data.item.price);
      $el.find('td[data-type="img"]').html(data.item.img);

      if (typeof data.item.seats !== 'undefined') {
        $el.find('td[data-type="seats"]').html(data.item.seats);
      }

      $el.removeClass("asset-item-row-dummy");

      if (data.qty > 1){
        location.reload();
      }
    }

    document._data.formData = formData;

    let params = {
      data,
      async: true,
      script,
      success: successCb,
      failure: notifyResult
    };

    ajaxExec(params);
  });

  $("#assets-listing-container").on("click", "#asset-category-add-button", function(){
    let script = "private/scripts/db/add_asset_category.php";

    let formData = fetchInputVals($(".add-asset-form-input"));

    if (hasEmpty(formData)) {
      $.growl.error({message: "Tous les champs doivent être remplis"})
      return;
    }

    let data = {
      name: formData.name,
      description: formData.description,
      price: formData.price,
      img: formData.img
    };

    if (typeof formData.seats !== 'undefined') {
      data.seats = formData.seats;
    }

    let notifyResult = (res) => {
      notify(res);
    };

    let successCb = (res) => {
      notify(res);

      let data = document._data.formData;

      let el = $(".asset-category-row-dummy").clone().insertAfter("#head-row");
      let $el = $(el);

      $el.attr("class", "asset-row");
      $el.data("asset-name", data.name);
      $el.data("id-asset", res.id);
      $el.find('td[data-type="name"]').html(data.name);
      $el.find('td[data-type="description"]').html(data.description);
      $el.find('td[data-type="price"]').html(data.price);
      $el.find('td[data-type="img"]').html(data.img);

      if (typeof data.seats !== 'undefined') {
        $el.find('td[data-type="seats"]').html(data.seats);
      }

      $(".add-asset-form-input").val("");
    }

    document._data.formData = formData;

    let params = {
      data,
      async: true,
      script,
      success: successCb,
      failure: notifyResult
    };

    ajaxExec(params);
  });

  $("#search-button").click(function() {
    let query  = $("#search-bar").val();

    if (query.length < 1) return;

    let script = "private/scripts/db/lookup_assets.php";

    let siteId = fetchSiteId();

    let data = {
      query,
      siteId
    };

    let cb = (res) => {
      let data = JSON.parse(res);
      let mergedData = {};

      for (var i = 0; i < data.length; i++) {
        mergedData[i] = data[i]
      }

      let finalJson = JSON.parse(JSON.stringify(mergedData));

      let params = {
        assetsData: finalJson,
        ajax: true
      };

      let script = "public/views/include/assets/assets-listing.php";
      let parent = "#listing-table";

      ajaxRefreshElement(script, parent, params);
    }

    let params = {
      data,
      async: true,
      script,
      callback: cb
    };

    ajaxFetchPHP(params);
  });
});
