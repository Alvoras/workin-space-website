$(document).ready(function() {
  $("#search-bar").keydown(function(e) {
    if (e.keyCode === 13) {
      $("#search-button").trigger("click");
    }
  });

});
