<?php require_once INCLUDE_ADMIN_VIEWS.'/global/head.php' ?>
<link rel="stylesheet" href="<?=INCLUDE_ADMIN_CSS?>/admin-login.css"/>
</head>
<body>
  <?php
    
   ?>
  <?php require_once __DIR__."/../../../private/scripts/login/load_errors.php" ?>
  <div id="main-container" class="flex-center-col">
    <div id="login-form-container" class="form-container flex-center-col">
      <div id="logo-container" class="flex-center-col">
        <img src="public/img/logo/ws_logo_gradient.png" id="ws-logo" alt="logo">
        <h2 id="login-title">
          Bienvenue
        </h2>
      </div>
      <form id="login-form" class="form flex-center-col" action="/a/admin-connect" method="POST">
        <div id="login-email-container" class="text-input-container flex-center-col">
          <input class="text-input login-form-input" type="text" name="email" placeholder="Email" value="">
        </div>

        <div id="login-password-container" class="text-input-container flex-center-col">
          <input class="text-input login-form-input" type="password" placeholder="Mot de passe" name="password" value="">
        </div>

        <div id="remember-me-container">
          <input type="checkbox" id="remember-me" class="login-form-input" name="remember_me" value=''>
          <label for="remember-me" id="remember-me-label">Se souvenir de moi</label>
        </div>

        <div id="login-button-container">
          <input id="login-button" class="form-button" type="submit" name="" value="Connexion">
          <div class="button-underline"></div>
        </div>

        <div id="forgot-password-container">
          <a href="/p/admin-fp" id="forgot-password-text">Mot de passe oublié ?</a>
        </div>
      </form>
    </div>
  </div>
<?php require_once INCLUDE_ADMIN_VIEWS."/global/scripts.php" ?>
<script src="<?=INCLUDE_ADMIN_JS?>/admin-login/admin-login-events.js"></script>
<script src="<?=INCLUDE_ADMIN_JS?>/admin-login/admin-login-animation.js"></script>
<?php require_once '../'.INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
