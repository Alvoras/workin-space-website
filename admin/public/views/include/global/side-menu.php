<div id="side-menu-container">
    <div id="side-menu-list-container">
        <ul id="side-menu-list" class="flex-center-col">
            <li class="side-menu-tab flex-center-col clickable" title="Utilisateurs"><a class="flex-center-col side-menu-link" data-page="Utilisateurs" href="/p/users"><i class="fas fa-users"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Salles"><a class="flex-center-col side-menu-link" data-page="Salles" href="/p/rooms"><i class="far fa-handshake"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Matériel"><a class="flex-center-col side-menu-link" data-page="Matériel" href="/p/hardware"><i class="fas fa-laptop"></i></a>
            <li id="side-menu-disconnect" class="side-menu-tab flex-center-col" title="Se déconnecter"><a class="flex-center-col side-menu-link" data-page="Déconnexion" href="/a/disconnect"><i class="fas fa-sign-out-alt"></i></a>
        </ul>
    </div>
</div>
<div id="side-menu-handle-container" class="flex-center-col display-side-menu-handle">
  <div class="side-menu-handle"></div>
  <div class="side-menu-handle"></div>
</div>
