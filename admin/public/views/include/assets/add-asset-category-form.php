<?php
  if (!empty($_POST)) {
    $assetType = $_POST["data"]["assetType"];
  }else {
    $assetType = $_SESSION["asset_type"];
  }
?>
<form id="add-asset-form" class="flex-center-row">
  <input class="add-asset-form-input" required="" type="text" name="name" value="" placeholder="Nom">
  <input class="add-asset-form-input" required="" type="text" name="description" value="" placeholder="Description">
  <?php
    if ($assetType === "room") {
      ?>
      <input class="add-asset-form-input" required="" type="number" name="seats" value="" placeholder="Capacité">
      <?php
    }
  ?>
  <input class="add-asset-form-input" required="" type="number" name="price" value="" placeholder="Prix à l'heure">
  <input class="add-asset-form-input" required="" type="text" name="img" value="" placeholder="Image">
  <span class="clickable asset-add-button" id="asset-category-add-button">
    <img class="plus-icon" src="https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/2x/btw_ic_speeddial_white_24dp_2x.png" alt="" /> Ajouter
  </span>
</form>
