<?php
  if (!empty($_POST["assetsData"]) && ((isset($_POST["assetsData"][0]["ajax"]) && $_POST["assetsData"][0]["ajax"] == "true") || $_POST["ajax"] == "true")) {
    $assetsData = $_POST["assetsData"];

    if (!isset($assetsData[0]["name"])) {
      ?>
      <div id="nothing-here-container" class="flex-center-row">
        <p>Aucun résultat</p>
      </div>
      <?php
      return;
    }

    $assetType = $_POST["assetsData"][0]["asset_type"];
    $mode = $_POST["assetsData"][0]["view_mode"];
    $editable = ($mode === "general")?"true":"false";
  }else {
    $assetType = $_SESSION["asset_type"];
    $mode = "general";
    $editable = "true";
  }

?>
<table id="listing-table" data-type="<?=$assetType?>">
  <tr id="head-row">
    <?php
      if ($mode === "items") {
        ?>
        <th>ID</th>
        <?php
      }
    ?>
    <th>Nom</th>
    <th>Description</th>
    <?php
      if ($assetType === "room") {
        ?>
        <th>Capacité</th>
        <?php
      }
    ?>
    <th>Prix</th>
    <th>Image</th>
    <?php
      if ($mode === "items") {
        ?>
        <th>Supprimé</th>
        <?php
      }
    ?>
  </tr>
  <?php
    foreach ($assetsData as $key => $val) {
      $idAsset = (isset($val["id_asset"]))?$val["id_asset"]:"N/A";
      ?>
      <tr class="asset-row" data-asset-name="<?=$val["name"]?>" data-id-asset="<?=$idAsset?>">
        <?php
          if ($mode === "items") {
            ?>
            <td data-type="id"><?=$val["id_asset"]?></td>
            <?php
          }
        ?>
        <td data-type="name" contenteditable="<?=$editable?>"><?=$val["name"]?></td>
        <td data-type="description" contenteditable="<?=$editable?>"><?=$val["description"]?></td>
        <?php
          if ($assetType === "room") {
            ?>
            <td data-type="seats" contenteditable="<?=$editable?>"><?=$val["seats"]?></td>
            <?php
          }
        ?>
        <td data-type="price" contenteditable="<?=$editable?>"><?=$val["price"]?></td>
        <td data-type="img" contenteditable="<?=$editable?>"><?=$val["img"]?></td>
        <?php
          if ($mode === "items") {
            ?>
            <td class="delete-cell" title="Supprimer ce produit"><span class="delete-cell-text"><?=$val["is_deleted_str"]?></span> <i class="clickable asset-action-delete asset-action-icon fas fa-times"></i></td>
            <?php
          }
        ?>
      </tr>
      <?php
    }
   ?>
   <tr class="asset-category-row-dummy" data-asset-name="<?=$val["name"]?>">
     <td data-type="name" contenteditable="<?=$editable?>"><?=$val["name"]?></td>
     <td data-type="description" contenteditable="<?=$editable?>"><?=$val["description"]?></td>
     <?php
       if ($assetType === "room") {
         ?>
         <td data-type="seats" contenteditable="<?=$editable?>"><?=$val["seats"]?></td>
         <?php
       }
     ?>
     <td data-type="price" contenteditable="<?=$editable?>"><?=$val["price"]?></td>
     <td data-type="img" contenteditable="<?=$editable?>"><?=$val["img"]?></td>
     <?php
       if ($mode === "items") {
         ?>
         <td class="delete-cell" title="Supprimer ce produit"><span class="delete-cell-text"><?=$val["is_deleted_str"]?></span> <i class="clickable asset-action-delete asset-action-icon fas fa-times"></i></td>
         <?php
       }
     ?>
   </tr>

   <tr class="asset-item-row-dummy" data-asset-name="<?=$val["name"]?>" data-id-asset="-1">
     <td data-type="id">N/A</td>
     <td data-type="name">N/A</td>
     <td data-type="description">N/A</td>
     <?php
       if ($assetType === "room") {
         ?>
         <td data-type="seats">N/A</td>
         <?php
       }
     ?>
     <td data-type="price">N/A</td>
     <td data-type="img">N/A</td>
     <?php
       if ($mode === "items") {
         ?>
         <td class="delete-cell" title="Supprimer ce produit"><span class="delete-cell-text">Non</span> <i class="clickable asset-action-delete asset-action-icon fas fa-times"></i></td>
         <?php
       }
     ?>
   </tr>

</table>
