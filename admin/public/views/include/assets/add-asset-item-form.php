<form id="add-asset-form" class="flex-center-row">
  <select class="select-form-input add-asset-form-input" name="name">
    <?php
    if (!empty($_POST)) {
      $siteNamesAndId = $_POST["data"]["siteNamesAndId"];
      $assetNames = $_POST["data"]["assetNames"];

      foreach ($assetNames as $key => $val) {
        ?>
        <option value="<?=$val?>"><?=$val?></option>
        <?php
      }
    }
    ?>
  </select>
  <select class="select-form-input add-asset-form-input" name="siteData">
    <?php
    if (!empty($_POST)) {
      foreach ($siteNamesAndId as $key => $val) {
        ?>
        <option value="<?php echo "$val[0]:$val[1]"?>"><?=$val[0]?></option>
        <?php
      }
    }
    ?>
  </select>
  <input class="add-asset-form-input" required="" type="number" name="qty" value="1" placeholder="Quantité">
  <span class="clickable asset-add-button" id="asset-item-add-button">
    <img class="plus-icon" src="https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/2x/btw_ic_speeddial_white_24dp_2x.png" alt="" /> Ajouter
  </span>
</form>
