<?php
  if (!empty($_POST["userData"]) && $_POST["ajax"] == "true") {
    $userData = $_POST["userData"];
  }

  if (!isset($userData[0]["email"])) {
    ?>
    <div id="nothing-here-container" class="flex-center-row">
      <p>Aucun résultat</p>
    </div>
    <?php
    return;
  }

?>
<table id="listing-table">
  <tr id="head-row">
    <th>Nom</th>
    <th>Prénom</th>
    <th>Email</th>
    <th>Téléphone</th>
    <th>Abonnement</th>
    <th>Activé</th>
    <th>Réinitialisation</th>
    <th>Supprimé</th>
  </tr>
  <?php
    foreach ($userData as $key => $val) {
      ?>
      <tr class="user-row" data-email="<?=$val["email"]?>">
        <td data-type="firstname" contenteditable="true"><?=$val["firstname"]?></td>
        <td data-type="lastname" contenteditable="true"><?=$val["lastname"]?></td>
        <td data-type="email" contenteditable="true"><?=$val["email"]?></td>
        <td data-type="phone" contenteditable="true"><?=$val["phone"]?></td>
        <td><?=$val["sub_name"]?></td>
        <td><?=$val["is_activated_str"]?></td>
        <td class="reset-cell">Réinitialiser</td>
        <td class="delete-cell" title="Supprimer cet utilisateur"><span class="delete-cell-text"><?=$val["is_deleted_str"]?></span> <i class="clickable user-action-delete user-action-icon fas fa-times"></i></td>
      </tr>
      <?php
    }
   ?>
   <tr class="user-row-dummy" data-email="">
     <td data-type="firstname" contenteditable="true"></td>
     <td data-type="lastname" contenteditable="true"></td>
     <td data-type="email" contenteditable="true"></td>
     <td data-type="phone" contenteditable="true"></td>
     <td>N/A</td>
     <td>Non</td>
     <td class="reset-cell">Reinitialiser</td>
     <td class="delete-cell" title="Supprimer cet utilisateur"><span class="delete-cell-text">Non</span> <i class="clickable user-action-delete user-action-icon fas fa-times"></i></td>
   </tr>
</table>
