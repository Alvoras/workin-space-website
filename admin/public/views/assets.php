<?php require_once INCLUDE_ADMIN_VIEWS.'/global/head.php' ?>
<link rel="stylesheet" href="<?=INCLUDE_ADMIN_CSS?>/assets/assets.css"/>
</head>
<body>
<div id="main-container" class="flex-center-row">
  <?php
    $assetsData = $_SESSION["assets_data"];
   ?>
  <?php require_once INCLUDE_ADMIN_VIEWS."/global/side-menu.php" ?>
  <div id="dashboard-container">
    <div id="chart-container">
      <?php require_once INCLUDE_ADMIN_VIEWS."/global/chart.php" ?>
    </div>
    <div id="switch-view-container" class="flex-center-col" data-view-mode="general">
      <div id="switch-view-subcontainer">
        <span class="clickable view-black-theme" id="switch-view-button">
           Vue d'ensemble
        </span>
      </div>
    </div>
    <div id="assets-listing-container" class="flex-center-col">
      <div id="assets-listing">
        <div id="search-bar-container">
          <input id="search-bar" type="text" name="search" value="" placeholder="Rechercher">
          <span class="clickable" id="search-button">
            <i class="fas fa-search"></i> Rechercher
          </span>
        </div>
        <div id="assets-listing-header" class="flex-center-row">
          <?php include INCLUDE_ADMIN_VIEWS."/assets/add-asset-category-form.php" ?>

          <!-- <form id="add-asset-form" class="flex-center-row">
            <input class="add-asset-form-input" required="" type="text" name="name" value="" placeholder="Nom">
            <input class="add-asset-form-input" required="" type="text" name="description" value="" placeholder="Description">
            <?php
              if ($_SESSION["asset_type"] === "room") {
                ?>
                <input class="add-asset-form-input" required="" type="number" name="seats" value="" placeholder="Capacité">
                <?php
              }
            ?>
            <input class="add-asset-form-input" required="" type="number" name="price" value="" placeholder="Prix à l'heure">
            <input class="add-asset-form-input" required="" type="text" name="img" value="" placeholder="Image">
            <span class="clickable" id="asset-add-button">
              <img class="plus-icon" src="https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/2x/btw_ic_speeddial_white_24dp_2x.png" alt="" /> Ajouter
            </span>
          </form> -->
        </div>
        <div id="site-selection-container" class="flex-center-row">
          <?php
          foreach ($_SESSION["sites_data"] as $key => $val) {
            ?>
            <span data-siteid="<?=$val["id_site"]?>" data-site-name="<?=$val["site_name"]?>" class="<?= ($key === 0)?"selected-header-name":'';?> clickable site-header-name"><?=$val["site_name"]?></span>
            <?php
          }
           ?>
        </div>

      <?php require_once INCLUDE_ADMIN_VIEWS."/assets/assets-listing.php" ?>
        </div>

    </div>
  </div>
</div>

<?php require_once INCLUDE_ADMIN_VIEWS."/global/scripts.php" ?>
<script src="<?=INCLUDE_ADMIN_JS?>/assets/assets-toolbox.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<script src="<?=INCLUDE_ADMIN_JS?>/assets/chart.js"></script>
<script src="<?=INCLUDE_ADMIN_JS?>/global/main-events.js"></script>
<script src="<?=INCLUDE_ADMIN_JS?>/assets/assets-events.js"></script>
<?php require_once '../'.INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
