<?php require_once INCLUDE_ADMIN_VIEWS.'/global/head.php' ?>
<link rel="stylesheet" href="<?=INCLUDE_ADMIN_CSS?>/users/users.css"/>
</head>
<body>
<div id="main-container" class="flex-center-row">
  <?php
    $userData = $_SESSION["user_data"];
   ?>
  <?php require_once INCLUDE_ADMIN_VIEWS."/global/side-menu.php" ?>
  <div id="dashboard-container">
    <div id="chart-container">
      <?php require_once INCLUDE_ADMIN_VIEWS."/global/chart.php" ?>
    </div>
    <div id="users-listing-container" class="flex-center-col">
      <div id="users-listing">
        <div id="search-bar-container">
          <input id="search-bar" type="text" name="search" value="" placeholder="Rechercher">
          <span class="clickable" id="search-button">
            <i class="fas fa-search"></i> Rechercher
          </span>
        </div>
        <div id="users-listing-header" class="flex-center-row">
          <form id="add-user-form" class="flex-center-row">
            <input class="add-user-form-input" required="" type="text" name="firstname" value="" placeholder="Prénom">
            <input class="add-user-form-input" required="" type="text" name="lastname" value="" placeholder="Nom">
            <input class="add-user-form-input" required="" type="email" name="email" value="" placeholder="Email">
            <input class="add-user-form-input" required="" type="tel" name="phone" value="" placeholder="Téléphone">
            <span class="clickable" id="user-add-button">
              <img class="plus-icon" src="https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/2x/btw_ic_speeddial_white_24dp_2x.png" alt="" /> Ajouter
            </span>
          </form>
        </div>
      <?php require_once INCLUDE_ADMIN_VIEWS."/users/listing-table.php" ?>
        </div>

    </div>
  </div>
</div>

<?php require_once INCLUDE_ADMIN_VIEWS."/global/scripts.php" ?>
<script src="<?=INCLUDE_ADMIN_JS?>/global/main-events.js"></script>
<script src="<?=INCLUDE_ADMIN_JS?>/users/users-events.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<script src="<?=INCLUDE_ADMIN_JS?>/users/users-toolbox.js"></script>
<script src="<?=INCLUDE_ADMIN_JS?>/users/chart.js"></script>
<?php require_once '../'.INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
