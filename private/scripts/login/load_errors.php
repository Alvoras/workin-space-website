<?php
if (isset($_SESSION["errors"]) && count($_SESSION["errors"]) > 0) {
  ?>
  <script>document._errors = [];</script>
  <?php

  foreach ($_SESSION["errors"] as $key => $err) {
    ?>
    <script>document._errors.push('<?=$err["errMsg"]?>');</script>
    <?php
  }

  unset($_SESSION["errors"]);
}

?>
