<?php
require_once "../script_toolset_include.php";

$ret = [];
$ret["success"] = false;
$ret["successMsg"] = "Le mot de passe a bien été mis à jour";
$wrapper = new Wrapper();
$password = $_POST["password"];
$email = $_POST["email"];

// Hash the password
$wrappedPwd = $wrapper->wrapPwd($_POST["password"]);

// If the db update fails, then display an error
if (!$db->updatePwd($email, $wrappedPwd)) {
  $ret["success"] = false;
  $logger->log(LOG_INFO, "[$email] Failed to update password");
  $ret["errorMsg"] = "Échec lors de la mise à jour du mot de passe";
}else {
  $ret["success"] = true;

  // If the db update is successful, try to remove the token as we don't need it anymore
  // NOTE:
  // It is not worth to cancel the password update process if the token removal fails,
  $db->removeToken($email);
}

echo json_encode($ret);
