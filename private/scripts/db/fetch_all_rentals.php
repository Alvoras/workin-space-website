<?php

require_once __DIR__."/../script_toolset_include.php";

$dFormatter = new DateFormatter();

$REPUBLIQUE = 1;
$site = $REPUBLIQUE;

$dates = [];

if (!isset($_POST) || empty($_POST)) {
  $name = $_SESSION["grid_data"][0]["name"];
  // Return an array with the starting day and the end day as DateTimes
  $dates = $dFormatter->getThisWeekInterval();
}else {
  $site = $_POST["siteId"];
  $name = $_POST["item"];
  $dates[0] = new DateTime($_POST["start_date"]);
  $dates[1] = new DateTime($_POST["stop_date"]);
}

$ret = [];

$ret["success"] = false;

$data = $db->fetchAllRentals($dates[0], $dates[1], $site, $name, $target);

if (empty($data)) {
  $ret["errorMsg"] = "Impossible de charger les réservations";

  $logger->log(LOG_INFO, "[".$dates[0]->format('Y-m-d H:i:s').", ".$dates[1]->format('Y-m-d H:i:s').", $site, $name] Failed to fetch rentals data");

  $_SESSION["all_rentals"] = [];
}else {
  $ret["success"] = true;
  $_SESSION["all_rentals"] = $data;
}

if (!empty($_POST)) {
  echo json_encode($ret);
}
