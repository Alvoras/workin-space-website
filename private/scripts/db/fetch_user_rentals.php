<?php
require_once __DIR__."/../script_toolset_include.php";

$ret = [];
$ret["success"] = false;

$siteId = $_POST["siteId"];

if (!$user->loadRentals($db, $siteId)) {
  $logger->log(LOG_INFO, "[Site $siteId] Failed to fetch rentals for the site");
  $ret["errorMsg"] = "Erreur lors de la récupération de vos réservations";
}else {
  $ret["success"] = true;
}

echo json_encode($ret);
