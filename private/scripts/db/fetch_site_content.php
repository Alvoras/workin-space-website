<?php
require_once __DIR__."/../script_toolset_include.php";

$siteId = $_POST["siteId"];

$target = (isset($_POST["ajax"]))?$_POST["target"]:$_SESSION["target"];

$includeDeleted = false;

$siteContent = $db->getAssetsInSite($siteId, $includeDeleted, $target);

$atLeastOne = false;

foreach ($siteContent as $key => $val) {
  // Absolute path not working ??
  include '../../../public/back/views/include/global/item-grid/grid-card.php';
  $atLeastOne = true;
}

if (!$atLeastOne) {
  ?>
  <div class="nothing-found flex-center-col">
    <h1 id="nothing-found-msg">Il n'y a rien à afficher</h1>
  </div>
  <?php
}
