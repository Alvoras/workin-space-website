<?php
require_once "../script_toolset_include.php";

$dFormatter = new DateFormatter();
$wrapper = new Wrapper();

$ret = [];

$ret["success"] = false;

$email = $user->email;
$itemName = $_POST["item"];
$siteId = $_POST["siteId"];

$day = str_replace("/", "-", $_POST["day"]);

$start = $dFormatter->swapDayMonth($day).' '.$_POST["rentalData"]["startHour"];
$end = $dFormatter->swapDayMonth($day).' '.$_POST["rentalData"]["stopHour"];

$uniqid = uniqid();
$rentToken = $wrapper->hash256(RENT_TOKEN_SALT.$uniqid);

$availableAssetId = $db->getFirstAvailableAsset($itemName, $siteId, $start, $end);

if ($availableAssetId) {
  $ret["success"] = $db->addRental($start, $end, $email, $itemName, $siteId, $availableAssetId, $rentToken);

  if (!$ret["success"]) {
    $logger->log(LOG_INFO, "[$start, $end, $itemName, $siteId, $availableAssetId] Réservation impossible");

    $ret["errorMsg"] = "Impossible d'effectuer la réservation";
  }else {
    $ret["successMsg"] = "La réservation a été validée";
  }
}else {
  $ret["errorMsg"] = "Aucun(e) $itemName disponible dans cette période";
}


echo json_encode($ret);
