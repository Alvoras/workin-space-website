<?php
require_once __DIR__."/../script_toolset_include.php";

function parseEntries($arr)
{
  $ret = [];
  $len = count($arr);
  $buf = [];

  for ($i=0; $i < $len; $i++) {
    for ($j=0; $j < $len; $j++) {
      if ($arr[$i][0] == $arr[$j][0]){

        $dateInfo = date_parse($arr[$j][1]);
        $buf["hour"][] = $dateInfo["hour"];
        $buf["min"][] = $dateInfo["minute"];
        $buf["sec"][] = $dateInfo["second"];
      }
    }

    $ret[$arr[$i][0]] = $buf;

    $buf = [];
  }

  return $ret;
}

$dFormatter = new DateFormatter();

$dates = $dFormatter->getLast7DaysInterval();

$data = $db->get("date_in, date_out",
                  "visit",
                  "email = ?
                  AND date_in > ?
                  AND date_out < ?
                  LIMIT 7",
                  [
                    $user->email,
                    $dates[0]->format("Y-m-d 01:00:00"),
                    $dates[1]->format("Y-m-d 23:00:00")
                  ],
                  $db::W_KEY);

$buf = [];
$buf["in"] = [];
$buf["out"] = [];

$emptyDay = [];
$emptyDay["hour"] = 0;
$emptyDay["min"] = 0;
$emptyDay["sec"] = 0;

$week = [];
$week["in"] = [];
$week["out"] = [];

for ($i=0; $i < 7; $i++) {
  $tmpDate = $dates[0]->add(new DateInterval("P1D"))->format("Y-m-d");
  $week["in"][$tmpDate] = [];
  $week["out"][$tmpDate] = [];
}

foreach ($data as $key => $entry) {
  $buf["in"][] = explode(" ", $entry["date_in"]);
  $buf["out"][] = explode(" ", $entry["date_out"]);
}

$visits["in"] = parseEntries($buf["in"]);
$visits["out"] = parseEntries($buf["out"]);

foreach ($week["in"] as $date => $val) {
  if (isset($visits["in"][$date])) {
    $week["in"][$date] = $visits["in"][$date];
  }else {
    $week["in"][$date] = $emptyDay;
  }
}

foreach ($week["out"] as $date => $val) {
  if (isset($visits["out"][$date])) {
    $week["out"][$date] = $visits["out"][$date];
  }else {
    $week["out"][$date] = $emptyDay;
  }
}

echo json_encode($week);
