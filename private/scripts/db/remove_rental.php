<?php
require_once "../script_toolset_include.php";

$rentId = $_POST["rentId"];

$ret = [];

$ret["success"] = false;

if ($db->removeRental($rentId)) {
  $ret["success"] = true;
  $ret["successMsg"] = "La réservation a bien été retirée";
}else {
  $logger->log(LOG_INFO, "[Rental $rentId] Failed to remove the rental $rentId");
  $ret["errorMsg"] = "La réservation n'a pas pu être modifiée. N'hésitez pas à contacter notre service client si le problème persiste";
}

echo json_encode($ret);
