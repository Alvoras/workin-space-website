<?php
require_once __DIR__.'/../script_toolset_include.php';

$dFormatter = new DateFormatter();
$REPUBLIQUE = 1;
$site = $REPUBLIQUE;
$target = $_SESSION["target"];
$date = $dFormatter->getToday();

if (!isset($_POST) || empty($_POST)) {
  $name = $_SESSION["grid_data"][0]["name"];
}else {
  $site = $_POST["siteId"];
  $name = $_POST["item"];
}

$ret = [];
$ret["success"] = false;

// $ret["data"] = $db->getAssetLeft($name, $date, $site, $target);
$ret["data"] = $db->getAssetQty($name, $site, $target);

if (!empty($ret["data"])) {
  $_SESSION["qty_item_available"] = $ret["data"];
  $ret["success"] = true;
}else {
  $logger->log(LOG_INFO, "[$name, $site] Failed to fetch available quantity");
  $ret["errorMsg"] = "Erreur lors de la récupération de la quantité disponible du produit sélectionné";
}

if (!empty($_POST)) {
  echo json_encode($ret);
}
