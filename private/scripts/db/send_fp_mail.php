<?php
require_once "../script_toolset_include.php";

$wrapper = new Wrapper();
$email = $_POST["email"];
$ret = [];
$ret["success"] = false;

if (!$db->exists("email", "USERS", "email = ?", [$email])) {
  $ret["errorMsg"] = "$email n'est pas un compte valide";

  echo json_encode($ret);
  die();
}

$token = $wrapper->generateToken();
$isUpdated = $db->updateToken($email, $token);

if (!$isUpdated) {
  $logger->log(LOG_INFO, "[$email] Failed to send a password reset email to $email");

  $ret["errorMsg"] = "Impossible d'envoyer l'email. Veuillez réessayer";
  echo json_encode($ret);
  die();
}

$isStaff = false;
$mail = new Mail($email);

if ($mail->sendPwdResetMail($token, $isStaff)) {
  $ret["success"] = true;
  $ret["successMsg"] = "Un email a été envoyé à l'adresse $email";
}else {
  $logger->log(LOG_INFO, "[$email] Failed to send a password reset email to $email : \n\t".$mail->getError());
  $ret["errorMsg"] = "Le mail n'a pas pu être envoyé";
}

echo json_encode($ret);
