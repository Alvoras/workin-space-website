<?php
require_once __DIR__."/../script_toolset_include.php";

$wrapper = new Wrapper();

$ret = [];
$ret["success"] = False;

if (isset($_POST["mode"]) && $_POST["mode"] == "pwd") {
  $candidate = $_POST["form-data"]["old-pwd"];
  $hPwd = $db->fetchUserPwdHash($user->email);

  if (password_verify($candidate, $hPwd)) {
    if ($_POST["form-data"]["new-pwd"] != $_POST["form-data"]["new-pwd-confirm"]) {
      $ret["success"] = false;
      $ret["errorMsg"] = "Le mot de passe de confirmation ne correspond pas";
      echo json_encode($ret);
      return;
    }
  }else {
    $ret["success"] = false;
    $ret["errorMsg"] = "Mot de passe incorrect";
    echo json_encode($ret);
    return;
  }

  $_POST["user-data"]["password"] = $wrapper->wrapPwd($_POST["form-data"]["new-pwd"]);
}

// Add token verif ?
$res = $db->update($_POST["user-data"], "USERS", "email = ?", [$user->email]);

if ($res) {
  if ($_POST["mode"] !== "pwd") {
    // Update the content of the stored user object
    foreach ($_POST["user-data"] as $key => $val) {
      $user->$key = $val;
    }
  }
  $session->uploadUser($user);
  $ret["success"] = true;
  $ret["successMsg"] = "Le profil a bien été mis à jour";
}else {
  $logger->log(LOG_INFO, "[$user->email] Failed to update profile");
  $ret["errorMsg"] = "Le profil n'a pas pu être mis à jour";
}

echo json_encode($ret);
