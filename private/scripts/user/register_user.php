<?php
require_once "../script_toolset_include.php";

$wrapper = new Wrapper();
$db = Db::getInstance();

$listOfErrors = [];

$ret = [];

$ret["success"] = false;
$ret["successMsg"] = "Un email a été envoyé à ".$_POST["email"];
$ret["errors"] = [];

if( count($_POST) == 9
	&& !empty($_POST["firstname"])
	&& !empty($_POST["lastname"])
	&& !empty($_POST["email"])
	&& !empty($_POST["emailc"])
	&& !empty($_POST["password"])
	&& !empty($_POST["passwordc"])
	&& !empty($_POST["phone"])
	&& isset($_POST["cgu"]) ) {

	$error = false;

	//Suppression des espaces en début et en fin de chaine
	$_POST["name"] = trim($_POST["firstname"]);
	$_POST["lastname"] = trim($_POST["lastname"]);
	$_POST["email"] = trim($_POST["email"]);
	$_POST["emailc"] = trim($_POST["emailc"]);
	$_POST["phone"] = trim($_POST["phone"]);

	//balises HTML inoffensives
	$_POST['firstname'] = htmlspecialchars($_POST['firstname']);
	$_POST['lastname'] = htmlspecialchars($_POST['lastname']);
	$_POST['email'] = htmlspecialchars($_POST['email']);
	$_POST['emailc'] = htmlspecialchars($_POST['emailc']);
	$_POST['password'] = htmlspecialchars($_POST['password']);
	$_POST['passwordc'] = htmlspecialchars($_POST['passwordc']);
	$_POST['phone'] = htmlspecialchars($_POST['phone']);


	if( strlen($_POST["firstname"]) < 2 || strlen($_POST["firstname"]) >= 20){
		$error = true;
		$listOfErrors[] = "Le prénom doit faire entre 3 et 20 caractères";
		}

	if( strlen($_POST["lastname"]) < 2 || strlen($_POST["lastname"]) >= 20){
		$error = true;
		$listOfErrors[] = "Le nom de famille doit faire entre 3 et 20 caractères";
	}

	if( strlen($_POST["password"]) < 8 || strlen($_POST["password"]) > 64 ){
		$error = true;
		$listOfErrors[] = "Le mot de passe doit faire entre 8 et 64 caractères";
	}

	if( $_POST["passwordc"] != $_POST["password"]){
		$error = true;
		$listOfErrors[] = "Les deux mots de passes rentrés ne sont pas identiques";
	}

	if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) ){
		$error = true;
		$listOfErrors[] = "L'email n'est pas valide";
	}

	if($_POST["email"] != $_POST["emailc"]){
		$error = true;
		$listOfErrors[] = "La confirmation de l'email n'est pas valide";
	}

  if (!preg_match("#^0[1-68]([-. ]?[0-9]{2}){4}$#", $_POST['phone']))
  {
      $error = true;
      $listOfErrors[] = "Le numéro de téléphone n'est pas valide";
  }

  if (!verifyCaptcha($_POST["g-recaptcha-response"])) {
  	$error = true;
  	$listOfErrors[] = "Le captcha n'est pas valide";
  }

	//Check if this email doesn't already exists
	$req = $db->get("email", "USERS", "email = ?", [$_POST["email"]], Db::W_KEY);
		if(!empty($req)){
		$error = true;
		$listOfErrors[] = "Cet email existe déjà";
	}

	if($error){
		// $_SESSION["errors_form"] = $listOfErrors;
		// $_SESSION["data_form"] = $_POST;
    $ret["errors"] = $listOfErrors;
    echo json_encode($ret);
    die();
	}

	$token = $wrapper->generateToken();

	$wrappedPwd = $wrapper->wrapPwd($_POST["password"]);

  if($db->registerUser($wrappedPwd, $_POST["lastname"], $_POST["firstname"], $_POST["email"], $_POST["phone"], $token)) {
		// if register ok, then send mail
		// if mail fails, then remove the registered user
		$mail = new Mail($_POST["email"]);
		if ($mail->sendActivationMail($token, $_POST["firstname"])) {
			$ret["success"] = true;
		}else {
			$logger->log(LOG_INFO, "[".$_POST["email"]."] Failed to send activation to ".$_POST["email"]);
			$listOfErrors[] = "Erreur lors de l'envoi du mail";
		}

		$ret["errors"] = $listOfErrors;
    echo json_encode($ret);
    die();

  }else {
    $listOfErrors[] = "L'utilisateur n'a pas pu être inscrit.";
		$logger->log(LOG_INFO, "[".$_POST["email"]."] Failed to insert user into db");
    $ret["errors"] = $listOfErrors;
    echo json_encode($ret);
    die();
  }

}else{
  $listOfErrors[] = "Le formulaire est invalide";
	$logger->log(LOG_INFO, "[".$_POST["email"]."] Invalid register form info");
  $ret["errors"] = $listOfErrors;
  echo json_encode($ret);
  die();
}
