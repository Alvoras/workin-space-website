<?php
require_once "../script_toolset_include.php";

$dFormatter = new DateFormatter();

$ret = [];
$ret["success"] = false;

$mode = $_POST["checkoutMode"];

$email = $user->email;

switch ($mode) {
  case 'renew':
    $dates = $db->get("sub_start, sub_stop", "subscribe", "email = ?", [$email], Db::W_KEY)[0];
    $end = (new DateTime($dates["sub_stop"]))->add(new DateInterval('P'.$_POST["duration"].'M'));
    $endStr = $end->format("Y-m-d H:i:s");
    $ret["success"] = $db->renewSubscription($email, $endStr);
    break;

  case 'update':
    // DB ARCHITECTURE PROBLEM
    $res = $db->updateSubscription();
    break;

  case 'subscribe':
    $duration = $_POST["duration"];
    $dates = $dFormatter->getMonthInterval($duration);
    $start = $dates[0]->format("Y-m-d H:i:s");
    $end = $dates[1]->format("Y-m-d H:i:s");
    $subId = $_POST["subId"];
    $res = $db->subscribeTo($email, $subId, $start, $end);
    break;
}

echo json_encode($ret);
