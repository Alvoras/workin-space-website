<?php

require_once __DIR__."/../script_toolset_include.php";

require_once(__DIR__ . '/../../model/db/fetch_staff_info.php');

$status = $_POST['value'];
if($status == 'all') $status = null;

$site = $_POST['site'];

if($_POST['category'] == 'user')
{
    $idStaffType = $db->get("id_staff_type", "STAFF", "email = ?",[$user->email], Db::W_KEY);
    if($idStaffType == 3) $_POST['category'] = 'admin';
    else $emails =$user->email;
}
else if($_POST['category'] == 'site')
{
    if($site == 'undefined')
    {
        $id_site = $db->get("id_site", "STAFF", "email = ?",[$user->email], Db::W_KEY);
        $emails = $db->get("email", "STAFF", "id_site = ?",[$id_site[0]["id_site"]], Db::W_KEY);
    }
    else
    {
        $emails = $db->get("email", "STAFF", "id_site = ?", [$site], Db::W_KEY);
    }
}
else if($_POST['category'] == 'admin')
{
    $emails = null;
}

$ticket = Ticket::getTickets($db, $emails, $_POST['category'], $status);

echo json_encode($ticket);
