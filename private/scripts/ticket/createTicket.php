<?php

require_once __DIR__."/../script_toolset_include.php";

if(isset($_POST['ticket_name']) && isset($_POST['ticket_description'])) {

	$id = $db->get("id_staff", "STAFF", "email = ?",[$user->email], Db::W_KEY);


	if(Ticket::createTicket($db, $_POST['ticket_name'], 1, $_POST['ticket_description'], $id[0]["id_staff"], $user->email)) {
    	http_response_code(201);
  	}else {
    	http_response_code(500);
  	}
}else {
	http_response_code(400);
}
