<?php

require_once __DIR__."/../script_toolset_include.php";

if(isset($_POST['ticket_id']) && isset($_POST['status'])) {

	if(Ticket::updateStatusTicket($db, $_POST['ticket_id'], $_POST['status'])) {
    	http_response_code(201);
  	}else {
    	http_response_code(500);
  	}
}else {
	http_response_code(400);
}
