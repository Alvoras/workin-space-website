<?php

require_once __DIR__."/../script_toolset_include.php";


if(isset($_POST['ticket_id']))
{
  if(Ticket::deleteTicket($db, $_POST['ticket_id'])) {
    http_response_code(201);
  }else {
    http_response_code(500);
  }
}else {
  http_response_code(400);
}
