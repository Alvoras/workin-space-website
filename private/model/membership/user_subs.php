<?php
require_once INCLUDE_MODEL."/membership/subscriptions.php";
require_once INCLUDE_MODEL."/model_toolset_include.php";

$idSubscriptionQuery = $db->get("id_subscription", "subscribe", "email = ?", [$user->email], Db::W_KEY);

$user->idSubscription = (isset($idSubscriptionQuery[0]["id_subscription"]))?$idSubscriptionQuery[0]["id_subscription"]:null;

$session->uploadUser($user);
