<?php
$includeDeleted = false;

if (!isset($_SESSION["sites_data"])) {
  $sitesData = $db->getSitesData();
  $session->appendTo("sites_data", $sitesData);
}
if (!isset($_SESSION["site_content"])) {
  $siteContent = $db->getRoomsInSite(1, $includeDeleted);
  $session->appendTo("site_content", $siteContent);
}
