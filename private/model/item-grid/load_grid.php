<?php

$dFormatter = new DateFormatter();

$REPUBLIQUE = 1;
$dayOfTheWeek = date('N');
$dayOfTheMonth = date('j');
$month = date('n');
$year = date('Y');

$data = null;

// NOTE : $month is the month of the start date given, aka the monday of this week
// getMaxDaysFromMonth() will thus return the number of days of the monday's month number
$totalDayThisMonth = $dFormatter->getMaxDaysFromMonth($month, $year);

$mondayNo = $dayOfTheMonth - $dayOfTheWeek;

if ($month-1 == 0) {
  $prevMonth = 12;
  $prevYear = $year-1;
}else {
  $prevMonth = $month-1;
  $prevYear = $year;
}

$totalDayPreviousMonth = $dFormatter->getMaxDaysFromMonth($prevMonth, $prevYear);

if ($mondayNo < 0) {
  $mondayNo = ($totalDayPreviousMonth + $dayOfTheMonth) - $dayOfTheWeek;
  $month = $prevMonth;
  $year = $prevYear;
}

$start = new DateTime("$year-$month-$mondayNo 01:00:00");
$startStr = $start->format('Y-m-d H:i:s');

// Add 7 days and 22 hours
$end = $start->add(new DateInterval('P7DT22H'));
$endStr = $end->format('Y-m-d H:i:s');

$includeDeleted = false;

// $_SESSION["target"] is set in index.php, right before this file is included
switch ($_SESSION["target"]) {
  case 'hardware':
  case 'room':
  case 'schedule':
    $data["grid_data"] = $db->getAssetsInSite($REPUBLIQUE, $includeDeleted, $_SESSION["target"]);
    foreach ($data["grid_data"] as $key => $val) {
      $data["grid_data"][$key]["rentals"] = $db->getRentals($val["name"], $REPUBLIQUE, $startStr, $endStr);
    }
    break;

  case 'cafeteria':
    $data["grid_data"] = $db->get("hardware_name", "HARDWARE", null, null, Db::W_NUM);
    break;
}

$session->append($data);
