<?php
require_once INCLUDE_MODEL."/model_toolset_include.php";
$cookie = Cookie::getInstance();

if (isset($user)) {
  if (count($user->getQrSalt()) > 0) {
    unlink(QRCACHE_PATH."/".$user->getQrSalt().".png");
  }
  $user->removeQrSalt($db);
  $db->removeToken($user->email);
  unset($user);
}

$cookie->flush();
$session->reset();

$_SESSION["flush_cookies"] = true;

header("Location: /p/login");
die();
