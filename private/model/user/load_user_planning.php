<?php
require_once INCLUDE_MODEL."/model_toolset_include.php";
$REPUBLIQUE = 1;

$user->loadRentals($db, $REPUBLIQUE);

if (!isset($_SESSION["sites_data"])) {
  $sitesData = $db->getSitesData();
  $session->appendTo("sites_data", $sitesData);
}

if (!isset($_SESSION["site_schedule"]) || empty($_SESSION["site_schedule"])) {
  $siteSchedule = $db->getDefaultSiteSchedule();

  foreach ($siteSchedule as $key => $val) {
    $siteSchedule[$key]["open_duration"] = $val["site_close"] - $val["site_open"];
  }

  $session->appendTo("site_schedule", $siteSchedule);
}
