<?php
// Called when and url containing ?a=verify&token=ab1cd23f4&email=email@gmail.com
// is resolved
//
// Test the GET token against the token associated with the account using $_GET["email"]
//
// Set the SESSION["verify_token"] accordingly and redirect to ?p=fp

require_once INCLUDE_MODEL."/model_toolset_include.php";

Err::clean();

$skip = false;

// Set by index.php to handle redirection
$location = $_SESSION["location"];
$token = $_GET["token"];
$email = $_GET["email"];

$error = [];

if ($db->evalToken($email, $token)) {
  $_SESSION["email"] = $email;

  $_SESSION["verify_token"] = true;

  if ($location === "login") {
    if (!$db->activateAccount($email)) {
      $err = new Err("Erreur lors de l'activation du compte");
      $err->load();
      $skip = true;
    }

    // We only want to create the row if the activation has been successful
    if (!$skip) {
      if (!$db->createAuthTokenRow($email)) {
        $err = new Err("Erreur lors de l'activation du compte");
        $err->load();
      }

      // The removal of the token must not depend on the row creation, since a
      // previous succesful activation but a failed row creation must not prevent
      // the token to be deleted if there is a new activation try
      $db->removeToken($email);
    }
  }
}else {
  $err = new Err("Veuillez vous reconnecter");
  $err->load();
}

// dbg($_SESSION["verify_token"]);

if (isset($_GET["setPassword"]) && $_GET["setPassword"] == "true") {
  $location = "fp";
}

header("Location: /p/$location");
die();
