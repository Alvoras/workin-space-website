<?php
require_once INCLUDE_MODEL."/model_toolset_include.php";

// Look for a remember me cookie
if ($cookie->isRemMeSet()) {
  // Validate the remember me cookie by looking up the database auth info
  if (Warden::validateRememberMe($db)) {
    // If the info are valids, then load up the user and redirect him to back end
    $selector = $cookie->getSelector();

    if (!isset($selector)) {
      $cookie->flush();
      header('Location: /p/login');
      die();
    }

    $email = $db->getEmailFromSelector($selector);

    if (!isset($email)) {
      $cookie->flush();
      header('Location: /p/login');
      die();
    }

    $data = $db->fetchUserInfo($email);

    if (!isset($data)) {
      $cookie->flush();
      header('Location: /p/login');
      die();
    }

    /*
    $data["membership"] = $db->fetchUserMembership($email);

    if (isset($data["membership"][0])) {
      $data["membership"] = $data["membership"][0];
      $data["membership"]["time_left"] = (strtotime($data["membership"]["sub_stop"]) - strtotime($data["membership"]["sub_start"]))/86400;
    }
    */
    $user = new User($data);

    $session->uploadUser($user);

    header('Location: /p/dashboard');
    die();
  }else {
    $logger->log(LOG_WARNING, "[".$_SERVER["REMOTE_ADDR"]."] Corrupted remember me");

    $cookie->flush();
    $session->reset();
  }
}
