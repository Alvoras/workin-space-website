<?php
require_once INCLUDE_MODEL."/model_toolset_include.php";

$dFormatter = new DateFormatter();
$wrapper = new Wrapper();
$cookie = Cookie::getInstance();
$errorData = [];
$email = $_POST["email"];


// We need to allow the checked box if it is set
if ( (count($_POST) === 2 ||count($_POST) === 3)
	&& !empty( $_POST ["email"] )
	&& !empty( $_POST ["password"] )
) {

	if ($db->connectUser($email, $_POST["password"])){
    $data = $db->fetchUserInfo($email);

    $user = new User($data);

    if ($user->cacheQrCode($db)) {
      // if $_POST["remember_me"] is set (which means that the box is cheked)
      // Then insert validator, selector and expire time in db
      if (isset($_POST["remember_me"])) {
				if (!$cookie->generateRememberMe($email)) {
			    $logger->log(LOG_ERROR, "[$email] Failed to generate remember me cookie");

					$err = new Err("Veuillez réessayer");
					$err->load();
					header('Location: /p/login');
					die();
				}
      }

      //$session->append($data);
      $session->uploadUser($user);

      header('Location: /p/dashboard');
      die();
    }else {
			// Failed to insert qrSalt into db
			$err = new Err("Veuillez réessayer");
			$err->load();
			$logger->log(LOG_ERROR, "[$email] Failed to cache the QrCode");

			header('Location: /p/login');
			die();
    }
	} else {
			$err = new Err("Identifiants invalides");
			$err->load();

			header('Location: /p/login');
			die();
		}
} else {
	$err = new Err("Veuillez réessayer");
	$err->load();
	$logger->log(LOG_WARNING, "[$email] Bad connection form info");

	header('Location: /p/login');
	die();
}

$logger->log(LOG_WARNING, "[$email] Unexpected redirection failure");

header('Location: /p/login');
die();
