<?php
require_once INCLUDE_MODEL."/model_toolset_include.php";

$currentDate = date("Y-m-d");

$rentals = $user->loadRentals($db, "all");

$subscriptionData = $user->getUserSubscriptions($db);

$user->membership = (isset($subscriptionData["id_subscription"]))?$subscriptionData:null;
$diff = date_diff(date_create($user->membership["sub_stop"]), date_create($currentDate));

$monthDiff = intval($diff->format('%m'));
$dayDiff = intval($diff->format('%d'));

$user->membership["time_left"] = $dayDiff + ($monthDiff*30);

$session->uploadUser($user);
