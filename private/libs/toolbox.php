<?php
function dump($var)
{
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
}

function dbg($item)
{
    dump($item);
    exit();
}

function isValidRequest($config, $page)
{
    if (!(array_key_exists($page, $config["views"]))) {
        throw new Exception;
    }
}

function isValidAction($config, $page, $post)
{
  foreach ($post as $key => $value) {
    if ( !(array_key_exists($key, $config["views"][$page]["actions"]))) {
      throw new Exception;
    }
  }
}

function itemAvailable($rentals, $time, $itemName)
{
  $cnt = 0;

  foreach ($rentals as $key => $val) {
    $dtRentStart = new DateTime($val["rent_start"]);
    $dtRentStop = new DateTime($val["rent_stop"]);

    if ($time >= $dtRentStart && $time < $dtRentStop && $val["name"] === $itemName) {
      $cnt++;
    }
  }

  return ($cnt == $_SESSION["qty_item_available"])?false:true;
}

function verifyCaptcha($response)
{
  //reCAPTCHA
  // Ma clé privée
  $secret = CAPTCHA_KEY;
  // Paramètre renvoyé par le recaptcha
  $response = $_POST['g-recaptcha-response'];
  // On récupère l'IP de l'utilisateur
  $remoteip = $_SERVER['REMOTE_ADDR'];

  $apiUrl = "https://www.google.com/recaptcha/api/siteverify?secret="
      . $secret
      . "&response=" . $response
      . "&remoteip=" . $remoteip ;

  $captchaRes = json_decode(file_get_contents($apiUrl), true);

  return $captchaRes["success"];
}
