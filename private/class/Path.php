<?php
/**
 * Handle path manipulation
 */

class Path{
    /**
     * Format any array or string to a valid path
     * @param  string $path string or array containing all the path element to be joined
     * @return string       formated path
     */
    public static function format($path){
        // Turn single string to an array as we must be able to call this function
        // giving it only a string as well as an array of string
        $array = (is_array($path))?$path:[$path];

        // Insert SITE_ROOT as the first element in the array
        array_unshift($array, SITE_ROOT);
        return join('/', $array);
    }

    /**
     * Require the view bound (set in conf.inc.php) to the given page title
     * @param  array $config array defined in conf.inc.php containing most of the bound elements for the site
     * @param  string $view  view name
     * @return none         /
     */
    public static function display($config, $view)
    {

      $pageTitle = (isset($config["views"][$view]["title"]))?$config["views"][$view]["title"]:null;;
      if ($pageTitle != null) {
        $_SESSION["title"] = $pageTitle;
      }
      $paths = $config["views"][$view]["path"];
      require_once self::format($paths);
      die();
    }

    /**
     * Require the view bound (set in conf.inc.php) to the given page title
     * @param  array $config array defined in conf.inc.php containing most of the bound elements for the site
     * @param  string $view  view name
     * @return none         /
     */
    public static function adminDisplay($config, $view)
    {
      $pageTitle = (isset($config["admin-views"][$view]["title"]))?$config["admin-views"][$view]["title"]:null;;
      if ($pageTitle != null) {
        $_SESSION["title"] = $pageTitle;
      }
      $paths = $config["admin-views"][$view]["path"];
      require_once self::format($paths);
      die();
    }

}
