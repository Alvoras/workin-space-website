<?php
/**
 * Handle redirection and user routes
 */

class Warden
{
    /**
     * Bounce the user if the ?a= argument is not expected
     * The check is done by checking the array define in conf.inc.php
     * @param  array  $config config array
     * @param  string  $query  get argument to be evalued
     */
    public static function isValidAdminAction($config, $query)
    {
      if (!(array_key_exists($query, $config["admin-actions"]))) {
          throw new Exception;
      }
    }

    /**
     * Bounce the user if the ?a= argument is not expected
     * The check is done by checking the array define in conf.inc.php
     * @param  array  $config config array
     * @param  string  $query  get argument to be evalued
     */
    public static function isValidAction($config, $query)
    {
      if (!(array_key_exists($query, $config["actions"]))) {
          throw new Exception;
      }
    }

    /**
     * Bounce the user if the ?p= argument is not expected
     * @param  array  $config config array
     * @param  string  $param  value to test
     */
    // Check if the given GET parameter is expected
    public static function isValidParameter($config, $param)
    {
      if (!(in_array($param, $config["parameters"]))) {
          throw new Exception;
      }
    }

    /**
     * Bounce the user if the view queried by ?p= are expected
     * @param  array  $config config array
     * @param  string  $view   view argument to be evalued
     */
    public static function isValidView($config, $view)
    {
        if (!(array_key_exists($view, $config["views"]))) {
            throw new Exception;
        }
    }

    /**
     * Bounce the user if the view queried by ?p= are expected
     * @param  array  $config config array
     * @param  string  $view   view argument to be evalued
     */
    public static function isValidAdminView($config, $view)
    {
        if (!(array_key_exists($view, $config["admin-views"]))) {
            throw new Exception;
        }
    }

    /**
     * Bounce the user if he's not connected. Used, in sensible area,
     * it look the existence of the token value, which is a teltale sign of the connected state of
     * the user
     * @param  Db  $db Db object
     * @return boolean     Return true if the user is currently connected
     */
    public static function isConnected($db)
    {
      $session = new Session();
      $user = $session->loadUser();

      if (!empty($_SESSION["user"])) {
        return $user->exists($db);
      }else {
        return false;
      }
    }

    public static function validateRememberMe($db)
    {
      $continue = false;
      $ret = false;
      $wrapper = new Wrapper();
      $cookie = Cookie::getInstance();

      $selector = $cookie->getSelector();
      $validator = $cookie->getValidator();

      $dbAuthInfo = $db->fetchAuthInfo($selector);

      if (isset($dbAuthInfo["expires"])) {
        $dbValidator = $dbAuthInfo["hashed_validator"];
        $expires = $dbAuthInfo["expires"];

        $now = date("Y-m-d H:i:s");

        if ($now < $expires && $wrapper->evalValidator($dbValidator, $validator)) {
          $ret = true;
        }
      }

      return $ret;
    }
}
