<?php

/**
 * Cookie layer
 */
class Cookie
{
  private static $instance = null;

  private function __construct()
  {

  }

  /**
   * getInstance from the Singleton pattern
   * @return Cookie The object is instanciated if it does not exist, else returns the object
   */
  public static function getInstance()
  {
    if (is_null(self::$instance)) {
      self::$instance = new Cookie();
    }

    return self::$instance;
  }

  /**
   * Generate the remember me cookie
   * @param  String $email email of the connecting user
   */
  public function generateRememberMe($email)
  {
    $ret = false;
    $wrapper = new Wrapper();
    $db = Db::getInstance();

    $validator = $wrapper->generateValidator();
    $selector = $wrapper->generateSelector();

    $hValidator = $wrapper->wrapValidator($validator);

    // Plus une semaine
    $inOneWeek = time()+60*60*24*7;
    $mysqlInOneWeek =  date("Y-m-d H:i:s", $inOneWeek);

    $ret = $db->updateAuthTokens($selector, $hValidator, $email, $mysqlInOneWeek);

    $this->set("remember_me", "$selector:$validator", $inOneWeek);

    // setcookie("remember_me", "$selector:$validator", $inOneWeek, '/');

    return $ret;
  }

  /**
   * Set a cookie key to the given value
   * @param String $name   key of the cookie
   * @param String $value  string to store inside the cookie
   * @param int $expire Unix timestamp of the desired cookie's expiration date
   */
  public function set($name, $value, $expire)
  {
    setcookie($name, $value, $expire, '/', '.hbouffier.info');
  }

  /**
   * Delete the given key
   * @param  String $name key of the cookie to delete
   */
  public function delete($name)
  {
    unset($_COOKIE[$name]);
  }

  /**
   * Delete all of the cookie content
   */
  public function flush()
  {
    foreach ($_COOKIE as $key => $value) {
      $this->set($key, '', 1);
      // setcookie($key, '', 1, '/', '.ws.localhost');
    }
  }

  /**
   * Returns the selector of the remember_me cookie if he's set
   * @return String selector stored into the cookie
   */
  public function getSelector()
  {
    $selector = false;

    if (isset($_COOKIE["remember_me"])) {
      $remMe = $_COOKIE["remember_me"];
      $selector = explode(':', $remMe)[0];
    }

    return $selector;
  }

  /**
   * Return the stored validator
   * @return Stirng Validator of the remember me cookie
   */
  public function getValidator()
  {
    $validator = false;

    if (isset($_COOKIE["remember_me"])) {
      $remMe = $_COOKIE["remember_me"];
      $validator = explode(':', $remMe)[1];
    }

    return $validator;
  }

  public function isRemMeSet()
  {
    return (isset($_COOKIE["remember_me"]))?true:false;
  }

  /**
   * Get the data stored in the given key
   * @param  String $name key of the cookie to retrieve
   * @return String       Data stored in the cookie
   */
  public function get($name)
  {
    return (isset($_COOKIE[$name]))?$_COOKIE[$name]:false;
  }

}
