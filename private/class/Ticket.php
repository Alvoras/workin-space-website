<?php

class Ticket implements JsonSerializable{

  private $id_ticket;
  private $ticket_name;
  private $ticket_status;
  private $ticket_description;
  private $id_staff;
  private $email;
  private $date_start;
  private $is_deleted;

  public function __construct($id_ticket, $ticket_name, $ticket_status, $ticket_description, $id_staff, $email, $date_start, $date_end, $is_deleted) {
    $this->id_ticket = $id_ticket;
    $this->ticket_name = $ticket_name;
    $this->ticket_status = $ticket_status;
    $this->ticket_description = $ticket_description;
    $this->id_staff = $id_staff;
    $this->email = $email;
    $this->date_start = $date_start;
    $this->date_end = $date_end;
    $this->is_deleted = $is_deleted;
  }

  public function getIdTicket() {
    return $this->id_ticket;
  }

  public function getTicketName() {
    return $this->ticket_name;
  }

  public function getTicketStatus() {
    return $this->ticket_status;
  }

  public function getTicketDescription() {
    return $this->ticket_description;
  }

  public function getIdStaff() {
    return $this->id_staff;
  }

  public function jsonSerialize() {
    return get_object_vars($this);
  }

   private static function ticketFromRow($row) {
    return new Ticket(
        $row['id_ticket'],
        $row['ticket_name'],
        $row['ticket_status'],
        $row['ticket_description'],
        $row['id_staff'],
        $row['email'],
        $row['date_start'],
        $row['date_end'],
        $row['is_deleted']
      );
  }

  private static function ticketFromRows($rows) {
    $tickets = [];
    foreach ($rows as $row) {
      $tickets[] = self::ticketFromRow($row);
    }
    return $tickets;
  }

  public static function getTickets($db, $emails, $category, $status) {

    $argQuery = "";

    if($category == 'admin'){
      if($status != null) $rows = $db->get("*", "TICKETS", "ticket_status = ?",[$status], Db::W_KEY);
      else $rows = $db->get("*", "TICKETS", null, null, Db::W_KEY);
    }
    else if($category == 'user')
    {
      // $argQuery = "email = '".$emails."'";
      //
      // if($status != null)
      // {
      //   $argQuery = $argQuery." AND ticket_status = '".$status."'";
      // }

      $argQuery = "email = ?";
      $bufArr = [];
      $bufArr[] = $emails;
      if ($status != null) {
        $argQuery .= "AND ticket_status = ?";
        $bufArr[] = $status;
      }

      $rows = $db->get("*", "TICKETS", $argQuery, $bufArr, Db::W_KEY);
    }
    else if($category == 'site')
    {
      $rows = [];
      foreach ($emails as $email) {
        foreach ($email as $key => $value) {
          // $argQuery = "email = '".$value."'";
          // if($status != null) $argQuery .= " AND ticket_status = '".$status."'";
          $argQuery = "email = ?";
          $bufArr = [];
          $bufArr[] = $value;
          if ($status != null) {
            $argQuery .= "AND ticket_status = ?";
            $bufArr[] = $status;
          }
          $rows = array_merge($rows, $db->get("*", "TICKETS", $argQuery, $bufArr, Db::W_KEY));
        }
      }
    }

    return self::ticketFromRows($rows);
  }

  public static function createTicket($db, $ticket_name, $ticket_status, $ticket_description, $id_staff, $email ) {

    date_default_timezone_set('Europe/Paris');
    $result = $db->insert("TICKETS (ticket_name,ticket_status,ticket_description, id_staff, email, date_start, date_end) VALUES (?, ?, ?, ?, ?, ?, ?);", [$ticket_name, $ticket_status, $ticket_description, $id_staff, $email, date('Y-m-d H:i:s', time()), null]);

    return  $result;
  }

  public static function deleteTicket($db, $id_ticket)
  {
    $data = [];
    $data["is_deleted"] = 1;
    $result = $db->update($data, 'TICKETS', "id_ticket = ?", [$id_ticket]);

    return $result;
  }

  public static function updateStatusTicket($db, $id_ticket, $status)
  {
    $data = [];
    $data["ticket_status"] = $status;
    date_default_timezone_set('Europe/Paris');

    if($status == 4)
    {
      $data["date_end"] = date('Y-m-d H:i:s', time());
    }
    $result = $db->update($data, 'TICKETS', "id_ticket = ?", [$id_ticket]);

    return $result;
  }
}

?>
