<?php

/**
 * Used to format strings
 */
class DateFormatter
{
  /**
   * Takes a number from 0 to 6 and return a tag for the corresponding day of the week
   * @param  int $dayNo day number
   * @return string day tag
   */
  public function parseWeekDay($dayNo)
  {
    $ret = null;

    switch ($dayNo) {
      case '0':
        $ret = 'DIM';
        break;
      case '1':
        $ret = 'LUN';
        break;
      case '2':
        $ret = 'MAR';
        break;

      case '3':
        $ret = 'MER';
        break;

      case '4':
        $ret = 'JEU';
        break;

      case '5':
        $ret = 'VEN';
        break;

      case '6':
        $ret = 'SAM';
        break;
      }
    return $ret;
  }

  /**
   * Give the DateTime object of today
   * @return DateTime   Today
   */
  public function getToday()
  {
    return new DateTime(date('m/d/Y H:i:s', time()));
  }

  /**
   * Returns the number of day of the given month (typically 30 or 31)
   * @param  int $month month
   * @param  int $year  year
   * @return string the number of days
   */
  public function getMaxDaysFromMonth($month, $year)
  {
    return date('t', strtotime("01-".$month."-".$year));
  }

  /**
   * Gives a DateTime corresponding to the closest previous monday
   * @return DateTime Monday
   */
  public function getMonday()
  {
    $dayOfTheWeek = date('N');
    $dayOfTheMonth = date('j');
    $month = date('n');
    $year = date('Y');

    $mondayNo = $dayOfTheMonth - $dayOfTheWeek;

    if ($month-1 == 0) {
      $prevMonth = 12;
      $prevYear = $year-1;
    }else {
      $prevMonth = $month-1;
      $prevYear = $year;
    }

    $totalDayPreviousMonth = $this->getMaxDaysFromMonth($prevMonth, $prevYear);

    if ($mondayNo < 0) {
      $mondayNo = (($totalDayPreviousMonth + $dayOfTheMonth) - $dayOfTheWeek)+1;
      $month = $prevMonth;
      $year = $prevYear;
    }

    return (new DateTime("$year-$month-$mondayNo 00:00:00"));
  }

  /**
   * Gives an array containing a DateTime of today minus 7 days as $start and today as $end
   * @return Array Array containing the two DateTimes
   */
  public function getLast7DaysInterval()
  {
    $start = $this->getToday()->sub(new DateInterval('P6DT23H'));
    $end = $this->getToday();

    return [$start, $end];

  }

  /**
   * Produce an array containing two DateTime, the first is the closest past monday
   * and the second one is the closest past monday plus roughly 7 days (6 days and 23 hours)
   * @return Array Array containing the two DateTime
   */
  public function getThisWeekInterval()
  {
    $start = $this->getMonday();

    // Add 6 days and 23 hours
    $end = $this->getMonday()->add(new DateInterval('P6DT23H'));

    return [$start, $end];
  }

  public function getMonthInterval($qty)
  {
    $today = $this->getToday();
    $buf = clone $today;
    $end = $buf->add(new DateInterval('P'.$qty.'M'));

    return [$today, $end];
  }

  /**
   * Takes a date as input and return the same date but with the day and month swapped
   * Useful to swap between american date and european date
   * Ex : 14-05-2018 turns to 05-14-2018
   * @param  string $date the date to be parsed
   * @return string       the parsed date
   */
  public function swapDayMonth($date)
  {
    if (strpos($date, "/") > 0) {
      $delim = '/';
    }else if (strpos($date, '-') > 0) {
      $delim = '-';
    }else {
      return null;
    }

    $exploded = explode($delim, $date);
    return implode($delim, [$exploded[0], $exploded[2], $exploded[1]]);
  }

  /**
   * Returns a DateTime object of today + 7 days
   * @return DateTime Today + 7 days
   */
  public function getNextWeek()
  {
    return $this->getToday()->add(new DateInterval("P7D"));
  }

  /**
   * Add an hour to a string date
   * @param String (date) the date to update
   * @return String (date) the updated date
   */
    public function addOneHour($strDate)
    {
      $date = new DateTime($strDate);

      $date->add(new DateInterval("P1D"));

      return $date->format("Y-m-d H:i:s");
    }
  }
