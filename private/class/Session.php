<?php
/**
 * Handle session object
 */

class Session{
    public function __construct(){
      if (session_status() == PHP_SESSION_NONE) {
        session_start();
      }
    }

    /**
     * Append the content of $data to $_SESSION
     * @param  array $data data to be appended
     * @return none       /
     */
    public function append($data){
      foreach ($data as $key => $val) {
        $_SESSION[$key] = $val;
      }
    }

    /**
     * Append the content of $data to $_SESSION[$array]
     * @param  [type] $array [description]
     * @param  [type] $data  [description]
     * @return [type]        [description]
     */
    public function appendTo($array, $data){
      foreach ($data as $key => $val) {
        $_SESSION[$array][$key] = $val;
      }
    }

    /**
     * Store the user object into the $_SESSION object
     * @param  User $user user object
     * @return none       /
     */
    public function uploadUser($user){
      $_SESSION["user"] = $user;
    }

    /**
     * Fetch the User object stored in the $_SESSION["user"] variable
     * @return User User object if not null else null
     */
    public function loadUser(){
      if (isset($_SESSION["user"])) {
        return $_SESSION["user"];
      }else {
        return null;
      }
    }

    /**
     * Reset the current $_SESSION
     */
    public function reset()
    {
      $_SESSION = array();

      if (ini_get("session.use_cookies")) {
          $params = session_get_cookie_params();
          setcookie(session_name(), '', time() - 42000,
              $params["path"], $params["domain"],
              $params["secure"], $params["httponly"]
          );
      }

      session_destroy();
    }

}
