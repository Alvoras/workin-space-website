<?php
/**
 * Handle logging
 */

class Logger{
  /**
   * Adds a line containing the date, hour and the message to the a log file.
   * The file has to be located into the logs directory.
   * @param string $message
   * @param string $fileName default "info.log"
   */
    private static function logInFile($message, $filename) {
    // file_put_contents("private/logs/$filename", date("[d-m-Y|G:i:s] ").$message."\n" , FILE_APPEND);
    }

    public function log($type, $message)
    {
      switch ($type) {
        case LOG_ERROR:
          $prefix = '[!]';
          break;

        case LOG_INFO:
          $prefix = '[+]';
          break;

        case LOG_WARNING:
          $prefix = '[-]';
          break;

        default:
          $prefix = '[+]';
          break;
      }

      file_put_contents($_SERVER["DOCUMENT_ROOT"]."/private/logs/logs", $prefix.date(" [d-m-Y|G:i:s] ").$message."\n" , FILE_APPEND);
    }

    /**
     * Adds a line containing the date, hour and the message to the info log file.
     * The info log file is located in logs/info.log
     * @param string $message
    */
    public static function logInfo($message) {
      self::logInFile($message, "info.log");
    }

    /**
     * Adds a line containing the date, hour and the message to the error log file.
     * The error log file is located in logs/error.log
     * @param string $message
    */
    public static function logError($message) {
      self::logInFile($message, "error.log");
    }
}
