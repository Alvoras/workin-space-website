<?php
/**
 * Handle user object
 */

class User
{
    public $firstname;
    public $lastname;
    public $email;
    public $phone;
    private $token = null;
    public $membership;
    // private $hPwd;
    public $idSubscription;
    private $qrSalt;
    public $rentals = [];

    function __construct($data)
    {
        $wrapper = new Wrapper();
        foreach ($data as $key => $val) {
            $this->$key = $val;
        }

        // $this->hPwd = $wrapper->hashPwd($pwd);
    }

    public function getToken()
    {
      return $this->token;
    }

    public function getHPwd()
    {
      return $this->hPwd;
    }

    public function setHPwd($hPwd)
    {
      $this->hPwd = $hPwd;
    }

    public function getQrSalt()
    {
      return $this->qrSalt;
    }

    public function removeQrSalt($db)
    {
      $data["qr_salt"] = '';

      return $db->update($data, "USERS", "email = ?", [$this->email]);
    }

    public function getUserSubscriptions($db)
    {
      $data = $db->get("id_subscription, sub_start, sub_stop", "subscribe", "email = ?", [$this->email], Db::W_KEY);

      $ret = (empty($data))?null:$data[0];

      return $ret;
    }

    public function exists($db)
    {
      $res = $db->get("email", "USERS", "email = ?", [$this->email], Db::W_NUM);

      if (!empty($res)) {
        return true;
      }else {
        return false;
      }
    }

    public function matchPwd($candidate, $hPwd)
    {
      $ret = false;
      $wrapper = new Wrapper();

      if (password_verify($candidate, $hPwd)) {
        $ret = true;
      }

      return $ret;
    }

    public function append($data)
    {
        foreach ($data as $key => $val) {
            $_SESSION[$key] = $val;
        }
    }

    public function generateQrSalt($db)
    {
      $rng = random_bytes(32);
      $data["qr_salt"] = md5($rng);
      $this->qrSalt = $data["qr_salt"];

      return $db->update($data, "USERS", "email = ?", [$this->email]);
    }

    public function generateQrCode()
    {
      require_once __DIR__."/../libs/phpqrcode/qrlib.php";

      QRcode::png("$this->lastname:$this->firstname:$this->email", QRCACHE_PATH."/$this->qrSalt.png");
    }

    public function cacheQrCode($db)
    {
      $ret = false;

      if($this->generateQrSalt($db)){
        $this->generateQrCode();
        $ret = true;
      }

      return $ret;
    }

    public function loadRentals($db, $siteId)
    {
      $ret = false;

      $andAssets = '';
      $items = [];
      $items[] = $this->email;

      if ($siteId != "all") {
        $andAssets = "AND ASSETS.id_site = ?";
        $items[] = $siteId;
      }

      $andAssets = ($siteId != "all")?"AND ASSETS.id_site = ?":'';

      $data = $db->get("IF(ASSETS.room_name IS NULL, ASSETS.hardware_name, ASSETS.room_name) AS name, rent.id_rent, rent.rent_start, rent.rent_stop, rent.id_asset",
      "rent LEFT JOIN ASSETS ON rent.id_asset = ASSETS.id_asset",
      "rent.is_deleted = 0
      AND rent.email = ? $andAssets", $items, $db::W_KEY);

      if (isset($data)) {
        $ret = true;
      }

      // Clean the rentals to prevent unwanted data from previous processing
      $this->rentals = [];

      foreach ($data as $key => $val) {
        $this->rentals[$key] = $val;
        $this->rentals[$key]["start"] = date_parse($val["rent_start"]);
        $this->rentals[$key]["stop"] = date_parse($val["rent_stop"]);
        $this->rentals[$key]["duration"] = [];
        $this->rentals[$key]["duration"]["hour"] = $this->rentals[$key]["stop"]["hour"] - $this->rentals[$key]["start"]["hour"];
        $this->rentals[$key]["duration"]["minute"] = $this->rentals[$key]["stop"]["minute"] - $this->rentals[$key]["start"]["minute"];
        $assetInfo = $db->get("room_name, hardware_name", "ASSETS", "id_asset = ?", [$val["id_asset"]], $db::W_KEY)[0];
        $this->rentals[$key]["asset"]["type"] = ($assetInfo["room_name"] != NULL)?"room":"hardware";
      }

      return $ret;
    }
}
