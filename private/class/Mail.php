<?php
/**
 * Class used to create and send mails
 */

require_once __DIR__."/../libs/phpmailer/PHPMailerAutoload.php";

class Mail
{
  private $mail;
  private $email;

  function __construct($dest)
  {
    $this->email = $dest;
    $this->mail = new PHPmailer();
    $this->mail->CharSet = MAIL_CHARSET;
    $this->mail->isSMTP();
    $this->mail->Host = MAIL_HOST;
    $this->mail->SMTPAuth = SMTP_AUTH;
    $this->mail->Username = MAIL_USERNAME;
    $this->mail->Password = MAIL_PASSWORD;
    $this->mail->SMTPSecure = MAIL_SECURE;
    $this->mail->Port = MAIL_PORT;

    // $link = "http://localhost/esgfaim/php/activation.php?user=".$user."&key=".$token;

    // $this->mail->SetFrom(MAIL_SENDER, "No Reply");
    $this->mail->From = MAIL_SENDER;
    $this->mail->FromName = "Workin' Space";
    $this->mail->AddReplyTo(MAIL_USERNAME, "No Reply");
    $this->mail->AddAddress($this->email, "Destinataire");
  }

  /**
   * Abstraction of the Send method of the phpmailer lib
   * @return boolean result of the method
   */
  public function send()
  {
    return $this->mail->Send();
  }

  public function sendPwdResetMail($token, $isStaff)
  {
    $subject = "Récupération du mot de passe Workin' Space";
    $logo = "";
    $adminPrefix =  ($isStaff)?'admin.':'';
    $link = "http://".$adminPrefix."ws.hbouffier.info/index.php?a=verify&token=$token&email=$this->email";
    $body= "<body style=\"background-color:#FCFCFC; box-shadow: 0 2px 3px rgba(35,35,35,0.3);\">
        <center>
          <div style=\"background-color: #1D2633\">
            ".$logo."
          <div>
        </center>
        <center>
          <div style=\"border: 5px solid black; margin-top: 10px; margin-bottom: 20px; padding: 20px; margin-right: 25%; margin-left: 25%; background-color: rgba(106, 189, 241, 0.5); border-radius: 20px; box-shadow: 0 2px 3px rgba(35,35,35,0.3);\">
              <h3>Récupération de mot de passe</h3>
              <p>Bonjour,</p>
              <p>Vous avez demandé une réinitialisation de votre mot de passe. Si vous n'êtes pas à l'origine de cette demande, vous pouvez ignorer ce mail.</p>
              <p>Vous pouvez réinitialiser votre mot de passe en cliquant sur ce lien :</p>
              <p>".$link."</p>
            </div>
          </center>
      </body>";

      $this->mail->Subject = $subject;
      $this->mail->MsgHTML($body);
      return $this->send();
  }

  public function sendActivationMail($token, $firstname)
  {
    // $logo = "<img src=\"cid:logo\" style = \"margin: 10px;\" alt=\"logo\">";
    $subject = "Activation du compte Workin' Space";
    $logo = "";
    $link = "http://ws.hbouffier.info/index.php?a=activate&token=$token&email=$this->email";
    $body= "<body style=\"background-color:#FCFCFC; box-shadow: 0 2px 3px rgba(35,35,35,0.3);\">
        <center>
          <div style=\"background-color: #1D2633\">
            ".$logo."
          <div>
        </center>
        <center>
          <div style=\"border: 5px solid black; margin-top: 10px; margin-bottom: 20px; padding: 20px; margin-right: 25%; margin-left: 25%; background-color: rgba(106, 189, 241, 0.5); border-radius: 20px; box-shadow: 0 2px 3px rgba(35,35,35,0.3);\">
              <h3>Activation du compte</h3>
              <p>Bonjour ".$firstname."
              <p>Veuillez confirmer votre compte en cliquant sur ce lien :
              <p>".$link."
            </div>
          </center>
      </body>";

      $this->mail->Subject = $subject;
      $this->mail->MsgHTML($body);
      return $this->send();
  }

  public function sendActivationMailWithPwd($token, $firstname, $password)
  {
    // $logo = "<img src=\"cid:logo\" style = \"margin: 10px;\" alt=\"logo\">";
    $subject = "Activation du compte Workin' Space";
    $logo = "";
    $link = "http://ws.hbouffier.info/index.php?a=activate&token=$token&email=$this->email&setPassword=true";
    $body= "<body style=\"background-color:#FCFCFC; box-shadow: 0 2px 3px rgba(35,35,35,0.3);\">
        <center>
          <div style=\"background-color: #1D2633\">
            ".$logo."
          <div>
        </center>
        <center>
          <div style=\"border: 5px solid black; margin-top: 10px; margin-bottom: 20px; padding: 20px; margin-right: 25%; margin-left: 25%; background-color: rgba(106, 189, 241, 0.5); border-radius: 20px; box-shadow: 0 2px 3px rgba(35,35,35,0.3);\">
              <h3>Activation du compte</h3>
              <p>Bonjour $firstname
              <p>Veuillez confirmer votre compte en cliquant sur ce lien :
              <p>$link
              <p>
              <p>Vous allez être redirigé vers une page qui vous permettra de définir votre mot de passe définitif. Si cette méthode échoue ou si vous n'avez plus accès à cette page, connectez-vous avec votre mot de passe temporaire et définissez-en un définitif à partir de la page Paramètres de votre espace client.
              <p>
              <p>Votre mot de passe temporaire est :
              <p><b>$password</b>
            </div>
          </center>
      </body>";

      $this->mail->Subject = $subject;
      $this->mail->MsgHTML($body);
      return $this->send();
  }

  /**
   * Getter for the ErrorInfo of the phpmailer object
   * @return string  error info, if there is any
   */
  public function getError()
  {
    return $this->mail->ErrorInfo;
  }

  /**
   * Setter for the embedded image parameter
   * @param string $imgPath path to the image to bind to the mail
   */
  public function addImg($imgPath)
  {
    $title = explode($imgPath);
    $title = $title[count($title)-1];
    $mail->AddEmbeddedImage($imgPath, $title);
  }
}
