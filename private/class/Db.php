<?php
/**
 * Handle database manipulation
 */

class Db
{
    private static $inst = null;
    private static $db = null;

    // CONST
    const W_KEY = PDO::FETCH_ASSOC;
    const W_NUM = PDO::FETCH_NUM;

    public static function getInstance()
    {
      if (is_null(self::$inst)) {
        self::$inst = new Db();
      }

      return self::$inst;
    }

    private function __construct()
    {
        try {
            self::$db = new PDO("mysql:host=".DB_HOST.";charset=utf8;dbname=".DB_NAME, DB_USER, DB_PWD);
        } catch (Exception $e) {
            die("ERREUR sql : ".$e->getMessage());
        }
    }

    /**
     * Abstraction of the MySQL select statement for easier handling
     * @param  string $what     the column names to be retrieved
     * @param  string $from     the table of the FROM statement
     * @param  string $whereArg the where statement
     * @param  string $arrayType  Db::W_KEY or Db::W_NUM
     * These are two constants that equals to respectiely PDO::FETCH_ASSOC and
     * PDO::FETCH_NUM. W_KEY makes the return array contain named keys according to their
     * respective column name
     * @return Array           If any, the results of the query else an empty array
     */
    public function get($what, $from, $whereArg, $items, $arrayType)
    {
        $rows = [];
        $where = $this->setWhere($whereArg);

        $query = self::$db->prepare("SELECT $what FROM $from $where");
        $result = $query->execute($items);

        if ($result) {
            while ($row = $query->fetch($arrayType)) {
                $rows[] = $row;
            }
        }

        // Array
        return $rows;
    }

    /**
     * Ping the db to see if a row exists
     * @param  string $what  column(s) to ping
     * @param  string $from  from which table
     * @param  string $where where statement
     * @return boolean        returns true if row exists else false
     */
    public function exists($what, $from, $where, $items)
    {
      $res = $this->get($what, $from, $where, $items, Db::W_NUM);

      return (!empty($res))?true:false;
    }

    /**
     * Update the is_activated column to 1 according to the given email
     * @param  string $email email primary key to look for
     * @return boolean        true if the query is successful else false
     */
    public function activateAccount($email)
    {
      $data = [];
      $data["is_activated"] = 1;

      return $this->update($data, "USERS", "email = ?", [$email]);
    }

    /**
     * Remove the token of the given user after use
     * @param  string $email email primary key to look for
     * @return boolean        true if the query is successful else false
     */
    public function removeToken($email)
    {
      $data = [];
      $data["token"] = ' ';



      return $this->update($data, "USERS", "email = ?", [$email]);
    }

    /**
     * Update the token of the given user with the given value
     * @param  string $email email primary key to look for
     * @param  string $token token value
     * @return boolean        true if the query is successful else false
     */
    public function updateToken($email, $token)
    {
      $data = [];
      $data["token"] = $token;

      return $this->update($data, "USERS", "email = ?", [$email]);
    }

    /**
     * Fetch the token of the given user and tells if it match the given candidate token
     * @param  string $email     email of the user to look for
     * @param  string $candidate candidate token
     * @return boolean            true if token match else false
     */
    public function evalToken($email, $candidate)
    {
      $token = $this->get("token", "USERS", "email = ?", [$email], Db::W_NUM)[0][0];
      return ($token === $candidate)?true:false;
    }

    /**
     * Abstraction of the MySQL insert statement
     * @param  string $what values to be inserted
     * @param  string $from placeholders
     * @return boolean       true if query is successful else false
     */
     public function insert($what, $whereArg)
     {
         $query = self::$db->prepare("INSERT INTO $what");
         $result = $query->execute($whereArg);

         // True or False
         return $result;
     }
  /*
    --- HS ---
    public function insert($table, $what, $values)
    {
        $query = $this->db->prepare("INSERT INTO $table ($what) VALUES ($what)", $values);
        $result = $query->execute();

        // True or False
        return $result;
    }*/

    /**
     * Fetch the rentals of the given site for the given item in the given time frame
     * @param  string $name   item name
     * @param  string $siteId site ID
     * @param  string $start  start of the time frame formated as 'Y-m-d H:00:00'
     * @param  string $end    end of the time frame formated as 'Y-m-d H:00:00'
     * @return Array         Results of the query
     */
    public function getRentals($name, $siteId, $start, $end)
    {
      return $this->get(
        "IF(ASSETS.room_name IS NULL, ASSETS.hardware_name, ASSETS.room_name) AS name, rent.rent_start, rent.rent_stop",
        "ASSETS LEFT JOIN rent ON rent.id_asset = ASSETS.id_asset",
        "ASSETS.id_site = ?
        AND (ASSETS.hardware_name = ? OR ASSETS.room_name = ?)
        AND rent.is_deleted = 0
        AND rent.rent_start > ?
        AND rent.rent_stop <= ?",
        [
          $siteId,
          $name,
          $name,
          $start,
          $end
        ],
        Db::W_KEY
      );
    }

    /**
     * Fetch the rentals of the given site for the given item in the given time frame
     * @param  string $name   item name
     * @param  string $siteId site ID
     * @param  string $start  start of the time frame formated as 'Y-m-d H:00:00'
     * @param  string $end    end of the time frame formated as 'Y-m-d H:00:00'
     * @return Array         Results of the query
     */
    public function getSiteRentals($siteId, $start, $end)
    {
      return $this->get(
        "IF(ASSETS.room_name IS NULL, ASSETS.hardware_name, ASSETS.room_name) AS name, rent.rent_start, rent.rent_stop",
        "ASSETS LEFT JOIN rent ON rent.id_asset = ASSETS.id_asset",
        "ASSETS.id_site = ?
        AND rent.is_deleted = 0
        AND rent.rent_start > ?
        AND rent.rent_stop <= ?",
        [
          $siteId,
          $start,
          $end
        ],
        Db::W_KEY
      );
    }

    public function getSiteRentalCount($siteId, $type, $start, $end)
    {
      return $this->get(
        "IF(ASSETS.room_name IS NULL, ASSETS.hardware_name, ASSETS.room_name) AS name, COUNT(rent.rent_start) AS qty",
        "ASSETS LEFT JOIN rent ON rent.id_asset = ASSETS.id_asset",
        "ASSETS.id_site = ?
        AND ".$type."_name IS NOT NULL
        AND rent.is_deleted = 0
        AND rent.rent_start > ?
        AND rent.rent_stop <= ?
        GROUP BY name ASC
        ",
        [
          $siteId,
          $start,
          $end
        ],
        Db::W_KEY
      );
    }

    public function getAvailableAssetsInSite($siteId, $type)
    {
      return $this->get("IF(ASSETS.room_name IS NULL, ASSETS.hardware_name, ASSETS.room_name) AS name",
      "ASSETS LEFT JOIN rent ON rent.id_asset = ASSETS.id_asset",
      "ASSETS.id_site = ?
      AND ".$type."_name IS NOT NULL
      AND rent.is_deleted = 0",
      [
        $siteId
      ],
      Db::W_KEY
      );
    }

    /**
     * Returns the id(s) of the rented assets with the given name in the given site
     * and within the given timeframe
     * @param  String $name   Name of the asset to lookup
     * @param  int $siteId Site id
     * @param  String (date) $start  start of the timeframe
     * @param  String (date) $end    end of the timeframe
     * @return Array         array containing the assets' id
     */
    public function getUnavailableItems($name, $siteId, $start, $end)
    {
      $data = [];

      $res = $this->get(
        "rent.id_asset",
        "ASSETS LEFT JOIN rent ON rent.id_asset = ASSETS.id_asset",
        "(ASSETS.hardware_name = ? OR ASSETS.room_name = ?)
        AND ASSETS.id_site = ?
        AND rent.is_deleted = 0
        AND ASSETS.is_deleted = 0
        AND
          (
            (? <= rent.rent_start
            && (? > rent.rent_start && ? <= rent.rent_stop))
          OR (
            (? >= rent.rent_start && ? < rent.rent_stop)
            && ? >= rent.rent_stop)
          OR (? <= rent.rent_start && ? > rent.rent_stop)
          OR (? >= rent.rent_start && ? < rent.rent_stop)
          )",
          [
            $name,
            $name,
            $siteId,
            $start,
            $end,
            $end,
            $start,
            $start,
            $end,
            $start,
            $end,
            $start,
            $end
          ],
          Db::W_KEY
        );


      foreach ($res as $key => $val) {
        $data[] = $val["id_asset"];
      }

      return $data;
    }

    /**
     * Fetch all the asset_id contained in the given site with the given name
     * @param String $name   Asset name
     * @param int $siteId Site id
     */
    public function getAssetIdsInSite($name, $siteId)
    {
      return $this->get(
        "id_asset",
        "ASSETS",
        "(hardware_name = ? OR room_name = ?)
        AND is_deleted = 0
        AND id_site = ?",
        [
          $name,
          $name,
          $siteId
        ],
        Db::W_KEY
      );
    }

    /**
     * List all the unavaible rented item in the given time frame,
     * compares it to the list of items in the given site and return the first
     * available one
     * @param String $name   Asset name
     * @param int $siteId Site Id
     * @param String (date) $start  start of the timeframe
     * @param String (date) $end    end of the timeframe
     */
    public function getFirstAvailableAsset($name, $siteId, $start, $end)
    {
      $assetId = false;
      $unavailable = $this->getUnavailableItems($name, $siteId, $start, $end);
      $items = $this->getAssetIdsInSite($name, $siteId);

      foreach ($items as $key => $itemId) {
        // If the current id_asset is not in the unavaible item list, then it is
        // available
        if (!in_array($itemId["id_asset"], $unavailable)) {
          $assetId = $itemId["id_asset"];
          break;
        }
      }

      return $assetId;
    }

    /**
     * Insert a rental with the given values in db
     * @param Array $dates    array containing the start date as [0] and the second as [1]
     * @param string $email    email of the renter
     * @param string $itemName item name
     * @param int $siteId   site ID
     * @return boolean         true if the query is successful else false
     */
    public function addRental($start, $end, $email, $itemName, $siteId, $assetId, $rentToken)
    {
      $ret = false;

      $query = self::$db->prepare("INSERT INTO rent(rent_start, rent_stop, email, id_asset, rent_token)
      VALUES('$start', '$end', '$email', $assetId, '$rentToken')");
      $ret = $query->execute();

      return $ret;
  }

  /**
   * Mark the given rental as deleted
   * @param  int $rentId the id of the rental to be deleted
   * @return boolean         Query result
   */
  public function removeRental($rentId)
  {
    $data = [];
    $data["is_deleted"] = 1;

    return $this->update($data, "rent", "id_rent = ?", [$rentId]);
  }

  /**
   * Set the password of the given user to the given value
   * @param  string $email      email of the user to look for
   * @param  string $wrappedPwd BCRYPT hash of the user's password
   * @return boolean             true if query is successful else false
   */
  public function updatePwd($email, $wrappedPwd)
  {
    $data = [];
    $data["password"] = $wrappedPwd;

    return $this->update($data, "USERS", "email = ?", [$email]);
  }

  /**
   * Abstraction for the update MySQL statement
   * @param  Array $what     key => value array containing the value the be set at the
   * corresponding key (column names)
   * @param  string $from     table where the update should take place
   * @param  string $whereArg where statement
   * @return boolean             true if query is successful else false
   */
    // public function update($what, $from, $whereArg)
    // {
    //     $where = self::setWhere($whereArg);
    //
    //     $query = "UPDATE $from SET ";
    //     $values = [];
    //
    //     foreach ($what as $key => $val) {
    //         /*if(is_str($val)){
    //           $val = "'$val'";
    //         }*/
    //         if (is_string($val)) {
    //           $values[] = "$key='$val' ";
    //         }else {
    //           $values[] = "$key=$val ";
    //         }
    //     }
    //
    //     $concatValues = join(", ", $values);
    //
    //     $query = $query.$concatValues.$where;
    //
    //     $query = self::$db->prepare($query);
    //
    //
    //     $result = $query->execute();
    //
    //     // True or False
    //     return $result;
    // }

    public function update($what, $from, $whereArg, $whereTokens)
    {
        $where = self::setWhere($whereArg);

        $bufTokens = [];

        $query = "UPDATE $from SET ";
        $values = [];

        foreach ($what as $key => $val) {
            $values[] = "$key = ?";
            $bufTokens[] = $val;
        }
        $tokens = array_merge($bufTokens, $whereTokens);

        $concatValues = join(", ", $values);

        $query = $query.$concatValues.' '.$where;

        $query = self::$db->prepare($query);

        $result = $query->execute($tokens);
        // True or False
        return $result;
    }

    /**
     * Abstration for custom queries the does not fit other abstractions
     * @param  string $request      MySQL query
     * @param  boolean $returnResult if true, then the function will return the query's results
     * else it will not
     * @param  int $listing      Db::W_KEY or db::W_NUM (see get())
     * @return Array / boolean   if $returnResult is true, then returns the query's results, else
     * the function returns the success state of the query
     */
    public function query($request, $returnResult, $listing)
    {
        $query = $this->db->prepare($request);
        $result = $query->execute();

        if ($returnResult) {
            while ($row = $query->fetch($listing)) {
                $rows[] = $row;
            }

            // Array
            return $rows;
        } else {
            // True or False
            return $result;
        }
    }

    /**
     * Used to correctly format the where statement and allow him to be optional
     * (by passing null as $where argument)
     * @param string $whereArg if $whereArg contains something, then return the formated
     * string, else null
     */
    private static function setWhere($whereArg)
    {
        return ($whereArg == null)? "" : "WHERE $whereArg" ;
    }


    /**
     * Registers a user into the database. Returns true if the registration succeeds, false otherwise.
     * @param string $password
     * @param string $lastname
     * @param string $firstname
     * @param string $email
     * @param string $phone
     * @return boolean success
    */
    public function registerUser($password, $lastname, $firstname, $email, $phone, $token)
    {
        $logger = new Logger();
        $req = self::$db->prepare('INSERT INTO USERS (password, lastname, firstname, email, phone, token) VALUES(?, ?, ?, ?, ?, ?);');
        $success = $req->execute(array($password, $firstname, $lastname, $email, $phone, $token));

        if (!$success) {
            $logger->log(LOG_ERROR, "Could not register user: $email\n\t".$req->errorInfo()[0]." : ". $req->errorInfo()[2]);
            $req->closeCursor();
        }

        return $success;
    }

    /**
    * Connects a user. Returns false if the email and password combination is not correct.
    * @param string $email
    * @param string $password
    * @return boolean success
   */
    public function connectUser($email, $password)
    {
      $res = false;
      $req = $this->get("password", "USERS", "email = ? AND is_deleted = 0 AND is_activated = 1", [$email], Db::W_KEY); //Checking if password matches the email and the account is not deleted

      $hPassword = $req[0]["password"];

      if (count($req) > 0) {
        if (password_verify($password, $hPassword)) {
          $res = true;
        }
      }

      return $res;

        // $req = self::$db->prepare('SELECT password FROM USERS WHERE email = ? AND is_deleted = 0 AND is_activated = 1'); //Checking if password matches the email and the account is not deleted
        // $ret = $req->execute(array($email));
        // if (password_verify($password, $req->fetch()["password"])) {
        //     $req = self::$db->prepare('SELECT email FROM USERS WHERE email = ?');
        //     $req->execute(array($email));
        //
        //
        //     if ($req->rowCount() != 0) { //Email and password combination is correct
        //         $req->closeCursor();
        //         return true;
        //     }
        // }
        // return false;
    }

    /**
     * Gives the default site schedule, aka the site 1 (République) schedule
     * @return Array array of the corresponding opening hours
     */
    public function getDefaultSiteSchedule()
    {
      $defaultSite = 1;
      return $this->get("day, is_open, site_open, site_close", "SCHEDULE", "id_site = ?", [$defaultSite], self::W_KEY);
    }

    /**
     * Gives the schedule for the given site
     * @param  int $id site ID
     * @return Array     schedule of the given site
     */
    public function getSiteSchedule($id)
    {
      return $this->get("day, is_open, site_open, site_close", "SCHEDULE", "id_site = ?", [$id], self::W_KEY)[0];
    }

    /**
     * Fetch everything from the given site
     * @return Array array containing all the data in the SITE table
     */
    public function getSitesData()
    {
      return $this->get("*", "SITE", null, null, Db::W_KEY);
    }

    /**
     * Fetch everything from the given site
     * @param  String $email staff email
     * @return Array array containing the data in the STAFF table of the given email
     */

    public function getStaffData($email)
    {
      return $this->get("*", "STAFF", "email = ?", [$email], Db::W_KEY);
    }

    /**
     * Fetch everything from the given site
     * @return Array array containing all different status of tickets
     */

    public function getTicketTypes()
    {
      return $this->get("*", "TICKET_TYPES", null, null, Db::W_KEY);
    }

    /**
     * Argument-triggered switch to fetch either the room or hardware content of the
     * given site
     * @param  int $siteId site ID
     * @param  string $type   mode that indicate which asset type to fetch
     * @return Array  query results
     */
    public function getAssetsInSite($siteId, $includeDeleted, $type)
    {
      switch ($type) {
        case 'room':
          return $this->getRoomsInSite($siteId, $includeDeleted);
          break;

        case 'hardware':
          return $this->getHardwareInSite($siteId, $includeDeleted);
          break;

        default:
      }
    }

    /**
     * Fetch all the rooms results from the given site
     * @param  int $siteId site ID
     * @return Array         query results
     */
    public function getRoomsInSite($siteId, $includeDeleted)
    {
      $is_deleted = ($includeDeleted)?'':"AND ASSETS.is_deleted = 0";

      return $this->get("DISTINCT(ROOM.name), ROOM.3D_texture, ROOM.price, ROOM.description, ROOM.img, ROOM.seats, ASSETS.is_deleted",
      "ROOM LEFT JOIN ASSETS ON ASSETS.room_name = ROOM.name",
      "ASSETS.id_site = ?
      $is_deleted
      AND ASSETS.hardware_name IS NULL
      ORDER BY ROOM.price DESC", [$siteId], Db::W_KEY);
    }

    /**
     * Fetch all the hardware results from the given site
     * @param  int $siteId site ID
     * @return Array         query results
     */
    public function getHardwareInSite($siteId, $includeDeleted)
    {
      $is_deleted = ($includeDeleted)?'':"AND ASSETS.is_deleted = 0";

      return $this->get("DISTINCT(HARDWARE.name), HARDWARE.3D_texture, HARDWARE.price, HARDWARE.description, HARDWARE.img, ASSETS.is_deleted",
      "HARDWARE LEFT JOIN ASSETS ON ASSETS.hardware_name = HARDWARE.name",
      "ASSETS.id_site = ?
      $is_deleted
      AND ASSETS.room_name IS NULL
      ORDER BY HARDWARE.price DESC", [$siteId], Db::W_KEY);
    }

    /**
     * Fetch all rentals of the the given item from the given site in the given timeframe
     * @param  string $start  date formated as Y-m-d H:00:00
     * @param  string $stop   date formated as Y-m-d H:00:00
     * @param  int $site   site ID
     * @param  string $name   item name
     * @param  string $target define what type of asset we want to fetch the results for
     * @return Array         results of the query
     */
    public function fetchAllRentals($start, $stop, $site, $name, $target)
    {
      // if (strpos($name, "'") && strpos($name, "\\'") == false) {
      //   $name = str_replace("'", "\\'", $name);
      // }

      return $this->get(
        "rent.id_asset, rent.rent_start, rent.rent_stop, ASSETS.".$target."_name AS name",
        "rent LEFT JOIN ASSETS ON ASSETS.id_asset = rent.id_asset",
        "ASSETS.id_site = ? AND ASSETS.".$target."_name = ?
        AND rent.is_deleted = 0
        AND rent.rent_start >= ?
        AND rent.rent_stop <= ?",
        [
          $site,
          $name,
          $start->format("Y-m-d H:00:00"),
          $stop->format("Y-m-d H:00:00")
        ],
        self::W_KEY
      );
    }

    /**
     * Get the total qty of the given item in the given site
     * @param string $name   item name
     * @param string $date   date -- UNUSED ?
     * @param int $site   site ID
     * @param string $target asset type
     * @return int  Quantity of item found
     */
    public function getAssetQty($name, $site, $target)
    {
      // if (strpos($name, "'") && strpos($name, "\\'") == false) {
      //   $name = str_replace("'", "\\'", $name);
      // }

      $data = $this->get(
        "COUNT(id_asset)",
        "ASSETS",
        "id_site = ?
        AND ASSETS.is_deleted = 0
        AND (ASSETS.hardware_name = ? OR ASSETS.room_name = ?)",
        [
          $site,
          $name,
          $name
        ],
        self::W_NUM
      );

      return (isset($data[0][0]))?$data[0][0]:$data;
    }

    /**
     * Get the quantity left (not rented) of the given item in the given site within
     * the given timeframe
     * @param string $name   item name
     * @param string $date   formated as Y-m-d H:i:s
     * @param int $site   site ID
     * @param string $target asset type
     */
    public function getAssetLeft($name, $date, $site, $target)
    {
      // if (strpos($name, "'") && strpos($name, "\\'") == false) {
      //   $name = str_replace("'", "\\'", $name);
      // }

      // Cherche dans la table rent pour chaque item où name = $name et id_site = $site
      // si $date est compris entre rent_start et rent_stop
      // Retourner le compte total

      return $this->get(
        "COUNT(rent.id_asset)",
        "rent LEFT JOIN ASSETS ON ASSETS.id_asset = rent.id_asset",
        "ASSETS.id_site = ?
        AND ASSETS.is_deleted = 0
        AND (ASSETS.hardware_name = ? OR ASSETS.room_name = ?)
        AND rent.is_deleted = 0
        AND (? >= rent.rent_start
        AND ? <= rent.rent_stop)",
        [
          $site,
          $name,
          $name,
          $date->format("Y-m-d H:i:s"),
          $date->format("Y-m-d H:i:s")
        ],
        self::W_NUM
      );
    }

    /**
     * Create a new row in the AUTH_TOKENS table for the remember me feature
     * @param  String $email email of the new user
     * @return boolean        true if the query is successful else false
     */
    public function createAuthTokenRow($email)
    {
      $query = "INSERT INTO AUTH_TOKENS (email) VALUES ( ? )";
      $query = self::$db->prepare($query);
      $result = $query->execute([$email]);

      return $result;
    }

    /**
     * Update the remember me info for the given user
     * @param  String $selector   temporary identifier mirrored in the user's cookie
     * @param  String $hValidator Hashed user's cookie validator
     * @param  String $email      Email of the user
     * @param  String (date) $expires    Expiration date of the remember me cookie
     * @return boolean             Success of the query
     */
    public function updateAuthTokens($selector, $hValidator, $email, $expires)
    {
      // Update the corresponding email row
      $data = [];
      $data["selector"] = $selector;
      $data["hashed_validator"] = $hValidator;
      $data["expires"] = $expires;

      return $this->update($data, "AUTH_TOKENS", "email = ?", [$email]);
    }

    /**
     * Fetch the remember me info stored in the AUTH_TOKENS table for the given user
     * @param  String $selector Temporary identifier mirrored in the user's cookie
     * @return Array           Data stored if any
     */
    public function fetchAuthInfo($selector)
    {
      $data = $this->get("hashed_validator, expires", "AUTH_TOKENS", "selector = ?", [$selector], Db::W_KEY);
      return (isset($data[0]))?$data[0]:$data;
    }

    /**
     * Fetch the given user info
     * @param  String $email email of the user
     * @return Array        Data stored if any
     */
    public function fetchUserInfo($email)
    {
      $data = $this->get("lastname, firstname, email, phone", "USERS", "email = ?", [$email], Db::W_KEY);
      return (isset($data[0]))?$data[0]:$data;
    }

    /**
     * Fetch the given user's membership infos
     * @param  String $email user's email
     * @return Array        Data stored if any
     */
    public function fetchUserMembership($email)
    {
      $data = $this->get("is_commited, sub_start, sub_stop, id_subscription", "subscribe", "email = ?", [$email], Db::W_KEY);

      return (isset($data[0]))?$data[0]:$data;
    }

    /**
     * Fetch the email which is bound to the given selector
     * @param  String $selector temporary identifier mirrored in the user's cookie
     * @return String           email of the user if found
     */
    public function getEmailFromSelector($selector)
    {
      $data = $this->get("email",
      "AUTH_TOKENS",
      "selector = ?", [$selector], Db::W_KEY);
      return (isset($data[0]))?$data[0]["email"]:$data;
    }

    public function fetchUserPwdHash($email)
    {
      return $this->get("password", "USERS", "email = ?", [$email], Db::W_KEY)[0]["password"];
    }

    public function connectStaff($email, $password, $type)
    {
      $res = false;
      $req = $this->get("password", "USERS LEFT JOIN STAFF ON USERS.email = STAFF.email", "
      USERS.email = ?
      AND USERS.is_deleted = 0
      AND USERS.is_activated = 1
      AND STAFF.id_staff_type = (
        SELECT DISTINCT(STAFF_TYPES.id_staff_type)
        FROM STAFF_TYPES
          LEFT JOIN STAFF ON STAFF_TYPES.id_staff_type = STAFF.id_staff_type
        WHERE STAFF_TYPES.name_staff_type = ?)",
      [$email, $type],
      Db::W_KEY); //Checking if password matches the email and the account is not deleted

// select USERS.password
// from USERS LEFT JOIN STAFF ON USERS.email = STAFF.email
// where USERS.email = 'hadrien.bouffier@gmail.com'
// AND USERS.is_deleted = 0 AND USERS.is_activated = 1
// AND STAFF.id_staff_type =
// ( SELECT DISTINCT(STAFF_TYPES.id_staff_type)
//   FROM STAFF_TYPES LEFT JOIN STAFF ON STAFF_TYPES.id_staff_type = STAFF.id_staff_type
//   WHERE STAFF_TYPES.name_staff_type = 'Manager')

      if (count($req) > 0) {
        $hPassword = $req[0]["password"];
        if (password_verify($password, $hPassword)) {
          $res = true;
        }
      }

      return $res;
    }

    public function getUserSubscriptionData($email)
    {
      return $this->get("SUBSCRIPTIONS.subscription_name, subscribe.is_commited",
      "SUBSCRIPTIONS LEFT JOIN subscribe ON SUBSCRIPTIONS.id_subscription = subscribe.id_subscription",
      "subscribe.email = ? AND subscribe.sub_stop > NOW()
      ORDER BY subscribe.sub_stop DESC
      LIMIT 1 ",
      [$email],
      Db::W_KEY);
    }

    public function loadUsers()
    {
      return $this->get("firstname, lastname, email, phone, is_activated, is_deleted", "USERS", null, null, Db::W_KEY);
    }

    public function searchUser($data)
    {
      if ($data !== "&all") {
        $tokens = [];
        $where = [];
        foreach ($data as $key => $val) {
          $where[] = "$key LIKE ?";
          $tokens[] = '%'.$val.'%';
        }

        $whereStr = implode(" or ", $where);
      }else {
        $whereStr = '';
        $tokens = [];
      }

      return $this->get("firstname, lastname, email, phone, is_activated, is_deleted", "USERS", $whereStr, $tokens, Db::W_KEY);
    }

    public function searchAssets($data, $table, $siteId)
    {
      $tableCaps = strtoupper($table);
      $seats = ($table === "room")?", ROOM.seats":'';

      if ($data !== "&all") {
        $tokens = [];
        $where = [];
        foreach ($data as $key => $val) {
          $where[] = "$key LIKE ?";
          $tokens[] = '%'.$val.'%';
        }

        $whereStr = implode(" or ", $where);
      }else {
        $whereStr = '';
        $tokens = [];
      }

      $tokens[] = $siteId;

      return $this->get("IF(ASSETS.room_name IS NULL, ASSETS.hardware_name, ASSETS.room_name) AS name, $tableCaps.price, $tableCaps.img, $tableCaps.description, ASSETS.id_asset, ASSETS.is_deleted $seats",
      "ASSETS LEFT JOIN $tableCaps ON ASSETS.".$table."_name = $tableCaps.name",
      $whereStr."
      AND ".$table."_name IS NOT NULL
      AND id_site = ?
      ",
      $tokens,
      Db::W_KEY);
    }

    public function loadSitesVisits($start, $stop)
    {
      return $this->get("SITE.site_name, visit.id_site, COUNT(visit.date_in) AS qty",
      "visit LEFT JOIN SITE ON visit.id_site = SITE.id_site",
      "visit.date_in > ?
      AND visit.date_out < ? GROUP BY id_site ASC",
      [$start, $stop],
      Db::W_KEY);
    }

    public function getAssetItemsInSite($siteId, $table)
    {
      $tableCaps = strtoupper($table);
      $seats = ($table === "room")?", ROOM.seats":'';

      return $this->get("IF(ASSETS.room_name IS NULL, ASSETS.hardware_name, ASSETS.room_name) AS name, $tableCaps.price, $tableCaps.img, $tableCaps.description, ASSETS.id_asset, ASSETS.is_deleted $seats",
      "ASSETS LEFT JOIN $tableCaps ON ASSETS.".$table."_name = $tableCaps.name",
      $tableCaps."_name IS NOT NULL
      AND id_site = $siteId
      ",
      null,
      Db::W_KEY);
    }

    public function addAssetCategory($table, $name, $description, $price, $img)
    {
      $tokens = [$name, $description, $price, $img];

      $tableCaps = strtoupper($table);
      $seats = ($table === "room")?", ROOM.seats":'';

      $req = self::$db->prepare("INSERT INTO $tableCaps(name, description, price, img)
      VALUES (?, ?, ?, ?)");

      return $req->execute($tokens);
    }

    public function addAsset($siteId, $name, $table)
    {
      $tokens = [$name, $siteId];

      $tableCaps = strtoupper($table);

      $req = self::$db->prepare("INSERT INTO ASSETS(".$table."_name, id_site)
      VALUES (?, ?)");

      return $req->execute($tokens);
    }

    public function getInfo($name, $table)
    {
      $tableCaps = strtoupper($table);

      return $this->get("$tableCaps.*, ASSETS.id_asset",
      "$tableCaps LEFT JOIN ASSETS on $tableCaps.name = ASSETS.".$table."_name",
      "ASSETS.".$table."_name = ?
      ORDER BY ASSETS.id_asset DESC LIMIT 1",
       [$name],
       Db::W_KEY)[0];
    }

    public function subscribeTo($email, $subId, $start, $end)
    {
      $req = self::$db->prepare("INSERT INTO subscribe(email, id_subscription, sub_start, sub_stop)
      VALUES(?, ?, ?, ?)");

      $res = $req->execute([$email, $subId, $start, $end]);

      return $res;
    }

    public function renewSubscription($email, $end)
    {
      $data = [];
      $data["sub_stop"] = $end;
      return $this->update($data, "subscribe", "email = ?", [$email]);
    }

    public function updateSubscription($data)
    {
      // $this->update($data, "")
    }
}
