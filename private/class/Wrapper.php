<?php
/**
 * Handle encryption and hash operations
 */

class Wrapper{
  public function __construct()
  {

  }

  /**
   * Abstraction for the BCRYPT hash function
   * @param  string $pwd user password
   * @return string      hashed password
   */
  public function wrapPwd($pwd)
  {
    return password_hash($pwd, PASSWORD_BCRYPT);
  }

  /**
   * Abstraction for the sha256 hash function
   * @param  string $pwd password
   * @return string      hashed password
   */
  public function hash256($pwd)
  {
    return hash("sha256", $pwd, false);
  }

  /**
   * Generate the hPwd, which is done by BCRYPTing the sha256 of the given password
   * @param  [type] $pwd [description]
   * @return [type]      [description]
   */
  public function hashPwd($pwd)
  {
    return password_hash(hash("sha256", $pwd, false), PASSWORD_BCRYPT);
  }

  public function wrapValidator($validator)
  {
    return $this->hash256($validator);
  }

  public function evalValidator($hValidator, $candidate)
  {
    $hCandidate = $this->hash256($candidate);

    // Using hash_equals allows to counter timing attacks
    return hash_equals($hValidator, $hCandidate);
  }

  public function generateSelector()
  {
    return uniqid();
  }

  public function generateValidator()
  {
    return md5(random_bytes(32));
  }

  /**
   * Generate psedo rendom token of length 32
   * @return string token
   */
  public function generateToken()
  {
    $rng = random_bytes(32);
    $token = md5($rng);

    return $token;
  }

  public function generateTempPwd()
  {
    $pool = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN123456789$!?:-_}=+";
    $randNb;
    $pwd = '';

    for ($i = 0; $i < 16; $i++) {
        $rng = mt_rand(0,69);
        $pwd .= $pool[$rng];
}
    return $pwd;
  }
}
