<?php

/**
 * Errors to be spawned
 */
class Err
{
  private $msg;

  function __construct($text)
  {
    $data["errMsg"] = $text;

    $this->msg = $data;
  }

  public function load()
  {
    if (!isset($_SESSION["errors"])) {
    	$_SESSION["errors"] = [];
    }

    $_SESSION["errors"][] = $this->msg;
  }

  public static function clean()
  {
    $_SESSION["errors"] = [];
  }
}
