<?php
$root = pathinfo($_SERVER['SCRIPT_FILENAME']);
define('BASE_FOLDER', basename($root['dirname']));
define('SITE_URL', 'https://'.$_SERVER['HTTP_HOST'].'/'.BASE_FOLDER);
define('SITE_ROOT', realpath($root['dirname']));

define("DB_USER", 'workin-space');
define("DB_PWD", 'toor');
define("DB_HOST", "localhost");
define("DB_NAME", "workin-space");

define('QRCACHE_PATH', 'private'.'/qrcache');
define('INCLUDE_LIBS', 'private'.'/libs');
define('INCLUDE_CLASS', 'private'.'/class');
define('INCLUDE_SCRIPT', 'private'.'/scripts');
define('INCLUDE_MODEL', 'private'.'/model');
define('FRONT_VIEWS_PATH', 'public'.'/front'.'/views');
define('BACK_VIEWS_PATH', 'public'.'/back'.'/views');
define('GLOBAL_VIEWS_PATH', 'public'.'/global'.'/views');
define('INCLUDE_FRONT_VIEWS', 'public'.'/front'.'/views'.'/include');
define('INCLUDE_BACK_VIEWS', 'public'.'/back'.'/views'.'/include');
define('INCLUDE_GLOBAL_VIEWS', 'public'.'/global'.'/views');
define('INCLUDE_GLOBAL_CSS', 'public'.'/global'.'/css');
define('INCLUDE_FRONT_JS', 'public'.'/front'.'/js');
define('INCLUDE_BACK_JS', 'public'.'/back'.'/js');
define('INCLUDE_GLOBAL_JS', 'public'.'/global'.'/js');
define('INCLUDE_FRONT_CSS', 'public'.'/front'.'/css');
define('INCLUDE_BACK_CSS', 'public'.'/back'.'/css');
define('IMG_FRONT_ROOT', 'public'.'/front'.'/img');
define('IMG_BACK_ROOT', 'public'.'/back'.'/img');
define('INCLUDE_BACK_IMG', 'public'.'/back'.'/img');
define('INCLUDE_GLOBAL_IMG', 'public'.'/global'.'/img');
define('MASTER_SECRET', '123456');

define('ADMIN_VIEWS_PATH', 'public'.'/views');
define('INCLUDE_ADMIN_VIEWS', 'public'.'/views'.'/include');
define('INCLUDE_ADMIN_JS', 'public'.'/js');
define('INCLUDE_ADMIN_JS_GLOBAL', 'public'.'/js'.'/global');
define('INCLUDE_ADMIN_JS_GLOBAL_LIB', 'public'.'/js'.'/global'.'/lib');
define('INCLUDE_ADMIN_CSS', 'public'.'/css');
define('INCLUDE_ADMIN_CSS_GLOBAL', 'public'.'/css'.'/global');
define('INCLUDE_ADMIN_CSS_LIB', 'public'.'/css'.'/lib');
define('INCLUDE_ADMIN_GLOBAL_JS', 'public'.'/js'.'/global');
define('INCLUDE_ADMIN_MODEL', 'private'.'/model');

define('MAIL_SENDER', "noreply.workinspace@gmail.com");
define('MAIL_CHARSET', 'UTF-8');
define('MAIL_HOST', 'smtp.gmail.com');
define('SMTP_AUTH', 'true');
define('MAIL_USERNAME', 'noreply.workinspace@gmail.com');
define('MAIL_PASSWORD', 'j@XErk9]s=&9m6-S');
define('MAIL_SECURE', 'ssl');
define('MAIL_PORT', 465);

define('RENT_TOKEN_SALT', 'ba7816bf8f01cfea414140dea9cb410ff61f20015ad');

define('CAPTCHA_KEY', '6Ld0bk0UAAAAAE2lfgJLj_OgMotHDeiZwRLQfCHe');

if (!defined('LOG_ERROR')) define('LOG_ERROR', 0);
if (!defined('LOG_INFO')) define('LOG_INFO', 1);
if (!defined('LOG_WARNING')) define('LOG_WARNING', 2);

$config = [];

$config["libs"]["toolbox"] = INCLUDE_LIBS.'/toolbox.php';
$config["libs"]["errors"] = INCLUDE_LIBS.'/errors.php';

$config["class"]["path"] = INCLUDE_CLASS.'/Path.php';
$config["class"]["warden"] = INCLUDE_CLASS.'/Warden.php';
$config["class"]["db"] = INCLUDE_CLASS.'/Db.php';
$config["class"]["logger"] = INCLUDE_CLASS.'/Logger.php';
$config["class"]["session"] = INCLUDE_CLASS.'/Session.php';
$config["class"]["user"] = INCLUDE_CLASS.'/User.php';
$config["class"]["wrapper"] = INCLUDE_CLASS.'/Wrapper.php';
$config["class"]["date_formatter"] = INCLUDE_CLASS.'/DateFormatter.php';
$config["class"]["mail"] = INCLUDE_CLASS.'/Mail.php';
$config["class"]["cookie"] = INCLUDE_CLASS.'/Cookie.php';
$config["class"]["err"] = INCLUDE_CLASS.'/Err.php';
$config["class"]["ticket"] = INCLUDE_CLASS.'/Ticket.php';

// Front office

$config["views"]["home"]["path"] = FRONT_VIEWS_PATH.'/home.php';
$config["views"]["home"]["need-auth"] = false;

$config["views"]["cgu"]["path"] = FRONT_VIEWS_PATH.'/cgu.php';
$config["views"]["cgu"]["need-auth"] = false;

$config["views"]["membership"]["path"] = FRONT_VIEWS_PATH.'/membership.php';
$config["views"]["membership"]["need-auth"] = false;

$config["views"]["coworking"]["path"] = FRONT_VIEWS_PATH.'/coworking.php';
$config["views"]["coworking"]["need-auth"] = false;

$config["views"]["sites"]["path"] = FRONT_VIEWS_PATH.'/sites.php';
$config["views"]["sites"]["need-auth"] = false;

$config["views"]["login"]["path"] = FRONT_VIEWS_PATH.'/login.php';
$config["views"]["login"]["need-auth"] = false;

$config["views"]["fp"]["path"] = FRONT_VIEWS_PATH.'/forgot_password.php';
$config["views"]["fp"]["need-auth"] = false;

$config["views"]["ohno"]["path"] = GLOBAL_VIEWS_PATH.'/ohno.php';
$config["views"]["ohno"]["need-auth"] = false;

// Admins

$config["admin-views"]["admin-ohno"]["path"] = ADMIN_VIEWS_PATH.'/ohno.php';
$config["admin-views"]["admin-ohno"]["need-auth"] = false;

$config["admin-views"]["admin-login"]["path"] = ADMIN_VIEWS_PATH.'/admin-login.php';
$config["admin-views"]["admin-login"]["need-auth"] = false;

$config["admin-views"]["admin-fp"]["path"] = ADMIN_VIEWS_PATH.'/admin_forgot_password.php';
$config["admin-views"]["admin-fp"]["need-auth"] = false;

$config["admin-views"]["hardware"]["path"] = ADMIN_VIEWS_PATH.'/assets.php';
$config["admin-views"]["hardware"]["need-auth"] = true;

$config["admin-views"]["rooms"]["path"] = ADMIN_VIEWS_PATH.'/assets.php';
$config["admin-views"]["rooms"]["need-auth"] = true;

// Auth wall

$config["views"]["dashboard"]["path"] = BACK_VIEWS_PATH.'/dashboard.php';
$config["views"]["dashboard"]["title"] = 'Dashboard';
$config["views"]["dashboard"]["need-auth"] = true;

$config["views"]["hardware-rental"]["path"] = BACK_VIEWS_PATH.'/rental.php';
$config["views"]["hardware-rental"]["title"] = 'Location de matériel';
$config["views"]["hardware-rental"]["need-auth"] = true;

$config["views"]["memsub"]["path"] = BACK_VIEWS_PATH.'/memsub.php';
$config["views"]["memsub"]["title"] = 'Abonnement';
$config["views"]["memsub"]["need-auth"] = true;

$config["views"]["room-rental"]["path"] = BACK_VIEWS_PATH.'/rental.php';
$config["views"]["room-rental"]["title"] = 'Location de salle';
$config["views"]["room-rental"]["need-auth"] = true;

$config["views"]["schedule"]["path"] = BACK_VIEWS_PATH.'/schedule.php';
$config["views"]["schedule"]["title"] = 'Réservations';
$config["views"]["schedule"]["need-auth"] = true;

$config["views"]["cafeteria"]["path"] = BACK_VIEWS_PATH.'/rental.php';
$config["views"]["cafeteria"]["title"] = 'Cafétéria';
$config["views"]["cafeteria"]["need-auth"] = true;

$config["views"]["settings"]["path"] = BACK_VIEWS_PATH.'/settings.php';
$config["views"]["settings"]["title"] = 'Paramètres';
$config["views"]["settings"]["need-auth"] = true;

$config["views"]["tickets"]["path"] = BACK_VIEWS_PATH.'/ticket.php';
$config["views"]["tickets"]["title"] = 'Tickets';
$config["views"]["tickets"]["need-auth"] = true;

$config["admin-views"]["users"]["path"] = ADMIN_VIEWS_PATH.'/users.php';
$config["admin-views"]["users"]["title"] = 'Admin';
$config["admin-views"]["users"]["need-auth"] = true;

$config["parameters"] = [
  "get:p",
  "get:a",
  "get:token",
  "get:email",
  "get:setPassword",
  "login:email",
  "login:password",
  "login:connect",
  "login:remember_me",
  "admin-connect:email",
  "admin-connect:password",
  "admin-connect:connect",
  "admin-connect:remember_me",
  "register:firstname",
  "register:lastname",
  "register:email",
  "register:emailc",
  "register:password",
  "register:passwordc",
  "register:g-recaptcha-response",
  "register:token",
  "register:phone",
  "register:cgu",
  "register:register"
];

// Path of the cript required when the controler find an "a" GET parameter
$config["actions"]["login"]["path"] = INCLUDE_MODEL.'/user/connect.php';
$config["actions"]["register"]["path"] = INCLUDE_MODEL.'/user/register.php';
$config["actions"]["disconnect"]["path"] = INCLUDE_MODEL.'/user/disconnect.php';
$config["actions"]["verify"]["path"] = INCLUDE_MODEL.'/user/verify_token.php';
$config["actions"]["activate"]["path"] = INCLUDE_MODEL.'/user/verify_token.php';

$config["admin-actions"]["admin-connect"]["path"] = INCLUDE_ADMIN_MODEL.'/user/admin-connect.php';
$config["admin-actions"]["verify"]["path"] = INCLUDE_ADMIN_MODEL.'/user/admin_verify_token.php';

$listOfErrors = [
    1=>"Le prénom doit faire entre 2 et 20 caractères.",
    2=>"Le nom doit faire entre 2 et 20 caractères.",
    3=>"Le mot de passe doit faire entre 8 et 64 caractères.",
    4=>"Le mot de passe de confirmation ne correspond pas.",
    5=>"L'email n'est pas valide.",
    6=>"L'email de confirmation ne correspond pas.",
    7=>"Le numéro de téléphone n'est pas valide",
    8=>"Le captcha n'est pas valide.",
    9=>"Un compte utilisant cette adresse email existe déjà.",
    10=>"Une erreur est survenue lors de la création du compte.",
    11=>"Identifiants invalides.",
];
