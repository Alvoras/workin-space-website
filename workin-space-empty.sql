-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 15 Mai 2018 à 23:50
-- Version du serveur :  5.5.57-0+deb8u1
-- Version de PHP :  7.0.22-2+0~20170804100528.5+jessie~1.gbpdea206

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `workin-space-empty`
--

-- --------------------------------------------------------

--
-- Structure de la table `ASSETS`
--

CREATE TABLE IF NOT EXISTS `ASSETS` (
`id_asset` int(11) NOT NULL,
  `id_site` int(11) DEFAULT NULL,
  `hardware_name` varchar(25) DEFAULT NULL,
  `room_name` varchar(25) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `ASSETS`
--

INSERT INTO `ASSETS` (`id_asset`, `id_site`, `hardware_name`, `room_name`, `is_deleted`) VALUES
(1, 1, NULL, 'Salle de réunion', 0),
(2, 1, NULL, 'Salle de réunion', 0),
(3, 1, NULL, 'Salle d''appel', 0),
(4, 1, NULL, 'Salle d''appel', 0),
(5, 1, NULL, 'Salle d''appel', 0),
(6, 1, 'Imprimante', NULL, 0),
(7, 2, NULL, 'Salle de réunion', 0),
(8, 2, NULL, 'Salle de réunion', 0),
(9, 2, NULL, 'Salle de réunion', 0),
(10, 2, NULL, 'Salle de réunion', 0),
(11, 2, NULL, 'Salle de réunion', 0),
(12, 2, NULL, 'Salle de réunion', 0),
(13, 2, NULL, 'Salle de réunion', 0),
(14, 2, NULL, 'Salle d''appel', 0),
(15, 2, NULL, 'Salle d''appel', 0),
(16, 2, NULL, 'Salle d''appel', 0),
(17, 2, NULL, 'Salle d''appel', 0),
(18, 2, NULL, 'Salle d''appel', 0),
(19, 2, 'Ordinateur', NULL, 0),
(20, 2, 'Ordinateur', NULL, 0),
(21, 2, 'Ordinateur', NULL, 0),
(22, 2, 'Ordinateur', NULL, 0),
(23, 2, 'Ordinateur', NULL, 0),
(24, 2, 'Ordinateur', NULL, 0),
(25, 2, 'Ordinateur', NULL, 0),
(26, 2, 'Ordinateur', NULL, 0),
(27, 2, 'Ordinateur', NULL, 0),
(28, 2, 'Ordinateur', NULL, 0),
(29, 2, 'Ordinateur', NULL, 0),
(30, 2, 'Ordinateur', NULL, 0),
(31, 2, 'Ordinateur', NULL, 0),
(32, 2, 'Ordinateur', NULL, 0),
(33, 2, 'Ordinateur', NULL, 0),
(34, 2, 'Ordinateur', NULL, 0),
(35, 2, 'Ordinateur', NULL, 0),
(36, 2, 'Ordinateur', NULL, 0),
(37, 2, 'Ordinateur', NULL, 0),
(38, 2, 'Ordinateur', NULL, 0),
(39, 2, 'Ordinateur', NULL, 0),
(40, 2, 'Ordinateur', NULL, 0),
(41, 2, 'Ordinateur', NULL, 0),
(42, 2, 'Ordinateur', NULL, 0),
(43, 2, 'Ordinateur', NULL, 0),
(44, 2, 'Ordinateur', NULL, 0),
(45, 2, 'Imprimante', NULL, 0),
(46, 2, 'Imprimante', NULL, 0),
(47, 2, 'Imprimante', NULL, 0),
(48, 3, NULL, 'Salle de réunion', 0),
(49, 3, NULL, 'Salle de réunion', 0),
(50, 3, NULL, 'Salle de réunion', 0),
(51, 3, NULL, 'Salle de réunion', 0),
(52, 3, NULL, 'Salle d''appel', 0),
(53, 3, NULL, 'Salle d''appel', 0),
(54, 3, 'Ordinateur', NULL, 0),
(55, 3, 'Ordinateur', NULL, 0),
(56, 3, 'Ordinateur', NULL, 0),
(57, 3, 'Ordinateur', NULL, 0),
(58, 3, 'Ordinateur', NULL, 0),
(59, 3, 'Ordinateur', NULL, 0),
(60, 3, 'Ordinateur', NULL, 0),
(61, 3, 'Ordinateur', NULL, 0),
(62, 3, 'Ordinateur', NULL, 0),
(63, 3, 'Ordinateur', NULL, 0),
(64, 3, 'Ordinateur', NULL, 0),
(65, 3, 'Ordinateur', NULL, 0),
(66, 3, 'Ordinateur', NULL, 0),
(67, 3, 'Ordinateur', NULL, 0),
(68, 3, 'Ordinateur', NULL, 0),
(69, 3, 'Ordinateur', NULL, 0),
(70, 3, 'Ordinateur', NULL, 0),
(71, 3, 'Ordinateur', NULL, 0),
(72, 3, 'Ordinateur', NULL, 0),
(73, 3, 'Imprimante', NULL, 0),
(74, 3, 'Imprimante', NULL, 0),
(75, 4, NULL, 'Salle de réunion', 0),
(76, 4, NULL, 'Salle de réunion', 0),
(77, 4, NULL, 'Salle de réunion', 0),
(78, 4, NULL, 'Salle de réunion', 0),
(79, 4, NULL, 'Salle de réunion', 0),
(80, 4, NULL, 'Salle d''appel', 0),
(81, 4, NULL, 'Salle d''appel', 0),
(82, 4, NULL, 'Salle d''appel', 0),
(83, 4, NULL, 'Salle d''appel', 0),
(84, 4, 'Ordinateur', NULL, 0),
(85, 4, 'Ordinateur', NULL, 0),
(86, 4, 'Ordinateur', NULL, 0),
(87, 4, 'Ordinateur', NULL, 0),
(88, 4, 'Ordinateur', NULL, 0),
(89, 4, 'Ordinateur', NULL, 0),
(90, 4, 'Ordinateur', NULL, 0),
(91, 4, 'Ordinateur', NULL, 0),
(92, 4, 'Ordinateur', NULL, 0),
(93, 4, 'Ordinateur', NULL, 0),
(94, 4, 'Ordinateur', NULL, 0),
(95, 4, 'Ordinateur', NULL, 0),
(96, 4, 'Ordinateur', NULL, 0),
(97, 4, 'Ordinateur', NULL, 0),
(98, 4, 'Ordinateur', NULL, 0),
(99, 4, 'Ordinateur', NULL, 0),
(100, 4, 'Ordinateur', NULL, 0),
(101, 4, 'Ordinateur', NULL, 0),
(102, 4, 'Ordinateur', NULL, 0),
(103, 4, 'Ordinateur', NULL, 0),
(104, 4, 'Ordinateur', NULL, 0),
(105, 4, 'Imprimante', NULL, 0),
(106, 5, NULL, 'Salle de réunion', 0),
(107, 5, NULL, 'Salle de réunion', 0),
(108, 5, NULL, 'Salle de réunion', 0),
(109, 5, NULL, 'Salle de réunion', 0),
(110, 5, NULL, 'Salle de réunion', 0),
(111, 5, NULL, 'Salle de réunion', 0),
(112, 5, NULL, 'Salle de réunion', 0),
(113, 5, NULL, 'Salle d''appel', 0),
(114, 5, NULL, 'Salle d''appel', 0),
(115, 5, NULL, 'Salle d''appel', 0),
(116, 5, NULL, 'Salle d''appel', 0),
(117, 5, NULL, 'Salle d''appel', 0),
(118, 5, 'Ordinateur', NULL, 0),
(119, 5, 'Ordinateur', NULL, 0),
(120, 5, 'Ordinateur', NULL, 0),
(121, 5, 'Ordinateur', NULL, 0),
(122, 5, 'Ordinateur', NULL, 0),
(123, 5, 'Ordinateur', NULL, 0),
(124, 5, 'Ordinateur', NULL, 0),
(125, 5, 'Ordinateur', NULL, 0),
(126, 5, 'Ordinateur', NULL, 0),
(127, 5, 'Ordinateur', NULL, 0),
(128, 5, 'Ordinateur', NULL, 0),
(129, 5, 'Ordinateur', NULL, 0),
(130, 5, 'Ordinateur', NULL, 0),
(131, 5, 'Ordinateur', NULL, 0),
(132, 5, 'Ordinateur', NULL, 0),
(133, 5, 'Ordinateur', NULL, 0),
(134, 5, 'Ordinateur', NULL, 0),
(135, 5, 'Ordinateur', NULL, 0),
(136, 5, 'Ordinateur', NULL, 0),
(137, 5, 'Ordinateur', NULL, 0),
(138, 5, 'Ordinateur', NULL, 0),
(139, 5, 'Imprimante', NULL, 0),
(140, 5, 'Imprimante', NULL, 0),
(141, 5, 'Imprimante', NULL, 0),
(142, 6, NULL, 'Salle de réunion', 0),
(143, 6, NULL, 'Salle de réunion', 0),
(144, 6, NULL, 'Salle d''appel', 0),
(145, 6, NULL, 'Salle d''appel', 0),
(146, 6, NULL, 'Salle d''appel', 0),
(147, 6, 'Ordinateur', NULL, 0),
(148, 6, 'Ordinateur', NULL, 0),
(149, 6, 'Ordinateur', NULL, 0),
(150, 6, 'Ordinateur', NULL, 0),
(151, 6, 'Ordinateur', NULL, 0),
(152, 6, 'Ordinateur', NULL, 0),
(153, 6, 'Ordinateur', NULL, 0),
(154, 6, 'Ordinateur', NULL, 0),
(155, 6, 'Ordinateur', NULL, 0),
(156, 6, 'Ordinateur', NULL, 0),
(157, 6, 'Ordinateur', NULL, 0),
(158, 6, 'Ordinateur', NULL, 0),
(159, 6, 'Ordinateur', NULL, 0),
(160, 6, 'Ordinateur', NULL, 0),
(161, 6, 'Ordinateur', NULL, 0),
(162, 6, 'Ordinateur', NULL, 0),
(163, 6, 'Ordinateur', NULL, 0),
(164, 6, 'Ordinateur', NULL, 0),
(165, 6, 'Ordinateur', NULL, 0),
(166, 6, 'Ordinateur', NULL, 0),
(167, 6, 'Ordinateur', NULL, 0),
(168, 6, 'Imprimante', NULL, 0),
(219, 1, NULL, 'Salle de réunion', 0);

-- --------------------------------------------------------

--
-- Structure de la table `AUTH_TOKENS`
--

CREATE TABLE IF NOT EXISTS `AUTH_TOKENS` (
  `selector` char(13) DEFAULT NULL,
  `hashed_validator` char(64) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `expires` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `AUTH_TOKENS`
--

INSERT INTO `AUTH_TOKENS` (`selector`, `hashed_validator`, `email`, `expires`) VALUES
('5af8a82045053', '56d64a8f24fc335adea36d8e3156161506a1a97ee6a3790ea7e71c16eb2dccdb', 'hadrien.bouffier@gmail.com', '2018-05-20 21:03:28');

-- --------------------------------------------------------

--
-- Structure de la table `HARDWARE`
--

CREATE TABLE IF NOT EXISTS `HARDWARE` (
  `name` varchar(25) NOT NULL,
  `price` float NOT NULL,
  `img` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `3D_texture` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `HARDWARE`
--

INSERT INTO `HARDWARE` (`name`, `price`, `img`, `description`, `3D_texture`) VALUES
('Imprimante', 10, 'public/global/img/cards/hardware/printer.png', 'Superbe imprimante nouvelle génération', ''),
('Ordinateur', 10, 'public/global/img/cards/hardware/laptop.png', 'Ordinateur portable performant', '');

-- --------------------------------------------------------

--
-- Structure de la table `rent`
--

CREATE TABLE IF NOT EXISTS `rent` (
`id_rent` int(11) NOT NULL,
  `rent_start` datetime DEFAULT NULL,
  `rent_stop` datetime DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `id_asset` int(11) NOT NULL,
  `rent_token` char(32) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ROOM`
--

CREATE TABLE IF NOT EXISTS `ROOM` (
  `name` varchar(25) NOT NULL,
  `seats` int(11) DEFAULT NULL,
  `price` float NOT NULL,
  `description` text NOT NULL,
  `img` varchar(150) NOT NULL,
  `3D_texture` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `ROOM`
--

INSERT INTO `ROOM` (`name`, `seats`, `price`, `description`, `img`, `3D_texture`) VALUES
('Salle d''appel', 15, 200, 'Nos salles de visioconférence sont parfaitement étanches au son. Vous pourrez ainsi travailler dans un calme absolu tout en profitant d''une discrétion sans égale.', 'public/global/img/cards/rooms/video_room.jpeg', 'public/front/img/pano/video_room_3d.jpg'),
('Salle de réunion', 10, 150, 'Nos salles de réunions sont ce qu''il vous faut lorsque vous avez besoin d''un espace privé dans lequel vous réunir avec votre équipe', 'public/global/img/cards/rooms/meeting_room.jpeg', 'public/front/img/pano/meeting_room_3d.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `SCHEDULE`
--

CREATE TABLE IF NOT EXISTS `SCHEDULE` (
  `id_schedule` int(11) NOT NULL,
  `day` varchar(10) DEFAULT NULL,
  `is_open` tinyint(1) DEFAULT NULL,
  `site_open` float DEFAULT NULL,
  `site_close` float DEFAULT NULL,
  `id_site` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `SCHEDULE`
--

INSERT INTO `SCHEDULE` (`id_schedule`, `day`, `is_open`, `site_open`, `site_close`, `id_site`) VALUES
(1, 'Lundi', 1, 9, 20, 1),
(2, 'Mardi', 1, 9, 20, 1),
(3, 'Mercredi', 1, 9, 20, 1),
(4, 'Jeudi', 1, 9, 20, 1),
(5, 'Vendredi', 1, 9, 20, 1),
(6, 'Samedi', 1, 11, 20, 1),
(7, 'Dimanche', 1, 11, 20, 1),
(8, 'Lundi', 1, 8, 21, 2),
(9, 'Mardi', 1, 8, 21, 2),
(10, 'Mercredi', 1, 8, 21, 2),
(11, 'Jeudi', 1, 8, 21, 2),
(12, 'Vendredi', 1, 9, 23, 2),
(13, 'Samedi', 1, 9, 20, 2),
(14, 'Dimanche', 1, 9, 20, 2),
(15, 'Lundi', 1, 9, 20, 3),
(16, 'Mardi', 1, 9, 20, 3),
(17, 'Mercredi', 1, 9, 20, 3),
(18, 'Jeudi', 1, 9, 20, 3),
(19, 'Vendredi', 1, 9, 20, 3),
(20, 'Samedi', 1, 11, 20, 3),
(21, 'Dimanche', 1, 11, 20, 3),
(22, 'Lundi', 1, 9, 20, 4),
(23, 'Mardi', 1, 9, 20, 4),
(24, 'Mercredi', 1, 9, 20, 4),
(25, 'Jeudi', 1, 9, 20, 4),
(26, 'Vendredi', 1, 9, 20, 4),
(27, 'Samedi', 1, 11, 20, 4),
(28, 'Dimanche', 1, 9, 20, 4),
(29, 'Lundi', 1, 8, 21, 5),
(30, 'Mardi', 1, 8, 21, 5),
(31, 'Mercredi', 1, 8, 21, 5),
(32, 'Jeudi', 1, 8, 21, 5),
(33, 'Vendredi', 1, 9, 23, 5),
(34, 'Samedi', 1, 9, 20, 5),
(35, 'Dimanche', 1, 9, 20, 5),
(36, 'Lundi', 1, 9, 20, 6),
(37, 'Mardi', 1, 9, 20, 6),
(38, 'Mercredi', 1, 9, 20, 6),
(39, 'Jeudi', 1, 9, 20, 6),
(40, 'Vendredi', 1, 9, 20, 6),
(41, 'Samedi', 1, 11, 20, 6),
(42, 'Dimanche', 1, 9, 20, 6);

-- --------------------------------------------------------

--
-- Structure de la table `SITE`
--

CREATE TABLE IF NOT EXISTS `SITE` (
`id_site` int(11) NOT NULL,
  `site_name` varchar(64) DEFAULT NULL,
  `site_address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `SITE`
--

INSERT INTO `SITE` (`id_site`, `site_name`, `site_address`) VALUES
(1, 'Republique', '4 Place de la Bastille, 75012 Paris'),
(2, 'Odéon', '18 Place de la République, 75010 Paris'),
(3, 'Place d''Italie', '7 Rue Corneille, 75006 Paris'),
(4, 'Ternes', '25 Boulevard Auguste Blanqui, 75013 Paris'),
(5, 'Beaubourg', '8 Avenue des Ternes, 75017 Paris'),
(6, 'Bastille', '26 Rue de Venise, 75004 Paris');

-- --------------------------------------------------------

--
-- Structure de la table `STAFF`
--

CREATE TABLE IF NOT EXISTS `STAFF` (
`id_staff` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `id_site` int(11) DEFAULT NULL,
  `id_staff_type` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `STAFF`
--

INSERT INTO `STAFF` (`id_staff`, `email`, `id_site`, `id_staff_type`, `is_deleted`) VALUES
(1, 'hadrien.bouffier@gmail.com', 1, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `STAFF_TYPES`
--

CREATE TABLE IF NOT EXISTS `STAFF_TYPES` (
  `id_staff_type` int(11) NOT NULL,
  `name_staff_type` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `STAFF_TYPES`
--

INSERT INTO `STAFF_TYPES` (`id_staff_type`, `name_staff_type`) VALUES
(1, 'Accueil'),
(2, 'Manager'),
(3, 'Support');

-- --------------------------------------------------------

--
-- Structure de la table `subscribe`
--

CREATE TABLE IF NOT EXISTS `subscribe` (
  `is_commited` tinyint(1) DEFAULT NULL,
  `sub_start` date DEFAULT NULL,
  `sub_stop` date DEFAULT NULL,
  `id_subscription` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `SUBSCRIPTIONS`
--

CREATE TABLE IF NOT EXISTS `SUBSCRIPTIONS` (
  `id_subscription` int(11) NOT NULL,
  `subscription_name` varchar(50) DEFAULT NULL,
  `first_hour` float DEFAULT NULL,
  `half_hour` float DEFAULT NULL,
  `day_price` float DEFAULT NULL,
  `commitment_duration` int(11) DEFAULT NULL,
  `monthly_no_commitment` float DEFAULT NULL,
  `monthly_w_commitment` float DEFAULT NULL,
  `unlimited_access` tinyint(1) NOT NULL,
  `hub_access` tinyint(1) DEFAULT NULL,
  `can_roam` tinyint(1) DEFAULT NULL,
  `access_all_spaces` tinyint(1) DEFAULT NULL,
  `student_rebate` int(11) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `SUBSCRIPTIONS`
--

INSERT INTO `SUBSCRIPTIONS` (`id_subscription`, `subscription_name`, `first_hour`, `half_hour`, `day_price`, `commitment_duration`, `monthly_no_commitment`, `monthly_w_commitment`, `unlimited_access`, `hub_access`, `can_roam`, `access_all_spaces`, `student_rebate`, `image`) VALUES
(1, 'visiteur', 5, 2.5, 24, NULL, NULL, NULL, 0, 0, 0, 0, 20, 'public/global/img/shuttle/shuttle_sm_shrink.png'),
(2, 'Simple', 4, 2, 20, 12, 24, 20, 0, 1, 1, 1, NULL, 'public/global/img/shuttle/shuttle_md_shrink.png'),
(3, 'Résident', NULL, NULL, NULL, 8, 300, 252, 1, NULL, NULL, NULL, NULL, 'public/global/img/shuttle/shuttle_lg_shrink.png');

-- --------------------------------------------------------

--
-- Structure de la table `TICKETS`
--

CREATE TABLE IF NOT EXISTS `TICKETS` (
`id_ticket` int(11) NOT NULL,
  `ticket_name` varchar(50) DEFAULT NULL,
  `ticket_status` int(11) DEFAULT NULL,
  `ticket_description` text,
  `id_staff` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `TICKET_TYPES`
--

CREATE TABLE IF NOT EXISTS `TICKET_TYPES` (
`id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `TICKET_TYPES`
--

INSERT INTO `TICKET_TYPES` (`id`, `type`) VALUES
(1, 'Nouveau'),
(2, 'En cours'),
(3, 'En attente'),
(4, 'Traité');

-- --------------------------------------------------------

--
-- Structure de la table `USERS`
--

CREATE TABLE IF NOT EXISTS `USERS` (
  `password` char(64) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `phone` char(10) DEFAULT NULL,
  `token` char(32) DEFAULT NULL,
  `qr_salt` char(32) NOT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `USERS`
--

INSERT INTO `USERS` (`password`, `lastname`, `firstname`, `email`, `phone`, `token`, `qr_salt`, `is_activated`, `is_deleted`) VALUES
('$2y$10$rOIJ8bbl3wWKyS8d9S1TeOJ/2L.m4MH.IvvlQsdRYhd1G0EDMtPi.', 'Hadrien', 'Bouffier', 'hadrien.bouffier@gmail.com', '0600000000', '', '0e3a81cc46612f8747e5f318084c5c51', 1, 0);

--
-- Déclencheurs `USERS`
--
DELIMITER //
CREATE TRIGGER `CREATE_RECORD` AFTER INSERT ON `USERS`
 FOR EACH ROW INSERT INTO AUTH_TOKENS (email) VALUES (NEW.email)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `visit`
--

CREATE TABLE IF NOT EXISTS `visit` (
  `date_in` datetime NOT NULL,
  `date_out` datetime DEFAULT NULL,
  `id_site` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ASSETS`
--
ALTER TABLE `ASSETS`
 ADD PRIMARY KEY (`id_asset`), ADD KEY `FK_ASSETS_id_site` (`id_site`), ADD KEY `FK_ASSETS_material_name` (`hardware_name`), ADD KEY `FK_ASSETS_room_name` (`room_name`);

--
-- Index pour la table `AUTH_TOKENS`
--
ALTER TABLE `AUTH_TOKENS`
 ADD PRIMARY KEY (`email`), ADD KEY `FK_auth_tok_email_users_email` (`email`);

--
-- Index pour la table `HARDWARE`
--
ALTER TABLE `HARDWARE`
 ADD PRIMARY KEY (`name`);

--
-- Index pour la table `rent`
--
ALTER TABLE `rent`
 ADD PRIMARY KEY (`id_rent`,`email`,`id_asset`), ADD KEY `FK_rent_email` (`email`), ADD KEY `FK_rent_id_asset` (`id_asset`);

--
-- Index pour la table `ROOM`
--
ALTER TABLE `ROOM`
 ADD PRIMARY KEY (`name`);

--
-- Index pour la table `SCHEDULE`
--
ALTER TABLE `SCHEDULE`
 ADD PRIMARY KEY (`id_schedule`), ADD KEY `FK_SCHEDULE_id_site` (`id_site`);

--
-- Index pour la table `SITE`
--
ALTER TABLE `SITE`
 ADD PRIMARY KEY (`id_site`);

--
-- Index pour la table `STAFF`
--
ALTER TABLE `STAFF`
 ADD PRIMARY KEY (`id_staff`,`email`), ADD KEY `FK_STAFF_email` (`email`), ADD KEY `FK_STAFF_id_site` (`id_site`), ADD KEY `FK_STAFF_id_staff_type` (`id_staff_type`);

--
-- Index pour la table `STAFF_TYPES`
--
ALTER TABLE `STAFF_TYPES`
 ADD PRIMARY KEY (`id_staff_type`);

--
-- Index pour la table `subscribe`
--
ALTER TABLE `subscribe`
 ADD PRIMARY KEY (`id_subscription`,`email`), ADD KEY `FK_subscribe_email` (`email`);

--
-- Index pour la table `SUBSCRIPTIONS`
--
ALTER TABLE `SUBSCRIPTIONS`
 ADD PRIMARY KEY (`id_subscription`);

--
-- Index pour la table `TICKETS`
--
ALTER TABLE `TICKETS`
 ADD PRIMARY KEY (`id_ticket`), ADD KEY `FK_TICKETS_id_staff` (`id_staff`), ADD KEY `FK_TICKETS_email` (`email`), ADD KEY `FK_TICKETS_ticket_status` (`ticket_status`);

--
-- Index pour la table `TICKET_TYPES`
--
ALTER TABLE `TICKET_TYPES`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `USERS`
--
ALTER TABLE `USERS`
 ADD PRIMARY KEY (`email`);

--
-- Index pour la table `visit`
--
ALTER TABLE `visit`
 ADD PRIMARY KEY (`date_in`), ADD KEY `FK_visit_email` (`email`), ADD KEY `FK_users_id_site` (`id_site`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `ASSETS`
--
ALTER TABLE `ASSETS`
MODIFY `id_asset` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=220;
--
-- AUTO_INCREMENT pour la table `rent`
--
ALTER TABLE `rent`
MODIFY `id_rent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SITE`
--
ALTER TABLE `SITE`
MODIFY `id_site` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `STAFF`
--
ALTER TABLE `STAFF`
MODIFY `id_staff` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `TICKETS`
--
ALTER TABLE `TICKETS`
MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `TICKET_TYPES`
--
ALTER TABLE `TICKET_TYPES`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
