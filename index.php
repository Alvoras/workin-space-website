<?php
// ACTIVATE ERRORE REPORTING WITHOUT MODIFYING PHP.INI
// error_reporting(E_ALL);
ini_set('display_errors', 0);

// Require config file
require_once "private/config/conf.inc.php";

foreach ($config["libs"] as $script) {
    require_once $script;
}

foreach ($config["class"] as $script) {
    require_once $script;
}



$logger = new Logger();

// Instanciate the PDO object inside the Db class
$db = Db::getInstance();

// Init or resume session
$session = new Session();
$user = $session->loadUser();

$cookie = Cookie::getInstance();

$errorData = [];

// if there is a "p" parameter, store it into page, else display home page and don't compute the other parameters
if (count($_GET)) {
    // Check if there is no unexpected parameter
    foreach ($_GET as $key => $value) {
        try {
          Warden::isValidParameter($config, "get:$key");
        } catch (Exception $e) {
          $logger->log(LOG_WARNING, "[".$_SERVER["REMOTE_ADDR"]."] [GET:$key] Unexpected GET parameter");
          Path::display($config, "ohno");
        }
    }

  if (array_key_exists("p", $_GET)) {
    $page = strtolower($_GET["p"]);

    // Check for non-existent views
    try {
        Warden::isValidView($config, $page);
    } catch (Exception $e) {
        $logger->log(LOG_INFO, "[".$_SERVER["REMOTE_ADDR"]."] [$page] Unexpected view queried");

        // Can be replaced for redirecting 404
        Path::display($config, "ohno");
    }

    if ($config["views"][$page]["need-auth"] == true) {

      // If the user data are not found in the session
      if ( !(Warden::isConnected($db)) ) {
        $err = new Err("Veuillez vous reconnecter");
        $err->load();

        // Log ?
        $cookie->flush();
        $session->reset();
        header("Location: /p/login");
        die();
      }
    }

    // MODEL QUERY FOR EVERY VIEWS HERE
    switch ($_GET["p"]) {
      case "sites":
        require_once INCLUDE_MODEL."/db/load_rooms.php";
        break;
      case "dashboard":
        require_once INCLUDE_MODEL."/dashboard/load_dashboard.php";
        break;

      case "membership":
        require_once INCLUDE_MODEL."/membership/subscriptions.php";
        break;

      case "memsub":
        require_once INCLUDE_MODEL."/membership/user_subs.php";
        break;

      case "tickets":
        require_once INCLUDE_MODEL.'/db/fetch_site_data.php';
        require_once INCLUDE_MODEL.'/db/fetch_staff_info.php';
        require_once INCLUDE_MODEL.'/db/fetch_ticket_status.php';
        break;

      case "fp":
        // $_SESSION["verify_token"] is set by the ?a=verify call with a valid token
        // if (!isset($_SESSION["verify_token"])) {
        //   $_SESSION["verify_token"] = false;
        // }
        break;

      case "login":
        // $_SESSION["activated"] is set by the ?a=activate call with a valid token
        if (!isset($_SESSION["activated"])) {
          $_SESSION["activated"] = false;
        }

        require_once INCLUDE_MODEL."/user/autoconnect.php";
        break;

      case "hardware-rental":
      // $target is used inside load_grid.php
      $localData["target"] = "hardware";
      $session->append($localData);
      require_once INCLUDE_MODEL."/user/load_user_planning.php";
      require_once INCLUDE_MODEL."/db/fetch_site_data.php";
      require_once INCLUDE_MODEL."/item-grid/load_grid.php";
      require_once INCLUDE_SCRIPT."/db/fetch_item_available_qty.php";
      require_once INCLUDE_SCRIPT."/db/fetch_all_rentals.php";
      break;

      case "room-rental":
      // $target is used inside load_grid.php
      $localData["target"] = "room";
      $session->append($localData);
      require_once INCLUDE_MODEL."/user/load_user_planning.php";
      require_once INCLUDE_MODEL."/db/fetch_site_data.php";
      require_once INCLUDE_MODEL."/item-grid/load_grid.php";
      require_once INCLUDE_SCRIPT."/db/fetch_item_available_qty.php";
      require_once INCLUDE_SCRIPT."/db/fetch_all_rentals.php";
      break;

      case "cafeteria":
      // $target is used inside load_grid.php
      $localData["target"] = "cafeteria";
      $session->append($localData);
      require_once INCLUDE_MODEL."/db/fetch_site_data.php";
      require_once INCLUDE_MODEL."/item-grid/load_grid.php";
      break;

      case "schedule":
      $localData["target"] = "schedule";
      $session->append($localData);
      require_once INCLUDE_MODEL."/user/load_user_planning.php";
      break;
    }

    // Require the view queried by the "p" get parameter
    Path::display($config, $_GET["p"]);
  }

  if (array_key_exists("a", $_GET)) {
    $action = $_GET["a"];

    // Check if there is no unexpected action
    try {
      Warden::isValidAction($config, $action);
    } catch (Exception $e) {
      $logger->log(LOG_INFO, "[".$_SERVER["REMOTE_ADDR"]."] [$action] Unexpected action parameter");

      Path::display($config, "ohno");
    }

    switch ($action) {
      case "verify":
        if (!isset($_SESSION["location"])) {
          $_SESSION["location"] = "fp";
        }
        break;

      case "activate":
        if (!isset($_SESSION["location"])) {
          $_SESSION["location"] = "login";
        }
        break;
    }

    if (count($_POST)) {
      // Check if there is no unexpected POSTed parameter
      foreach ($_POST as $param => $val) {
        try {
          Warden::isValidParameter($config, "$action:$param");
        } catch (Exception $e) {
          $logger->log(LOG_WARNING, "[".$_SERVER["REMOTE_ADDR"]."] [$action:$param] Unexpected POST parameter");
          Path::display($config, "ohno");
        }
      }
    }

    require_once Path::format($config["actions"][$action]["path"]);
  }
}else{
  Path::display($config, "home");
}
