<?php require_once INCLUDE_BACK_VIEWS.'/global/head.php' ?>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/schedule/schedule.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/global/planning.css"/>
</head>
<body>
  <?php
  include INCLUDE_BACK_VIEWS."/global/loading-placeholder.php";

  $session = new Session();
  $user = $session->loadUser();
  ?>

  <div id="main-container" class="container-fluid">
    <div id="main-row-container" class="row">

    <?php require_once INCLUDE_BACK_VIEWS.'/global/side-menu.php' ?>

    <div id="planning-container" data-activated="false" class="flex-center-col">
      <?php require_once INCLUDE_BACK_VIEWS.'/global/planning.php' ?>
    </div>
    </div>
  </div>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/scripts.php' ?>
  <script src="<?=INCLUDE_BACK_JS?>/planning/planning-toolbox.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/planning/planning-events.js"></script>

  <?php require_once INCLUDE_GLOBAL_VIEWS.'/footer.php' ?>
