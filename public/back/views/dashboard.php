<?php require_once INCLUDE_BACK_VIEWS.'/global/head.php' ?>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/global/side-menu.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/dashboard/dashboard.css"/>
</head>
<body>
  <?php
    $session = new Session();
    $user = $session->loadUser();
   ?>
  <?php include INCLUDE_BACK_VIEWS."/global/loading-placeholder.php" ?>
  <?php include INCLUDE_GLOBAL_VIEWS."/transition-ball.php" ?>
  <div id="main-container" class="container-fluid">
    <div id="main-row-container" class="d-flex justify-content-center row">
      <?php require_once INCLUDE_BACK_VIEWS.'/global/side-menu.php' ?>

      <div id="tiles-row-container" class="col-md-12">
          <div class="tile-header-container flex-center-col row">
              <h1 id="tile-header" class="col-md-10 tile-header-title">Dashboard</h1>
          </div>

          <div class="row d-flex offset-md-1" id="tiles-row-subcontainer">
            <div class="tile-container col-md-4 mb-4">
              <div class="tile flex-center-col">
                <div class="user-tile-image-container flex-center-col">
                  <img src="public/front/img/logo/ws_logo_gradient.png" class="user-tile-image" alt="">
                  <!--<img src="<?php echo QRCACHE_PATH.'/'.$user->getQrSalt().".png" ?>" class="user-tile-image" alt="">-->
                </div>
                <div class="user-image-caption-container tile-text flex-center-col">
                  <h4 class="user-image-caption-name"><?= $user->firstname." ".$user->lastname ?></h4>
                  <h6 class="user-image-caption-email"><?= $user->email ?></h6>
                </div>
              </div>
            </div>
            <div class="tile-container col-md-4 mb-4">
              <div class="tile">
                  <div class="events-top-container flex-center-row">
                    <h1 class="tile-title">Cafétéria</h1>
                    <a class="tile-link clickable" href="?p=cafeteria">Réserver</a>
                  </div>
                  <div class="coming-soon-container">
                    <h2 class="coming-soon flex-center-col">
                      Bientôt disponible !
                    </h2>
                  </div>
                  <!-- <div class="events-container flex-center-row">
                    <div class="events-text-container flex-center-col">
                      <h3 class="events-text-order">Commande 123456</h3>
                      <h5 class="events-text-date">01/02/2018</h5>
                    </div>
                    <div class="events-look-container">
                      <a class="flex-center-col clickable" href="#">Voir</a>
                    </div>
                  </div> -->
              </div>
            </div>
            <div class="tile-container col-md-4 mb-4">
              <div class="tile">
                  <div class="events-top-container flex-center-row">
                    <h1 class="tile-title">Réservations</h1>
                    <a class="tile-link clickable" href="?p=hardware-rental">Réserver</a>
                  </div>
                  <div class="tile-rent-container">
                    <?php
                      foreach ($user->rentals as $key => $val) {
                        $text = "Le ".$val["start"]["day"]."/".$val["start"]["month"]."/".$val["start"]["year"]." de ".$val["start"]["hour"]."h00 à ".$val["stop"]["hour"]."h00";
                        if ($val["start"]["hour"] === $val["stop"]["hour"]) {
                          $text = "Le ".$val["start"]["day"]."/".$val["start"]["month"]."/".$val["start"]["year"]." à ".$val["start"]["hour"]."h00";
                        }
                        ?>
                        <div class="events-container flex-center-row">
                          <div class="events-text-container flex-center-col">
                            <h3 class="events-text-order">#<?=$val["id_rent"]?> <?=$val["name"]?></h3>
                            <h5 class="events-text-date"><?=$text?></h5>
                          </div>
                          <div class="events-look-container">
                            <a class="flex-center-col clickable" href="?p=schedule">Voir</a>
                          </div>
                        </div>
                        <?php
                      }
                    ?>
                  </div>
              </div>
            </div>

            <div class="dw-100"></div>
              <div class="tile-container col-md-8 mb-4">
                <div class="tile">
                    <canvas id="freq-chart"></canvas>
                </div>
              </div>
              <div class="tile-container col-md-4 mb-4">
                <div class="tile">
                  <div class="time-left-container flex-center-col">
                    <?php if (empty($user->membership["id_subscription"]) || $user->membership["id_subscription"] == null) { ?>
                      <h3 class="time-left-title">Aucun abonnement</h3>
                      <div id="membership-renew-container"  class="flex-center-col">
                        <a href="?p=memsub" id="membership-renew-button" class="flex-center-col clickable new-member">
                          S'abonner
                        </a>
                      </div>
                    <?php
                   }else { ?>
                      <h3 class="time-left-title">Temps restant</h3>
                      <h1 class="time-left"><?=$user->membership["time_left"]?></h1>
                      <h3 class="time-left-caption">jours</h3>
                      <div id="membership-renew-container"  class="flex-center-col">
                        <a href="?p=memsub" id="membership-renew-button" class="flex-center-col clickable membership-<?=$user->membership["id_subscription"]?>">
                          Renouveler
                        </a>
                      </div>
                    <?php
                  }
                    ?>
                  </div>
                </div>
              </div>
          </div>
    </div>
  </div>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/scripts.php' ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/dashboard/dashboard-events.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/dashboard/chart.js"></script>
  <?php require_once INCLUDE_GLOBAL_VIEWS.'/footer.php' ?>
