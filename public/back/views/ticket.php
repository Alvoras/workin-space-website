<?php
require_once INCLUDE_BACK_VIEWS.'/global/head.php';
?>
<link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/ticket/ticket.css"/>
</head>
<body>
  <?php
  include INCLUDE_BACK_VIEWS."/global/loading-placeholder.php";
  if($_SESSION['staff_data'][0] == null || $_SESSION['staff_data'][0]['is_deleted'])
  {
    header('Location: /p/ohno');
  }
 ?>
  <div id="main-container" class="container-fluid">

    <div class="tile-header-container flex-center-col row">
        <h1 id="tile-header" class="col-md-10 tile-header-title">Tickets</h1>
    </div>

    <div id="main-row-container" class="row d-flex justify-content-center">

      <div class="col-md-10" id="ticket-form-container">

        <div class="flex-center-col ticket-tile" id="add-tickets">
          <div class="ticket-tile-title-container">
            <h1 class="ticket-tile-title">Ajouter un ticket</h1>
            <button id='display_ticket' class='ticket-button'>Afficher les tickets</button>
          </div>
          <center>
            <input type="text" id="ticket_name" placeholder="Objet du ticket"/>

            <div class="col-md-6" id="description-ticket">
              <textarea class="form-control" id="ticket_description" rows="3" placeholder="Description"></textarea>
            </div>

            <button class="ticket-button" id='submit_ticket' type="button">Ajouter</button>
            </center>
          </div>

        <div id="select-display-ticket">
          <div class="flex-center-col ticket-tile" >
            <div class="ticket-tile-title-container">
              <h1 class="ticket-tile-title">Affichage des tickets</h1>
              <?php
                if($_SESSION['staff_data'][0]['id_staff_type'] < 3)
                {
                  echo "<button id='new_ticket' class='ticket-button'>Creer ticket</button>";
                }
                else
                {
                  echo "<button id='export_ticket' class='ticket-button'>Exporter les ticket</button>";
                }
              ?>
              </div>

            <div id="sort_ticket">
              <?php
              if($_SESSION['staff_data'][0]['id_staff_type'] > 2)
              {
                echo "<button option='admin' class='ticket-button isselect'>Tous les sites</button>";
                foreach($_SESSION['sites_data'] as $key => $value)
                {
                  echo "<button class='ticket-button' option='site_".$value['id_site']."'>".$value['site_name']."</button>";
                }
              }
              else
              {
                echo "<button option='site' class='ticket-button isselect'>Tickets de votre site</button>";
                echo "<button class='ticket-button' option='user'>Vos Tickets</button>";
              }
              ?>
              <div id='sort_ticket_status' class='flex-center-row'>
                <?php
                  echo "<button option='all' class='ticket-button isselect'>Tous les status</button>";
                  foreach($_SESSION['ticket_status'] as $key => $value)
                  {
                    echo "<button class='ticket-button' option='".$value['id']."'>".$value['type']."</button>";
                  }
                ?>
              </div>
            </div>
          </div>

          <div id="ticket_div">
            <table class="table table-hover table-dark ticket_table">
              <thead id="thead_ticket">
              </thead>
              <tbody id="tbody_ticket">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="data" data-item='' data-startHour='' data-stopHour='' data-day=''></div>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/scripts.php'; ?>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script src="<?=INCLUDE_BACK_JS?>/ticket/index.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/ticket/ticket.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/ticket/ticketController.js"></script>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/footer.php' ?>
