<?php require_once INCLUDE_BACK_VIEWS.'/global/head.php' ?>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/hardware-rent/hardware-rent.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/global/item-grid.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/global/planning.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/rental/rental.css"/>
</head>
<body>
  <?php
  include INCLUDE_BACK_VIEWS."/global/loading-placeholder.php";
  $session = new Session();
  $user = $session->loadUser();
 ?>
  <div id="main-container" class="container-fluid">
    <div id="main-row-container" class="d-flex justify-content-center row">

    <?php require_once INCLUDE_BACK_VIEWS.'/global/side-menu.php' ?>

    <?php require_once INCLUDE_BACK_VIEWS.'/global/item-grid/item-grid.php' ?>
    <div id="planning-container" data-activated="true" class="flex-center-col">
      <?php require_once INCLUDE_BACK_VIEWS.'/global/planning.php' ?>
    </div>

    </div>
  </div>
  <!-- <div id="data" data-item='' data-startHour='' data-stopHour='' data-day=''></div> -->
  <?php require_once INCLUDE_BACK_VIEWS.'/global/scripts.php' ?>
  <script src="<?=INCLUDE_BACK_JS?>/rental/rental-events.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/planning/planning-toolbox.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/planning/planning-events.js"></script>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/footer.php' ?>
