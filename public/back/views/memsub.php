<?php require_once INCLUDE_BACK_VIEWS.'/global/head.php' ?>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/memsub/memsub.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/memsub/card-selection.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/global/checkout-form.css"/>
  <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/sub-cards.css"/>
</head>
<body>
  <?php
  $session = new Session();
  $user = $session->loadUser();
  ?>
  <?php include INCLUDE_BACK_VIEWS."/global/loading-placeholder.php" ?>
  <div id="main-container" class="container-fluid">

    <?php require_once INCLUDE_BACK_VIEWS.'/global/side-menu.php' ?>

    <?php include INCLUDE_BACK_VIEWS."/global/checkout_form.php" ?>

    <?php include INCLUDE_GLOBAL_VIEWS."/card-selection.php" ?>

    <div id="renew-screen-container" class="flex-center-col">
      <span id="renew-screen-title">Choissez une durée</span>
      <div id="renew-card-container" class="flex-center-row">
        <?php
          $subId = intval($user->idSubscription)-1;
          $renewCardVal = [1, 3, 6, 12];
          foreach ($renewCardVal as $key => $val) {
            ?>
            <div data-duration="<?=$val?>" class="flex-card-col renew-card back-glow-yellow clickable">
              <span class="renew-card-text">
                <?=$val?> mois
              </span>
              <span class="renew-card-text renew-card-price">
                <?=$val*intval($_SESSION["sub-cards"][$subId]["monthly_no_commitment"])?>€
              </span>
            </div>
            <?php
          }
         ?>
      </div>
    </div>

    <div id="main-row-container" class="d-flex justify-content-start row col-md-12">
      <div class="offset-md-2 col-md-3" id="current-membership">
          <?php
          $buttonText = "Modifier";
          $disabled = "";
          $topCheckoutMode = "update";
          if (empty($user->membership["id_subscription"]) || $user->membership["id_subscription"] == null) {
            $buttonText = "S'abonner";
            $topCheckoutMode = "subscribe";
            $disabled = "disabled";
            ?>
            <div id="current-membership-card">
              <h1 id="no-membership" class="flex-center-col">?</h1>
            </div>
            <?php
          }else {
            include INCLUDE_BACK_VIEWS."/memsub/current-card.php";
          }
            $botCheckoutMode = "renew";
           ?>
      </div>
      <div class="col-md-7 flex-center-col" id="option-buttons-container">
        <div data-checkout-mode="<?=$topCheckoutMode?>" id="option-button-upgrade" class="clickable option-button option-button-mem-<?=$user->membership["id_subscription"]?>">
          <h3 id="option-button-upgrade-text"><?=$buttonText?></h3>
        </div>
        <div data-checkout-mode="<?=$botCheckoutMode?>" id="option-button-renew" class="<?=$disabled?> clickable option-button option-button-mem-<?=$user->membership["id_subscription"]?>">
          <h3 id="option-button-renew-text">Renouveler</h3>
        </div>
      </div>
    </div>

  </div>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/scripts.php' ?>
  <script src="<?=INCLUDE_BACK_JS?>/checkout-form/checkout-events.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/checkout-form/checkout-toolbox.js"></script>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/footer.php' ?>
