<?php require_once INCLUDE_BACK_VIEWS.'/global/head.php' ?>
  <link rel="stylesheet" href="<?=INCLUDE_BACK_CSS?>/settings/settings.css"/>
</head>
<body>
  <?php
    $session = new Session();
    $user = $session->loadUser();
   ?>
  <?php include INCLUDE_BACK_VIEWS."/global/loading-placeholder.php" ?>
  <div id="main-container" class="container-fluid">
    <div id="main-row-container" class="row d-flex justify-content-center">

    <?php require_once INCLUDE_BACK_VIEWS.'/global/side-menu.php' ?>

    <div class="col-md-6" id="settings-form-container">
      <div id="qrcode-container" class="flex-center-col">
        <div id="qrcode-img-container">
          <img src="<?php echo QRCACHE_PATH.'/'.$user->getQrSalt().".png" ?>" alt="qr-code" id="qrcode-img">
        </div>
      </div>
      <div class="flex-center-col settings-tile" id="settings-profile">
        <div class="settings-tile-title-container">
          <h1 class="settings-tile-title">Informations du profil</h1>
        </div>
        <input id="settings-lastname" class="settings-profile-input" type="text" name="lastname" value="<?=$user->lastname?>">

        <input id="settings-firstname" class="settings-profile-input" type="text" name="firstname" value="<?=$user->firstname?>">

        <input id="settings-email" class="settings-profile-input" type="text" name="email" value="<?=$user->email?>">

        <input id="settings-phone" class="settings-profile-input" type="text" name="phone" value="<?=$user->phone?>">
        <button class="settings-update-button" id="settings-profile-button" data-tile="profile" type="button" name="button">Mettre à jour</button>
      </div>
        <div class="flex-center-col settings-tile" id="settings-password">
          <div class="settings-tile-title-container">
            <h1 class="settings-tile-title">Mot de passe</h1>
          </div>
          <input id="settings-pwd-old" class="settings-pwd-input" type="password" name="old-pwd" value="" placeholder="Mot de passe courrant">
          <label for="settings-pwd-old"></label>

          <input id="settings-pwd-new" class="settings-pwd-input" type="password" name="new-pwd" value="" placeholder="Entrer un nouveau mot de passe">
          <label for="settings-pwd-new"></label>

          <input id="settings-pwd-new-confirm" class="settings-pwd-input" type="password" name="new-pwd-confirm" value="" placeholder="Confirmer le nouveau mot de passe">
          <label for="settings-pwd-new-confirm"></label>
          <button class="settings-update-button" id="settings-pwd-button" data-tile="pwd" type="button" name="button">Mettre à jour</button>
        </div>
    </div>

    </div>
  </div>
  <?php require_once INCLUDE_BACK_VIEWS.'/global/scripts.php' ?>
  <script src="<?=INCLUDE_BACK_JS?>/settings/settings-events.js"></script>
  <script src="<?=INCLUDE_BACK_JS?>/settings/settings-toolbox.js"></script>
