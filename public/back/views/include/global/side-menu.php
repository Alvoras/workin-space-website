<div id="side-menu-container">
    <div id="side-menu-list-container">
        <ul id="side-menu-list" class="flex-center-col">
            <li class="side-menu-tab flex-center-col clickable" title="Dashboard"><a class="flex-center-col side-menu-link" data-page="Dashboard" href="/p/dashboard"><i class="icon-web"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Mon abonnement"><a class="flex-center-col side-menu-link" data-page="Abonnement" href="/p/memsub"><i class="icon-star"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Louer une salle"><a class="flex-center-col side-menu-link" data-page="Location de salle" href="/p/room-rental"><i class="icon-handshake-o"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Louer du matériel"><a class="flex-center-col side-menu-link" data-page="Location de matériel" href="/p/hardware-rental"><i class="icon-desktop"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Mes réservations"><a class="flex-center-col side-menu-link" data-page="Réservations" href="/p/schedule"><i class="icon-calendar"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Caféteria"><a class="flex-center-col side-menu-link" data-page="Cafétéria" href="/p/cafeteria"><i class="icon-spoon-knife"></i></a>
            <li class="side-menu-tab flex-center-col clickable" title="Paramètres"><a class="flex-center-col side-menu-link" data-page="Paramètres" href="/p/settings"><i class="icon-cog"></i></a>
            <li id="side-menu-disconnect" class="side-menu-tab flex-center-col" title="Se déconnecter"><a class="flex-center-col side-menu-link" data-page="Déconnexion" href="/a/disconnect"><i class="icon-log-out"></i></a>
        </ul>
    </div>
</div>
<div id="side-menu-handle-container" class="flex-center-col display-side-menu-handle">
  <div class="side-menu-handle"></div>
  <div class="side-menu-handle"></div>
</div>
