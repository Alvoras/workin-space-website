<div id="checkout-form-container" class="flex-center-row">
  <span id="checkout-form-exit" class="flex-center-col">X</span>
  <div id="checkout-item-container">

  </div>
    <div id="checkout-form" class="flex-center-col">
      <div id="checkout-form-padding" class="flex-center-col">
        <div id="checkout-form-card-no"  class="input-container flex-center-row">
          <input id="card-no-1" class="card-form-input card-no-part" type="text" name="card-no-1" value="" placeholder="????">
          <span class="input-separator">-</span>
          <input id="card-no-2" class="card-form-input card-no-part" type="text" name="card-no-2" value="" placeholder="????">
          <span class="input-separator">-</span>
          <input id="card-no-3" class="card-form-input card-no-part" type="text" name="card-no-3" value="" placeholder="????">
          <span class="input-separator">-</span>
          <input id="card-no-4" class="card-form-input card-no-part" type="text" name="card-no-4" value="" placeholder="????">
          <label for="card-no" id="card-no-error" class="error-msg hidden-error-msg">Le numéro de carte est invalide</label>
        </div>
        <div id="checkout-form-card-info" class="input-container flex-center-row">
          <div class="error-msg-container">
            <input id="card-exp-1" class="card-form-input card-exp" type="text" name="card-exp-1" value="" placeholder="??">
            <span class="input-separator">/</span>
            <input id="card-exp-2" class="card-form-input card-exp" type="text" name="card-exp-2" value="" placeholder="??">
            <label for="card-no" id="card-exp-error" class="input-container error-msg hidden-error-msg">Ce n'est pas une date valide</label>
          </div>
          <div class="error-msg-container">
            <input id="card-cvv" class="card-form-input" type="text" name="card-cvv" value="" placeholder="???">
            <label for="card-no" id="card-cvv-error" class="error-msg hidden-error-msg">Le code de vérification n'est pas valide</label>
          </div>
        </div>
        <button id="checkout-form-button" type="button" class="option-button clickable" name="validate">Envoyer</button>
      </div>
    </div>
</div>
