<?php
  if (isset($_POST["ajax"]) && $_POST["ajax"] == true) {
    require_once "../../../../../private/config/conf.inc.php";

    foreach ($config["libs"] as $script) {
        require_once "../../../../../$script";
    }

    foreach ($config["class"] as $class) {
        require_once "../../../../../$class";
    }

    $session = new Session();
    $user = $session->loadUser();
  }

  // From material colors table
  $colorTable = [
    "red",
    "pink",
    "deep-purple",
    "indigo",
    "light-blue",
    "cyan",
    "teal",
    "light-green",
    "lime",
    "amber",
    "orange",
    "deep-orange",
    "brown",
    "grey",
    "blue-grey",
    "blue",
    "purple",
    "black",
    "yellow",
    "green"
  ];

  $showOnlySelectedItem = false;

  $dFormatter = new DateFormatter();

  $today = $dFormatter->getToday();

  // Index and convert all the necessary data
  if (isset($_POST["date"])) {
    $strDate = strtotime($_POST["date"]);

    $dayOfTheWeek = date('N', $strDate);
    $dayOfTheMonth = date('j', $strDate);
    $month = date('n', $strDate);
    $year = date('Y', $strDate);
  }else {
    $dayOfTheWeek = date('N');
    $dayOfTheMonth = date('j');
    $month = date('n');
    $year = date('Y');
  }

  if (isset($_POST["siteId"])) {
    // Minus 1 because we are checking against a foreach $key, which start at 0
    // while our site ids starts at 1
    $selectedSite = $_POST["siteId"]-1;
  }else {
    $selectedSite = 0;
  }

  if (isset($_POST["item"])) {
    $item = $_POST["item"];
  }else {
    // $_POST["item"] will be undefined when loading the page without ajax call or when no item has been selected yet
    // In this case, we default to the first item if the list
    if (isset($_SESSION["grid_data"][0])) {
      $item = $_SESSION["grid_data"][0]["name"];
    }
  }

  if (isset($_POST["isRestricted"]) && $_POST["isRestricted"] != null) {
    // True or false
    $showOnlySelectedItem = $_POST["isRestricted"];
  }

  $isChecked = ($showOnlySelectedItem == "true")?"checked":"";

  $mondayNo = $dayOfTheMonth - $dayOfTheWeek;

  // NOTE : Since $month is the month of the date clicked, $totalDayThisMonth
  // will store the max days of the month that has been clicked
  // and not the monday's month

  if ($month-1 == 0) {
    $prevMonth = 12;
    $prevYear = $year-1;
  }else {
    $prevMonth = $month-1;
    $prevYear = $year;
  }

  $totalDayPreviousMonth = $dFormatter->getMaxDaysFromMonth($prevMonth, $prevYear);

  if ($mondayNo < 0) {
    $mondayNo = ($totalDayPreviousMonth + $dayOfTheMonth) - $dayOfTheWeek;
    $month = $prevMonth;
    $year = $prevYear;
  }

  $totalDayThisMonth = $dFormatter->getMaxDaysFromMonth($month, $year);

  $offset = date('N', strtotime("01-".$month."-".$year));

  // Attach an id to each rental in order to bind colors to them
  for ($i=0; $i < count($user->rentals); $i++) {
    $user->rentals[$i]["id"] = $i;
  }
?>

<div id="planning">
  <div id="table-header">
    <div id="planning-date-container" class="flex-center-row">
      <span id="planning-date-title">Planning pour la semaine du</span>
      <input id="planning-date-change" type="text" value="<?= $mondayNo+1 ?>/<?= $month ?>/<?= $year ?>" placeholder="Changer la date">
      <?php
        if ($_SESSION["target"] !== "schedule") {
        ?>
        <div id="show-rentals-checkbox-container" class="flex-center-row">
          <label for="show-rentals-checkbox" id="show-rentals-text">Montrer uniquement les réservations liées à cet article</label>
          <input type="checkbox" <?= $isChecked ?> id="show-rentals-checkbox">
        </div>
        <?php
        }
        ?>
    </div>
      <?php
      if ($_SESSION["target"] == "schedule") {
        ?>
          <div id="site-header-container" class="flex-center-row">
        <?php
        foreach ($_SESSION["sites_data"] as $key => $val) {
          ?>
          <span class="header-site-name clickable <?php if($key == $selectedSite) echo "site-name-selected"?>" data-siteId="<?=$val["id_site"]?>"><?=$val["site_name"]?></span>
          <?php
        }
        ?>
        </div>
        <?php
      }
       ?>
  </div>
  <table>
    <tr class="header-row">
      <th class="padding-header-row"></th>
      <?php
      for ($i=0; $i < 7; $i++) {
        ?>
        <th class="day-header"><?=$dFormatter->parseWeekDay(($mondayNo+$i+$offset)%7)?></th>
        <?php
      }
         ?>
    </tr>
    <tr class="header-row">
      <th class="padding-header-row"></th>
      <?php
      $computedDates = [];
      for ($i=0; $i < 7; $i++) {
        $computedDayNo = (($mondayNo+$i)%$totalDayThisMonth)+1;
        $computedDayNo = ($computedDayNo+1 < 10)?"0".($computedDayNo):$computedDayNo;
        if ($computedDayNo < $mondayNo+1+$i) {
          if ($month+1 > 12) {
            $computedMonthNo = 1;
          }else {
            $computedMonthNo = $month+1;
          }
        }else {
          $computedMonthNo = $month;
        }
        $computedMonthNo = ($computedMonthNo < 10)?"0".$computedMonthNo:$computedMonthNo;
        $computedDates[] = $computedDayNo.'/'.$computedMonthNo;
        ?>
        <th class="date-header"><?= $computedDayNo ?>/<?= $computedMonthNo ?></th>
        <?php
      }
         ?>
    </tr>
    <?php
    for ($hour=8; $hour <= 20; $hour++) {
      // if time-row is reserved then append tile with time-row stamp
      ?>
      <tr>
        <td class="time-row" data-hour="<?= $hour ?>h"><?= $hour ?>h</td>
        <?php
          for ($day=0; $day < 7; $day++) {
            $disabledCell = '';
            $reservedCell = '';
            $currentDay = $mondayNo+$day+1;
            $actualMonth = $month;
            $actualYear = $year;
            $unavailable = '';
            // if we are overlapping next month, then remove the overflow by removing
            // the number of days from the monday's month in order to adjust the new month
            if ($currentDay > $totalDayThisMonth) {
              $currentDay -= $totalDayThisMonth;
              if ($month+1 > 12) {
                $actualMonth = 1;
                $actualYear++;
              }else {
                $actualMonth = $month+1;
              }
            }

            // Get a DateTime object of the current looped date
            $tempDateTime = new DateTime("$actualYear-$actualMonth-$currentDay $hour:00:00");

            if (
              $hour < $_SESSION["site_schedule"][$day]["site_open"]
              || $hour > $_SESSION["site_schedule"][$day]["site_close"]
            ){
              $disabledCell = "disabled-cell";
            }else if ($tempDateTime < $today) {
              // Compare the two DateTime objects to know if the current looped date is in the past
              // compared to today
              $disabledCell = "disabled-cell";
            }else{
              if ($_SESSION["target"] !== "schedule") {
                if(!itemAvailable($_SESSION["all_rentals"], $tempDateTime, $item)){
                  $disabledCell = "disabled-cell";
                  $unavailable = "unavailable";
                }
              }
            }

            ?>
            <td class="planning-cell <?= $disabledCell ?> <?= $unavailable ?>"
               data-hour="<?= $hour ?>h"
               data-dayid="<?=$day?>"
               data-day="<?= $computedDates[$day] ?>"
               >
               <div class="reserved-cell-container flex-center-row">
            <?php
            foreach ($user->rentals as $key => $val) {
              $selectedCell = '';
              $displayHour = '';
              $reservedCell = '';
              $dataReserved = 'false';
              $shadowReserved = '';
              $hidden = '';

              if ($val["stop"]["day"] - $val["start"]["day"] >= 0) {
                if (($currentDay >= $val["start"]["day"]
                && $currentDay <= $val["stop"]["day"])
                && ($hour >= $val["start"]["hour"] && $hour < $val["stop"]["hour"])
                && ($actualMonth >= $val["start"]["month"] && $actualMonth <= $val["stop"]["month"])
                && ($actualYear >= $val["start"]["year"] && $actualYear <= $val["stop"]["year"])
              ) {
                  $selectedCell = "selected-cell";
                  $displayHour = $hour.'h00';
                  $parsedMinutes["start"] = (count($val["start"]["minute"]) == 1)?"0".$val["start"]["minute"]:$val["start"]["minute"];
                  $parsedMinutes["stop"] = (count($val["stop"]["minute"]) == 1)?"0".$val["stop"]["minute"]:$val["stop"]["minute"];

                  $dataReserved = "true";

                  if ($_SESSION["target"] !== "schedule") {
                    if ($item !== null) {
                      if ($item !== $val["name"]) {
                        if ($showOnlySelectedItem == "true") {
                          $hidden = "hidden";
                        }
                        $shadowReserved = "shadow-reserved";
                        $dataReserved = "shadow";
                      }
                    }
                  }

                  $reservedCell = "<span
                  data-rentid=\"".$val["id_rent"]."\"
                  data-rental-name=\"".$val["name"]."\"
                  data-rental-start=\"".$val["start"]["hour"]."h".$parsedMinutes["start"]."\"
                  data-rental-stop=\"".$val["stop"]["hour"]."h".$parsedMinutes["stop"]."\"
                  data-reserved=\"".$dataReserved."\"
                  class=\"$shadowReserved $hidden reserved-cell ".$val["asset"]["type"]." ".$colorTable[$val["id"]%count($colorTable)]."-border\">
                  $displayHour
                  </span>";
                }
              }
              // For each rented item, add a tile in the cell
                echo $reservedCell;
            }
            ?>
              </div>
             </td>
          <?php
          }
         ?>
      </tr>
      <?php
    }
     ?>
  </table>
</div>
<div id="planning-tooltip" data-rentid="-1">
  <div id="tooltip-name">

  </div>
  <div id="tooltip-duration">

  </div>
  <div id="tooltip-button-container" class="flex-center-col">
    <div id="tooltip-button" class="flex-center-col clickable">
      <div id="tooltip-button-text">Supprimer</div>
    </div>
  </div>
</div>
