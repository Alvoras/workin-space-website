<div id="tiles-row-container" class="col-md-12 flex-center-col">
  <div class="tile-header-container flex-center-col row">
      <h1 id="tile-header" class="col-md-10 offset-md-1 tile-header-title">Location</h1>
  </div>
  <div id="site-header-container" class="flex-center-row">
    <?php
    foreach ($_SESSION["sites_data"] as $key => $val) {
      ?>
      <span class="header-site-name clickable <?php if($key == 0) echo "site-name-selected"?>" data-siteId="<?=$val["id_site"]?>"><?=$val["site_name"]?></span>
      <?php
    }
     ?>
  </div>

  <div id="item-grid-container" class="row offset-md-2 offset-lg-1 d-flex justify-content-start" data-gridtype="<?=$_SESSION["target"]?>">
    <?php
    $atLeastOne = false;
    foreach ($_SESSION["grid_data"] as $key => $val) {
      include INCLUDE_BACK_VIEWS.'/global/item-grid/grid-card.php';
      $atLeastOne = true;
    }
    if (!$atLeastOne) {
      ?>
      <div class="nothing-found flex-center-col">
        <h1 id="nothing-found-msg">Il n'y a rien à afficher</h1>
      </div>
      <?php
    }
    ?>
  </div>
</div>
