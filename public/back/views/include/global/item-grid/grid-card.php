<div class="col-lg-3 col-md-5 col-xs-5 site-tile mr-4" <?php if (isset($val["3D_texture"])){ ?> data-threetxt="<?=$val["3D_texture"]?>" <?php } ?> >
  <div class="site-tile-img-container">
    <img src="<?=$val["img"]?>" class="site-tile-image" alt="room">
  </div>
  <div class="site-tile-description">
    <div class="site-tile-description-header-container flex-center-row">
      <h3 class="site-tile-description-title">
        <?=$val["name"]?>
      </h3>
      <h4 class="site-tile-description-price"><?=$val["price"]?>€/heure</h4>
    </div>
    <?php if(isset($val["seats"])){
    ?>
    <div class="site-tile-description-title-caption">
      <h6><?=$val["seats"]?> personnes</h6>
    </div>
    <?php
    }
    ?>
    <p class="site-tile-description-text">
      <?=$val["description"]?>
    </p>
  </div>
</div>
