<?php
$session = new Session();
$user = $session->loadUser();

$subId = intval($user->idSubscription)-1;

$commitmentDuration = ($_SESSION["sub-cards"][$subId]["commitment_duration"] != null)?$_SESSION["sub-cards"][$subId]["commitment_duration"]:"";

$subCaption = ($_SESSION["sub-cards"][$subId]["monthly_w_commitment"]!= null)?
'<h6 class="plan-card-text">Engagement '.$commitmentDuration.' mois : '.$_SESSION["sub-cards"][$subId]["monthly_w_commitment"].'€/mois</h6>':
'<h6 class="plan-card-text">Étudiants : '.$_SESSION["sub-cards"][$subId]["student_rebate"].'€/jour</h6>';

$caption = ($_SESSION["sub-cards"][$subId]["monthly_no_commitment"]!= null)?
'<h3 class="plan-card-text">'.$_SESSION["sub-cards"][$subId]["monthly_no_commitment"].'€/mois</h3>':
'<h3 class="plan-card-text">'.$_SESSION["sub-cards"][$subId]["day_price"].'€/jour</h3>';

$unlimitedAccess = ($_SESSION["sub-cards"][$subId]["unlimited_access"] == true)?'<li>Accès illimité':"";
if ($_SESSION["sub-cards"][$subId]["unlimited_access"] == true) {
  $rocketUnlimited = '<div class="flex-center-col">
                          <i id="unlimited-rocket" class="icon-rocket"></i>
                          <h2 class="plan-card-text">AUCUNE LIMITE<h2>
                      </div>';

  $openSpace = '';
  $unlimitedWifi  = '';
  $unlimitedDrinks = '';
}else{
  $rocketUnlimited = '';

  if ($_SESSION["sub-cards"][$subId]["monthly_no_commitment"]) {
    $openSpace = '';
    $unlimitedWifi = '';
    $unlimitedDrinks = '';
  }else{
    $openSpace = '<li><i class="icon-desktop"></i>Accès à l\'open space';
    $unlimitedWifi = '<li><i class="icon-network_check"></i>Wifi illimité';
    $unlimitedDrinks = '<li><i class="icon-coffee"></i>Snacking et boissons à volonté';
  }
}

$hubAccess = ($_SESSION["sub-cards"][$subId]["hub_access"] == true)?'<li><i class="icon-device_hub"></i>Accès premium au HUB':"";

$canRoam = ($_SESSION["sub-cards"][$subId]["can_roam"] == true)?'<li><i class="icon-location"></i>Valable dans tous nos sites':"";

$accessAllSpaces = ($_SESSION["sub-cards"][$subId]["access_all_spaces"] == true)?'<li><i class="icon-infinity"></i>Accès libre à tous les espaces':"";

$firstHour = '<li>'.$_SESSION["sub-cards"][$subId]["first_hour"].'€ la première heure';
$halfHour = '<li>'.$_SESSION["sub-cards"][$subId]["half_hour"].'€ par demi-heure';

  echo '<div data-price="'.$_SESSION["sub-cards"][$subId]["monthly_no_commitment"].'" class="plan-card solo back-glow-'.$subId.' flex-center-col">
          <div class="plan-card-title">
            <h2 class="plan-card-text">'.$_SESSION["sub-cards"][$subId]["subscription_name"].'</h2>
          </div>
          <div class="plan-card-img flex-center-col">
            <img src="'.$_SESSION["sub-cards"][$subId]["image"].'" alt="basic-plan-shuttle">
          </div>
          <div class="plan-card-caption flex-center-col">
          '.$caption.'
          '.$subCaption.'
          </div>
            ';
            if (!$_SESSION["sub-cards"][$subId]["unlimited_access"]){
              echo '
                <ul class="hourly-rate flex-center-col" class="plan-card-text">
                  '.$firstHour.'
                  '.$halfHour.'
                </ul>';
            }
            echo '
            <div class="bonus-list-container">
              <div class="bonus-list flex-center-col">
                <ul class="plan-card-text">
                  '.$unlimitedWifi.'
                  '.$unlimitedDrinks.'
                  '.$openSpace.'
                  '.$accessAllSpaces.'
                  '.$canRoam.'
                  '.$hubAccess.'
                  '.$rocketUnlimited.'
                </ul>
            </div>
          </div>
      </div>';
?>
