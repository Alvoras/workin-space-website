function drawChart() {
  let script = "private/scripts/db/fetch_user_visits.php";
  let params = {
    script,
    async: false
  };
  let userData = ajaxFetchPHP(params);
  userData = JSON.parse(userData);
  console.log(userData);
  let data = [];
  let days = [];
  let buf = 0;
  let hourBuf = 0;
  let minuteBuf = 0;
  let parsedMinutes = 0;

    for (let key in userData["in"]) {
      days.push(key);
      for (let i = 0; i < userData["in"][key]["hour"].length; i++) {
        hourBuf += parseInt(userData["out"][key]["hour"][i]) - parseInt(userData["in"][key]["hour"][i]);
        minuteBuf += parseInt(userData["out"][key]["min"][i]) - parseInt(userData["in"][key]["min"][i]);
        parsedMinutes = ((minuteBuf !== 0)?time60ToTime100(minuteBuf):0);
        buf += hourBuf + parsedMinutes;
      }
      data.push(buf);
      buf = 0;
      hourBuf = 0;
      minuteBuf = 0;
      parsedMinutes = 0;
  }

  console.log(data);

  let ctx = document.getElementById("freq-chart").getContext('2d');

  let gradient = ctx.createLinearGradient(0, 0, 600, 600);
  gradient.addColorStop(0, '#ffb88c');
  gradient.addColorStop(1, '#ea758f');

  let myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: days,
          datasets: [{
              label: 'Temps présent',
              backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
              data,
              borderWidth: 2,
              borderColor: gradient,
              fill: false
          }]
      },
      options: {
          maintainAspectRatio: false,
          scales: {
             yAxes: [{
                 ticks: {
                     beginAtZero: true,
                     userCallback: function(label, index, labels) {
                         // Return whole numbers
                         if (Math.floor(label) === label) {
                             return label;
                         }

                     },
                 }
             }],
         }
      }

  });
}
