$(document).ready(function(){
  $('#submit_ticket').click(function(){
    var ticket_name = document.getElementById('ticket_name').value;
    var ticket_description = document.getElementById('ticket_description').value;

    TicketController.createTicket(ticket_name, ticket_description, function(success) {
      ticket_name = null;
      ticket_description = null;
      swapScreen('display');
    });
  });

  $('#sort_ticket').click(function(event){
    if($(event.target).attr('id') == 'sort_ticket' || $(event.target).attr('id') == 'sort_ticket_status')
    {
      return;
    }

    $('#'+$(event.target.parentNode).attr('id') +' > button').filter('.isselect').removeClass('isselect');
    $(event.target).addClass('isselect');

    displayTicket();
  });

  $('#display_ticket').click(function(){
    swapScreen('display');
  });


  $('#new_ticket').click(function(){
    swapScreen('create');
  });

  $('#export_ticket').click(function(){
    $.ajax({
      url: "private/scripts/ticket/execProgram.php",
      type: "POST",
      async: false, // Mode synchrone
      complete: function(data){
          console.log(data.responseText);
      }
    });
  });

  displayTicket();
});

function swapScreen(screen)
{
  if(screen == 'display')
  {
    $('#add-tickets').fadeOut(400, function()
    {
      $('#select-display-ticket').fadeIn(400);
    });
  }
  else if(screen == 'create')
  {
    $('#select-display-ticket').fadeOut(400, function()
    {
      $('#add-tickets').fadeIn(400);
    });
  }
}

function displayTicket() {

  var category = $('#sort_ticket > button').filter('.isselect').attr('option');
  var status = $('#sort_ticket_status > button').filter('.isselect').attr('option');
  var site;

  if((site = category.split('_')) != undefined)
  {
    category = site[0];
    site = site[1];
  }

  TicketController.listTicket(category, status, site, function(ticket) {
    //var infoStaff = dataStaff();
    var infoStaff;
    $.ajax({
      url: "private/scripts/ticket/getStaffInfo.php",
      type: "POST",
      async: false, // Mode synchrone
      complete: function(data){
        // var data;
        infoStaff = JSON.parse(data.responseText);
        fillDisplayTicket(infoStaff, ticket);
      }
    });
  });
}

function fillDisplayTicket(infoStaff, ticket)
{
  var tbody = document.getElementById('tbody_ticket');
  tbody.innerHTML = "";
  var thead = document.getElementById('thead_ticket');
  thead.innerHTML = "";

  var fillTitle = false;
  var newTilesLine = thead.insertRow(-1);

  var newCell, newLine;
  var i = 0;

  ticket.forEach(function(element)
  {
    if(element['is_deleted'] == 0)
    {
      newLine = tbody.insertRow(-1);
      newLine.id = element['id_ticket'];

      for(var property in element)
      {
        if(!fillTitle)
        {
          let title = property.split('_');
          newCell = newTilesLine.insertCell(i);
          newCell.innerHTML = title[0];
          if(title[1] != undefined) newCell.innerHTML += " " + title[1];
        }

        newCell = newLine.insertCell(i);
        if(property == 'is_deleted')
        {
          if(infoStaff['id_staff'] == element['id_staff'] || infoStaff['id_staff_type'] > 3)
          {
            newCell.innerHTML = "Delete";
            newCell.addEventListener('click', function(){
              TicketController.deleteTicket(this.parentNode);
            });
          }
          else
          {
            newCell.innerHTML = "";
          }
        }
        else
        {
          newCell.innerHTML = element[property];
        }
        i++;
      }

      if(infoStaff['id_staff_type'] > 2)
      {
        if(!fillTitle)
        {
          newCell = newTilesLine.insertCell(i);
          newCell.innerHTML = 'changer statut';
        }
        else if(element['ticket_status'] != 4)
        {
          newCell = newLine.insertCell(i);
          newCell.innerHTML =   "<div class='change_status' id='progress' title='Ticket en cours'><i class='fas fa-envelope-open'></i></div>"+
                                "<div class='change_status' id='late' title='Ticket en attente'><i class='fas fa-clock'></i></div>"+
                                "<div class='change_status' id='finish' title='Ticket traité'><i class='fas fa-check'></i></div>";

            $('.change_status').click(function(e){
              var status, id_ticket;

              status = this.id;
              id_ticket = this.parentNode.parentNode.id;

              if(status != 'late' && status != 'progress' && status != 'finish')
              {
                return ;
              }

              if(status == 'late') status = 3;
              else if(status == 'progress') status = 2;
              else if(status == 'finish') status = 4;

              TicketController.changeStatus(status, id_ticket, function(success) {
                displayTicket();
              });

            });
        }
      }

      i = 0;
      fillTitle = true;
    }
  }, this);
}

function dataStaff()
{
  $.ajax({
    url: "private/scripts/ticket/getStaffInfo.php",
    type: "POST",
    async: false, // Mode synchrone
    complete: function(data){
      var data;
      data = JSON.parse(data.responseText);
      return data;
    }
  });
}
