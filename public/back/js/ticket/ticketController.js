
const TicketController = function() {};

TicketController.createTicket = function(ticket_name, ticket_description, callback) {
  const request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(request.readyState == 4) {
      if(request.status == 201) {
        callback(true);
        displayTicket();
      } else {
        callback(false);
      }
    }
  };
  request.open('POST', 'private/scripts/ticket/createTicket.php');
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.send(`ticket_name=${ticket_name}&ticket_description=${ticket_description}`);
};

TicketController.listTicket = function(category, value, site, callback) {
  const request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(request.readyState == 4) {
      if(request.status == 200) {
        callback(JSON.parse(request.responseText));
        //callback(request.responseText);
      } else {
        callback([]);
      }
    }
  };
  request.open('POST', 'private/scripts/ticket/listTicket.php');
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.send(`category=${category}&value=${value}&site=${site}`);
}

TicketController.deleteTicket = function(ticket)
{
  if(confirm('Etes-vous sur de vouloir supprimer ce ticket ?'))
  {
    const request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if(request.readyState == 4) {
        if(request.status == 201) {
          displayTicket();
        }
      }
    };
    request.open('POST', 'private/scripts/ticket/deleteTicket.php');
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.send(`ticket_id=${ticket.id}`);
  }
}

TicketController.changeStatus = function(status, id_ticket, callback)
{
  const request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(request.readyState == 4) {
      if(request.status == 201) {
        callback(true);
      }
      else
      {
        callback(request.responseText);
      }
    }
  };
  request.open('POST', 'private/scripts/ticket/updateStatusTicket.php');
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.send(`ticket_id=${id_ticket}&status=${status}`);
}
