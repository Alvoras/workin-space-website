if (typeof document._data !== 'undefined') {
  document._data.reverse = false;
  // Default to the first site
  document._data.siteId = 1;
}

$(document).ready(() => {
  let datepickerParams = {
    // Found in planning-toolbox.js
    onSelect: updatePlanningWithDate
  };

  $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
  $("#planning-date-change").datepicker(datepickerParams);

  var mouseDown = false;
  var hoverMode = "add";
  var hasHovered = false;

  if ($("#planning-container").data("activated") == true) {
    $("#planning-container").mousedown(function(e){
      // If the user keep the mouse down while leaving the window to trigger mouse up
      // outside of it and then reenters it to trigger another mouse down
      // cancel it
      if (mouseDown) {
        return;
      }

      // Reset default to avoid a "true" state while selecting only one cell
      document._data.reverse = false;

      let that = $(e.target);
      if (that.hasClass("planning-cell") || that.hasClass("reserved-cell-container")) {
        e.preventDefault();
        e.stopPropagation();

        that = getPlanningCell(that);

        if (that.hasClass("disabled-cell")
        || that.find(".reserved-cell:not(.hidden)").length > 0)
        {
          return;
        }

        document._data.time.day = that.data("day");
        document._data.time.startHour = that.siblings(".time-row").html();
        document._data.startCellDay = that.data("dayid");

        mouseDown = true;
        if (that.hasClass("selected-cell")) {
          hoverMode = "remove";
        }
        that.toggleClass("selected-cell");
      }
    });

    $("#planning-container").mouseup(function(e){
      let that = $(e.target);

      if (!that.hasClass("planning-cell")
      &&!that.hasClass("reserved-cell-container")
      &&!that.hasClass("reserved-cell:not(.hidden)")
      &&!that.hasClass("reserved-cell")
      &&!that.find(".reserved-cell").length > 0
      ) {
        // mouseDown prevent firing the event when clicking outside of the window and then
        // mouse up over the planning
        return;
      }

      if (!mouseDown) {
        return;
      }

        that = getPlanningCell(that);

        hoverMode = "add";
        mouseDown = false;

        let day = document._data.time.day;
        let formatedDay = `${document._data.year}/${day}`;
        let siteId = document._data.siteId;
        // let item = document._data.item.replace("'", "\\'");
        let item = document._data.item;
        let startHour = document._data.time.startHour;
        if (typeof startHour !== 'undefined') {
          startHour = startHour.replace("h", "");
        }
        let stopHour = '';

        if (hasHovered) {
          stopHour = document._data.time.stopHour.replace("h", "");
        }else {
          stopHour = startHour;
        }

        // Cleanse _data.time
        document._data.time = {};

        hasHovered = false;

        if (parseInt(startHour) > parseInt(stopHour)) {
          // Swap both
          [startHour, stopHour] = [stopHour, startHour];
        }

        if (document._data.reverse) {
           startHour = subOneHour(startHour);
           stopHour = addOneHour(stopHour);
        }

        if (startHour === stopHour) {
          stopHour = addOneHour(startHour);
        }

        let displayHour = `de ${startHour}h à ${stopHour}h`;

        let successMsg = "La réservation est validée";

        displayHour = `${displayHour} le ${day}`;

        let confirmRes = confirm(`Voulez-vous réserver "${document._data.item}" ${displayHour} ?`);

        if (confirmRes) {
          // ajaxExec to register new reservation
          // If success then refreshphp planning
          // Display notification

          startHour = `${startHour}:00:00`;
          stopHour = `${stopHour}:00:00`;

          let script = "private/scripts/db/add_rental.php";

          let data = {
            "rentalData": {
              startHour,
              stopHour
            },
            item,
            siteId,
            day: formatedDay
          };

          let notifyResult = (res) => {
      			notify(res);
      		};

      		let params = {
      			data,
      			async: false,
      			script,
      			success: notifyResult,
      			failure: notifyResult
      		};

      		ajaxExec(params);
        }

        $(".selected-cell").removeClass("selected-cell");

        // item = item.replace("\\", '');

        let res = updatePlanningWithSiteName(siteId, item);

        if (!res) {
          $.growl.error({message: "La mise à jour du planning a échouée"});
        }
    });

    $("#planning-container").mouseover(function(e){
      let that = $(e.target);
      if (that.hasClass("planning-cell")
      || that.hasClass("reserved-cell")
      ||that.hasClass("reserved-cell-container")) {
        // If we're hovering a reserved cell, then get the parent planning-cell
        that = getPlanningCell(that);

        let currentDayCell = document._data.time.day;

        // If the user is hovering outside of the column or on a disabled cell
        if (!mouseDown || that.hasClass("disabled-cell") || that.data("day") != currentDayCell) return;

        hasHovered = true;

        let dayCells = $(`.planning-cell[data-day="${document._data.time.day}"]`);

        let parsedStartHour = parseInt(document._data.time.startHour.replace("h", ""));

        let $el;
        let elParsedHour;
        let currentParsedHour = parseInt(that.data("hour").replace("h", ""));

        let start;
        let end;

        let reserved = false;
        let breakLoop = false;

        if (currentParsedHour < parsedStartHour) {
          // If the user is hovering from bottom to top, reverse the array to put any
          // potential reserved cell between the start and the currently hovered cell
          dayCells = $(dayCells.toArray().reverse());
        }

        dayCells.each(function(index, el) {
          if (breakLoop) {
            return false;
          }

          $el = $(el);

          elParsedHour = parseInt($el.data("hour").replace("h", ""));

          if (parsedStartHour > currentParsedHour) {
            // If we're going from bottom to top then swap the start and the end
            start = currentParsedHour;
            end = parsedStartHour;
            document._data.reverse = true;
          }else {
            start = parsedStartHour;
            end = currentParsedHour;
            document._data.reverse = false;
          }

          if (elParsedHour >= start && elParsedHour <= end) {
            // If we find a reserved cell that should be included in the painted trail
            // then break the loop and prevent the loop to go further by setting breakLoop as true
            if ($el.find(".reserved-cell[data-reserved='true']").length > 0) {
              breakLoop = true;
              return false;
            }

            if ($(e.target).data("reserved") == false
            || $(e.target).find(".reserved-cell[data-reserved='true']").length === 0) {
              document._data.time.stopHour = addOneHour($el.siblings(".time-row").html());
            }

            $el.addClass("selected-cell");
          }else {
            $el.removeClass("selected-cell");
          }

          reserved = false;
        });


        breakLoop = false;
      }
    });
  }

  $("#planning-container").click(function(e){
    let that = $(e.target);

    if ($("#planning-tooltip").css("opacity") == "1"
    && $("#planning-tooltip").css("display") != "none"
    && !that.hasClass("reserved-cell")
    ) {
      $("#planning-tooltip").css("opacity", "0");
      setTimeout(() => {
        $("#planning-tooltip").css("display", "none");
      }, 200);
    }

    if (that.is("#tooltip-button-text")) {
      let script = "private/scripts/db/remove_rental.php";

      let tooltip = that.closest("#planning-tooltip");

      if (tooltip.length < 1) {
        return;
      }

      let siteId = document._data.siteId;
      let item = document._data.item;

      let rentId = tooltip.data("rentid");

      let data = {
        rentId
      };

      let successCb = (res) => {
        notify(res);

        updatePlanningWithSiteName(siteId, item);
      };

      let failureCb = (res) => {
        notify(res);
      }

      let params = {
        data,
        async: true,
        script,
        success: successCb,
        failure: failureCb
      };

      ajaxExec(params);
    }

    if ( (!(that.hasClass("header-site-name"))
    || that.hasClass("site-name-selected"))
    && !that.hasClass("reserved-cell")
    && !that.is("#planning-date-change")
    ) {
      return;
    }

    if (that.hasClass("reserved-cell") && !that.hasClass("legend-cell")) {
      let rentalName = that.data("rental-name");
      let rentalStart = that.data("rental-start");
      let rentalStop = that.data("rental-stop");
      let rentId = that.data("rentid");
      let mousex = e.clientX;
      let mousey = e.clientY;

      // Insert the right info
      // Position the buble
      $("#planning-tooltip").data("rentid", rentId);
      $("#tooltip-name").html(rentalName);
      $("#tooltip-duration").html(`${rentalStart} - ${rentalStop}`);
      $("#planning-tooltip").css({
        display: "block",
        top: mousey,
        left: mousex
       });
       setTimeout(() => {
         $("#planning-tooltip").css("opacity", "1");
       }, 50);


    }else if (that.hasClass("header-site-name")) {
      let siteId = that.data("siteid");
      document._data.siteId = siteId;
      let item = (typeof document._data != "undefined")?document._data.item:undefined;
      let res = updatePlanningWithSiteName(siteId, item);

      if (res) {
        $(".site-name-selected").removeClass("site-name-selected");
        $(`.header-site-name[data-siteid=${siteId}]`).addClass("site-name-selected");
      }
    }
  });

});
