function updatePlanningWithDate(date) {
  // We need to send a day/month/year formated date (european) to PHP
  // because strtotime() interprets slash-separated date to be
  // formated the american way (m/d/y)
  if (date.indexOf("/") != -1) {
    date = date.split("/").join("-");
  }

  if (typeof document._data !== 'undefined' && typeof document._data.time !== 'undefined') {
    document._data.time.year = date.split("-")[2];
  }

  let isRestricted = isShowRentalsChecked();

  let siteId = $(".site-name-selected").data("siteid");

  updateUserRentals(siteId);

  // Ajax query to request a new planning with the updated info and replace the old one
  let script = "public/back/views/include/global/planning.php";
  let parent = "#planning-container";
  let params = {
    ajax: true,
    date,
    siteId,
    isRestricted
  };

  let res = ajaxRefreshElement(script, parent, params);

  $("#planning-date-change").datepicker(
  {
    onSelect: updatePlanningWithDate
  });

  return res;
}

function updatePlanningWithSiteName(siteId, item) {
  let date = $("#planning-date-change").val().split("/").join("-");
  let isRestricted = isShowRentalsChecked();

  let ret = updateUserRentals(siteId);

  if (!ret) return;

  if (typeof item != "undefined") {
      // Update the max available qty of a given item
      ret = updateSelectedItemQty(siteId, item);
      if (!ret) return;
  }

  // Ajax query to request a new planning with the updated info and replace the old one
  let script = "public/back/views/include/global/planning.php";
  let parent = "#planning-container";
  data = {
    ajax: true,
    date,
    siteId,
    isRestricted,
    item
  };

  ret = ajaxRefreshElement(script, parent, data);

  // $("#planning-date-change").datepicker(
  // {
  //   onSelect: updatePlanningWithDate
  // });

  updateDatepicker();

  return ret;
}

function updateSelectedItemQty(siteId, item) {
  let script = "private/scripts/db/fetch_item_available_qty.php";
  let data = {
    siteId,
    item
  };

  let failureCb = (res) => {
    if (!res.success) {
      $.growl.error({message: res.errMsg});
    }
  };

  let params = {
    data,
    script,
    async: false,
    failure: failureCb
  }

  let ret = ajaxExec(params);

  return ret;
}

function getPlanningCell($el) {
  return ($el.hasClass("planning-cell"))?$el:$($el.closest(".planning-cell")[0]);
}

function isShowRentalsChecked() {
  return ($("#show-rentals-checkbox").length > 0)?$("#show-rentals-checkbox").is(":checked"):null;
}
