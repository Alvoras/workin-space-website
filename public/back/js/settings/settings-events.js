$(document).ready(() => {
  $("#settings-profile-button").click(() => {
    let data = {};
    data["user-data"] = fetchInputVals($(".settings-profile-input"))
    data["mode"] = "user";

    let errorList = checkUserProfileForm(data["user-data"], data["mode"]);

    if (errorList.length > 0) {
      for (errorMsg of errorList) {
        $.growl.error({message: errorMsg});
      }

      return;
    }
    updateUserProfile(data);
  });

  $("#settings-pwd-button").click(() => {
    let data = {};
    data["form-data"] = fetchInputVals($(".settings-pwd-input"));
    data["mode"] = "pwd";

    let errorList = checkUserProfileForm(data["form-data"], data["mode"]);

    if (errorList.length > 0) {
      for (errorMsg of errorList) {
        $.growl.error({message: errorMsg});
      }

      return;
    }
    updateUserProfile(data);
  });

});
