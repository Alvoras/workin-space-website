function updateUserProfile(data) {
  let script = "private/scripts/db/update_user_profile.php";

  let notifyResult = (res) => {
    notify(res);
  };

  let params = {
    data,
    async: true,
    script,
    success: notifyResult,
    failure: notifyResult
  };

  ajaxExec(params);
}

function checkUserProfileForm(data, mode) {
  let errorList = [];

  switch (mode) {
    case "pwd":
      if (data.password !== data.passwordc) {
        errorList.push("Le mot de passe de confirmation ne correspond pas");
      }

      if (!validateInput(data.password, "password")) {
        errorList.push("Le mot de passe n'est pas valide");
      }else if (!validateInput(data.passwordc, "password")) {
        errorList.push("Le mot de passe n'est pas valide");
      }
      break;

      case "user":
        if (!validateInput(data.lastname, "lastname")) {
          errorList.push("Le nom de famille n'est pas valide");
        }

        if (!validateInput(data.firstname, "firstname")) {
          errorList.push("Le prénom n'est pas valide");
        }

        if (!validateInput(data.email, "email")) {
          errorList.push("L'email n'est pas valide");
        }
        break;
  }

  return errorList;
}
