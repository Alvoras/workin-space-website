$(document).ready(() => {
  $("#card-exp-1, #card-exp-2").keydown(function(e){
    preventChars(e, this, 2);
  });

  $("#card-cvv").keydown(function(e){
    preventChars(e, this, 3);
  });

  $("#card-no-1, #card-no-2, #card-no-3, #card-no-4").keydown(function(e){
    preventChars(e, this, 4);
  });

  $("#card-no-1").keyup(function(){
    focusNext(this, 4, "#card-no-2");
  });

  $("#card-no-2").keyup(function(){
    focusNext(this, 4, "#card-no-3")
  });

  $("#card-no-3").keyup(function(){
    focusNext(this, 4, "#card-no-4")
  });

  $("#card-no-4").keyup(function(){
    focusNext(this, 4, "#card-exp-1")
  });

  $("#card-exp-1").keyup(function(){
    focusNext(this, 2, "#card-exp-2")
  });

  $("#card-exp-2").keyup(function(){
    focusNext(this, 2, "#card-cvv")
  });
});
