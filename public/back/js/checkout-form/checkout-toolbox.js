function preventChars(e, el, limit) {
  if (e.keyCode != 13 && e.keyCode != 8 && e.keyCode != 9) {
    if (($(el).val().length >= limit) || (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  }
}

function focusNext(el, limit, next) {
  if ($(el).val().length >= limit) {
    $(next).focus();
  }
}

function hasEmpty(data) {
  let ret = false;

  jQuery.each(data, function(key) {
    if(data[key].trim().length < 1){
      ret = true;
    }
  });

  return ret;
}

function checkCheckoutForm(data) {
  if (hasEmpty(data)) {
    return false;
  }else if (
    data["card-no-1"].length != 4
  || data["card-no-2"].length != 4
  || data["card-no-3"].length != 4
  || data["card-no-4"].length != 4
  || data["card-exp-1"].length != 2
  || data["card-exp-2"].length != 2
  || data["card-cvv"].length != 3
  ) {
    return false;
  }

  return true;
}

function fetchCheckoutMode(el) {
  return el.data("checkout-mode");
}

function setCheckoutMode(mode) {
  document._data.checkoutMode = mode;
}

function fetchInputVals(el) {
  let data = {};

  el.each(function(ndx, input) {
    data[$(input).attr('name')] = $(input).val();
  });

  return data;
}

function fetchRenewDuration() {
  let card = $("#checkout-item-container").find(".renew-card");
  return card.data("duration");
}

function fetchNewSubId() {
  let card = $("#checkout-item-container").find(".back-glow-green");
  return card.data("id");
}
