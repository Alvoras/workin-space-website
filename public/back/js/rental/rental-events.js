document._data = {};
document._data.time = {};
$(document).ready(() => {

  document._data.siteId = $(".site-name-selected").data("siteid");
  document._data.year = (new Date()).getFullYear();

  $("#planning-container").click(function(e){
    let target = $(e.target);

    if (target.hasClass("reserved-cell")
    ||target.hasClass("planning-cell")
    ||target.hasClass("reserved-cell-container")
    ||target.is("th")
    ||target.is("tr")
    ||target.is("td")
    ||target.is("#planning-date-change")
    ||target.is("#table-header")
    ||target.is("#table-header > div")
    ||target.is("#planning-tooltip")
    ||target.is("#planning-tooltip > div")
    ||target.is("#tooltip-button-container")
    ||target.is("#tooltip-button-container > *")
    ||target.is("#tooltip-button-text")
    ||target.is("#show-rentals-checkbox-container")
    ||target.is("#show-rentals-checkbox-container > *")
    ){
      return;
     }

    let that = $(this);
    that.css("opacity", "0");
    setTimeout(() => {
      that.css("z-index", "-1");
    }, 200);
  });

  $("#tiles-row-container").click(function(e){
    // On click
    // Display the planning
    let target = $(e.target);
    let siteTile = target.closest(".site-tile");
    let siteId = document._data.siteId;

    if (siteTile.hasClass("disabled-tile")) return;

    let planning = $("#planning-container");

    if (siteTile.length > 0) {
      let item = siteTile.find(".site-tile-description-title").html().trim();
      document._data.item = item;

      updatePlanningWithSiteName(siteId, item);

      updateSelectedItemQty(siteId, item);

      planning.css("z-index", "998");
      setTimeout(() => {
        planning.css("opacity", "1");
      }, 50);
    }
  });

  $(".header-site-name").click(function() {
    let that = $(this);

    if (that.hasClass("site-name-selected")) return;

    let siteId = that.data("siteid");

    document._data.siteId = siteId;

    let target = $("#item-grid-container").data("gridtype");
    let parent = "#item-grid-container";
    let script = "private/scripts/db/fetch_site_content.php";

    let params = {
      siteId,
      target
    };

    let res = ajaxRefreshElement(script, parent, params);

    // If success, update the selected state of the buttons
    if (res) {
      $(".site-name-selected").removeClass("site-name-selected");
      that.addClass("site-name-selected");
    }

    let item = document._data.item;
    updatePlanningWithSiteName(siteId, item);
  });

  $("#planning-container").change(function(e){
    let that = $(e.target);
    if (that.is("#show-rentals-checkbox")||that.is("#show-rentals-text")) {
      let item = document._data.item;
      let state = isShowRentalsChecked();

      if (state) {
        $(`.reserved-cell[data-rental-name!="${item}"]`).addClass("hidden");
      }else {
        $(`.reserved-cell[data-rental-name!="${item}"]`).removeClass("hidden");
      }
    }
  });
});
