document._data = {};

$(document).ready( () => {
  var isDisappearing = false;

  setTimeout( () => {
    hideLoading();
  }, 500);

  // setInterval( () => {
  //   rollBacklight();
  // }, 200);

  $($("body")).click(function(e) {
    if ($(e.target).attr("id") != "side-menu-handle-container" && !$(e.target).hasClass("side-menu-handle")) {
      $("#side-menu-container").removeClass("display-bottom-menu");
    }
  });

  $(".side-menu-link").click( function(e) {
    e.preventDefault();
    let link = this.getAttribute("href");

    $("#loading-title").html(this.dataset.page);

    setTimeout( () => {
      window.location = link;
    }, 1000);
    showLoading();
  });

  $("#side-menu-handle-container").click(() => {
    $("#side-menu-container").toggleClass("display-bottom-menu");
  });

  $("#side-menu-handle-container").swipe({
    swipeUp:function(event, direction, distance, duration, fingerCount, fingerData) {
        $("#side-menu-container").toggleClass("display-bottom-menu");
        },
        threshold: 0,
        allowPageScroll : "none"
      });

  $("#option-button-upgrade").click(function() {
    if (isDisappearing) return;
    setCheckoutMode($(this).data("checkout-mode"));
    $("#card-selection-container").css("z-index", "997");
    setTimeout( () => {
      $("#card-selection-container").addClass("show-card-selection")
      $(".plan-card:not(.solo)").addClass("show-cards");
    }, 100);
  });

  $("#option-button-renew").click(function() {
    if (isDisappearing) return;
    console.log($(this));
    setCheckoutMode($(this).data("checkout-mode"));
    $("#renew-screen-container").css("z-index", "997");
    setTimeout( () => {
      $("#renew-screen-container").addClass("show-card-selection")
    }, 100);
  });

  $(".renew-card").click(function() {
    if (
      isDisappearing
    ){
      return;
    }

    let that = $(this);

    isDisappearing = true;

    $(".renew-card, #renew-screen-title").css({
      "opacity": 0
    });

    setTimeout( () => {
      isDisappearing = false;
      $(".renew-card").css("z-index", "-1");
    }, 500);

    $("#checkout-form-container").css("z-index", "998");
    setTimeout( () => {
      $("#checkout-form-container").addClass("display-checkout-form");

      //$("#checkout-form-button").addClass(`option-button-mem-${selectedCard.data("id")}`);
      let el = that.clone().appendTo("#checkout-item-container");
      $el = $(el);

      $el.removeClass("clickable");

      $el.css("z-index", "998");
      setTimeout(() => {
        $el.css("opacity", "1");
      }, 50);
      $("#checkout-item-container .plan-card").removeClass("fade-out-card");
    }, 800);
  });

  $("#renew-screen-container").click( function(e) {
    if (
      isDisappearing
      ||$(e.target).hasClass("renew-card-text")
      ||$(e.target).hasClass("renew-card")
    ){
      return;
    }

    let that = $(this);

    isDisappearing = true;

    that.removeClass("show-card-selection");
    setTimeout( () => {
      isDisappearing = false;
      that.css("z-index", "-1");
    }, 500);
  });

  $("#card-selection-container").click( function(e) {
    if ($(e.target).hasClass("glowing-card") || $(e.target).parents(".glowing-card").length > 0 || isDisappearing) {
      return;
    }

    let that = $(this);

    isDisappearing = true;

    that.removeClass("show-card-selection");
    setTimeout( () => {
      isDisappearing = false;
      that.css("z-index", "-1");
      $(".back-glow-green").removeClass("back-glow-green");
    }, 500);
    $(".plan-card:not(.solo)").removeClass("show-cards");
  });

  $("#checkout-form-exit").click( () => {
    let cardSelContainer = $("#card-selection-container");

    setTimeout(() => {
      $(".renew-card, #renew-screen-title").css({
        "opacity": 1
      });

      setTimeout(() => {
        $(".renew-card").css("z-index", "997");
      }, 200);
    }, 1500);

    $("#checkout-form-container, #renew-screen-container").removeClass("display-checkout-form");
    cardSelContainer.removeClass("show-card-selection");
    $(".plan-card:not(.solo)").removeClass("show-cards fade-out-card");

    setTimeout(() => {
      $("#checkout-form-container, #renew-screen-container").css("z-index", "-1");
      cardSelContainer.css("z-index", "-1");
      $(".back-glow-green").removeClass("back-glow-green");
      $("#checkout-item-container").html('');
    }, 500);
  });

  $(".glowing-card").click( (e) =>{
    if (isDisappearing) return;

    let selectedCard;
    let cardSelContainer = $("#card-selection-container");

    if ($(".show-cards").length < 3) {
      cardSelContainer.removeClass("show-card-selection");
      cardSelContainer.css("z-index", "-1");
      $(".plan-card:not(.solo)").removeClass("show-cards");
      return;
    }

    $(".glowing-card").each((index, el) => {
      if ($(e.target).closest(".glowing-card").data("id") != $(el).data("id")) {
        $(el).removeClass("show-cards").css("opacity", "0");
      }else {
        selectedCard = $(el);
        selectedCard.addClass("back-glow-green");
      }
    });

    $(".glowing-card").addClass("fade-out-card");

    selectedCard.css("transform", "translate3D(-100px, -20px, 0) !important");

    // Display checkout form
    $("#checkout-form-container").css("z-index", "998");
    setTimeout( () => {
      $("#checkout-form-container").addClass("display-checkout-form");

      //$("#checkout-form-button").addClass(`option-button-mem-${selectedCard.data("id")}`);
      selectedCard.clone().appendTo("#checkout-item-container");
      $("#checkout-item-container .plan-card").removeClass("fade-out-card");
    }, 800);
  });

  $("#checkout-form-button").click( function() {
    let that = $(this);

    let checkoutMode = document._data.checkoutMode;

    let script = "private/scripts/user/update_subscription.php";

    let formData = fetchInputVals($(".card-form-input"));

    if (!checkCheckoutForm(formData)) {
      $.growl.error({message: "Le formulaire n'est pas valide"});
      return;
    }

    let defaultDuration = 1; // month

    let data = {
      checkoutMode: document._data.checkoutMode
    };

    switch (checkoutMode) {
      case "renew":
        var renewDuration = fetchRenewDuration();
        data.duration = renewDuration;
        break;
      case "update":
        var newSubId = fetchNewSubId();
        data.duration = defaultDuration;
        data.newSubId = newSubId;
        break;
      case "subscribe":
        var subId = fetchNewSubId();
        data.duration = defaultDuration;
        data.subId = subId;
        break;
    }

    let notifyResult = (res) => {
      console.log(res);
      notify(res);
    };

    let successCb = (res) => {
      console.log(res);
      let cardSelContainer = $("#card-selection-container");
      notify(res);

      setTimeout(() => {
        $(".renew-card, #renew-screen-title").css({
          "opacity": 1
        });

        setTimeout(() => {
          $(".renew-card").css("z-index", "997");
        }, 200);
      }, 1500);

      $("#checkout-form-container, #renew-screen-container").removeClass("display-checkout-form");
      cardSelContainer.removeClass("show-card-selection");
      $(".plan-card:not(.solo)").removeClass("show-cards fade-out-card");

      setTimeout(() => {
        $("#checkout-form-container, #renew-screen-container").css("z-index", "-1");
        cardSelContainer.css("z-index", "-1");
        $(".back-glow-green").removeClass("back-glow-green");
        $("#checkout-item-container").html('');
      }, 500);
    }

    let params = {
      data,
      async: true,
      script,
      success: successCb,
      failure: notifyResult
    };

    console.log(params);

    ajaxExec(params);
  });
});
