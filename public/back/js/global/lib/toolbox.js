function hideLoading() {
  let screen = $("#loading-placeholder");
  screen.addClass("hide-loading")
  setTimeout(() => {
    screen.css("z-index", "-1");
  }, 300);
}

function showLoading() {
  let screen = $("#loading-placeholder");
  screen.removeClass("hide-loading");

  screen.css({
    transform: "translateX(-100px)",
    opacity: "0",
    "z-index": "999"
  });
  setTimeout(() => {
    screen.addClass("show-loading");
  }, 50);
}
/*
function rollBacklight() {
  // HS
  let glowAttr;

  $(".glowing-card").each((i, el) => {
    rng = Math.random();
    glowAttr = $(el).css("filter");
    $(el).css("filter", "1")
  });
}
*/
function time60ToTime100(min) {
  let buf = 60/min;

  return Number(((100/buf)/100).toFixed(2));
}
