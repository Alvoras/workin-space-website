$(document).ready(() => {
  $("#tile-container").click( function(e) {
    let that = $(e.target);

    if (that.closest(".site-tile").length === 0) return;

    let tile = that.closest(".site-tile");

    let tileImg = tile.find(".site-tile-image");
    let tileDesc = tile.find(".site-tile-description");

    let threeTexture = tile.data("threetxt");

    $("#more-tile-container").css("z-index", "999");
    $("#three-viewer").css("display", "block");
    setTimeout(() => {
      $("#more-tile-container").css("opacity", "1");
    }, 100);

    $("#more-tile-img-container").html(tileImg.clone());
    $("#more-tile-description").html(tileDesc.clone());

    //let data = initThreeViewer(threeTexture);
    initThreeViewer(threeTexture);
    //animateThreeViewer(data);
  });

  $("#more-tile-container").click( function() {
    animateThree = false;

    $(this).css("opacity", "0");
    setTimeout(() => {
      $(this).css("z-index", "-1");
      $("#three-viewer").css("display", "none");
    }, 200);
  });

  $(".header-site-name").click( function() {
    let that = $(this);
    if ( $(this).hasClass("site-name-selected")) return;

    let siteId = that.data("siteid");
    let script = "private/scripts/db/fetch_site_content.php";

    $(".site-name-selected").removeClass("site-name-selected");
    $(`.header-site-name[data-siteid=${siteId}]`).addClass("site-name-selected");

    let data = {
      siteId,
      target: "room",
      ajax: true
    };

    let callback = (siteContent) => {
      console.log("callback");
      $("#tile-container").html(siteContent);
    };

    let failureCb = () => {
      let errorContent = "\
      <div>\
        <h3>Impossible de charger les sites :(</h3>\
      </div>";
      $("#tile-container").html(errorContent);
    };

    let params = {
      data,
      async: true,
      script,
      callback,
      failure: failureCb
    };

    ajaxFetchPHP(params);

  });
});
