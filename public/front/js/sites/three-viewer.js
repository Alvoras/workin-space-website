function initThreeViewer(texture) {
  console.log("Three init");
    let camera, scene, renderer, sphere; //INIT
    let width, height;
    let visu = document.getElementById('three-viewer');

    //texture = "public/front/img/pano/meeting_room_3d.jpg";

    let onWindowResize = function (width, height) {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( width, height );
    }

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.x = 0.1;

    width = $('#three-viewer').width();
    height = window.innerHeight / 2;

    renderer = new THREE.WebGLRenderer();
    renderer.domElement.id = "three-viewer-canvas";
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( width, height );

    sphere = new THREE.Mesh(
        new THREE.SphereGeometry(100, 32, 32),
        new THREE.MeshBasicMaterial({
          map: new THREE.TextureLoader().load(texture)
        })
    );
    sphere.scale.x = -1;
    scene.add(sphere);

    // Light
    let light = new THREE.DirectionalLight( 0xbbbbbb );
    light.position.set(1,1,1);
    scene.add(light);

    let light2 = new THREE.DirectionalLight( 0xbbbbbb );
    light2.position.set(-0.5,-0.5,-0.5);
    scene.add(light2);

    let ambientLight = new THREE.AmbientLight( 0x404040 ); // soft white light
    scene.add( ambientLight );

    visu.innerHTML = '';
    visu.appendChild(renderer.domElement);

    window.addEventListener( 'resize', onWindowResize, false );

    console.log("Three controls init");

    let controls = initControls(camera, visu);

    render(renderer, scene, camera, controls);
    //return {renderer, scene, camera, controls};
}

function render(renderer, scene, camera, controls) {
  console.log("Three animate");
    controls.update();
    requestAnimationFrame(() => {
      render(renderer, scene, camera, controls);
    });
    renderer.render(scene, camera);
}

function onMouseWheel(event) {
    event.preventDefault();

    if (event.wheelDeltaY) { // WebKit
        camera.fov -= event.wheelDeltaY * 0.05;
    } else if (event.wheelDelta) { 	// Opera / IE9
        camera.fov -= event.wheelDelta * 0.05;
    } else if (event.detail) { // Firefox
        camera.fov += event.detail * 1.0;
    }
    camera.fov = Math.max(40, Math.min(100, camera.fov));
    camera.updateProjectionMatrix();
}

function initControls(camera, visu)
{
    var controls = new THREE.OrbitControls(camera, visu);
    controls.enablePan = false;
    controls.autoRotate = true;
    controls.autoRotateSpeed = 0.5;
    controls.enableZoom = false;

    return controls;
}

function animateThreeViewer(data) {
    data.controls.update();
    requestAnimationFrame( () => {
      animateThreeViewer(data);
    });
    data.renderer.render( data.scene, data.camera );
}
