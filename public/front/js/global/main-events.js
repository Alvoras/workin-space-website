$(document).ready( () => {
  let orbits = document.getElementsByClassName('orbit');

  if (orbits.length > 0) {
    let rng = (Math.random()*100)%180;
    let rng2 = (Math.random()*100)%180;
    let rng3 = (Math.random()*100)%180;
    let rng4 = (Math.random()*100)%180;

    rng = (rng > 45) ? -rng: rng;
    rng2 = (rng2 > 45) ? -rng2: rng2;
    rng3 = (rng3 > 45) ? -rng3: rng3;
    rng4 = (rng3 > 45) ? -rng3: rng3;

    let cnt = rng;
    let cnt2 = rng2;
    let cnt3 = rng3;
    let cnt4 = rng4;

    //initOrbits();

    setInterval( () => {
      $(orbits[0]).css("transform", `rotate(${cnt%360}deg)`);
      $(orbits[1]).css("transform", `rotate(${cnt2%360}deg)`);
      $(orbits[2]).css("transform", `rotate(${cnt3%360}deg)`);
      $(orbits[3]).css("transform", `rotate(${cnt4%360}deg)`);

      cnt += 0.12;
      cnt2 += 0.3;
      cnt3 += 0.45;
      cnt4 += 0.90;
    }, 30);
  }

  $(".explodable-trigger").click( () => {
     //toggleExplode();
  });
});
