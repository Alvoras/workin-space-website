function toggleExplode() {
    $(".explodable").toggleClass("explode");
}

function initOrbits() {
    let orbits = document.getElementsByClassName('orbit');

    for (el of orbits) {
        $(el).css("bottom", `-${$(el).height()/2}px`);
    }
}
/*
function ajaxProcessPHP(script, data) {
  let params = {
    url: script,
    type: "POST",
    async: false,
    data
  };

  $.ajax(params).done(function(msg) {
    ret = msg;
  }).fail(function(jqXHR, statusText) {
    console.log(jqXHR, statusText);
    ret = null;
  });

  return ret;
}

function ajaxExec(script, data) {
  if (typeof data == 'undefined') var data = null;
  console.log(data);

  let ret = {
    success: false,
    errorMsg: 'Something went wrong'
  };

  let params = {
    url: script,
    type: "POST",
    async: false,
    data
  };

  $.ajax(params).done(function(msg) {
    ret = JSON.parse(msg);
  }).fail(function(jqXHR, statusText) {
    console.log(jqXHR, statusText);
    ret.errorMsg = `Query failed (${statusText})`;
  });
  return ret;
}
*/

function checkLoginForm(data, mode) {
  let errorList = [];

  switch (mode) {
    case "login":
      if (!validateInput(data.email, "email")) {
        errorList.push("L'email n'est pas valide");
      }

      if (!validateInput(data.password, "password")) {
        errorList.push("Le mot de passe n'est pas valide");
      }
      break;
    case "register":
      if (!validateInput(data.lastname, "lastname")) {
        errorList.push("Le nom de famille n'est pas valide");
      }

      if (!validateInput(data.firstname, "firstname")) {
        errorList.push("Le prénom n'est pas valide");
      }

      if (data.email !== data.emailc) {
        errorList.push("L'email de confirmation ne correspond pas");
      }

      if (!validateInput(data.email, "email")
      ||!validateInput(data.emailc, "email")) {
        errorList.push("L'email n'est pas valide");
      }

      if (data.password !== data.passwordc) {
        errorList.push("Le mot de passe de confirmation ne correspond pas");
      }

      if (!validateInput(data.password, "password")
      || !validateInput(data.passwordc, "password")) {
        errorList.push("Le mot de passe n'est pas valide");
      }

      if (!validateInput(data.phone, "phone")) {
        errorList.push("Le numéro de téléphone n'est pas valide");
      }
      break;
  }

  return errorList;
}
