const SCROLL_UP = 1;
const SCROLL_DOWN = 2;

function animateBackground(interval) {
	let angle = 135;
	setInterval(() => {
		$("#background-container").css("background", `linear-gradient(${angle}deg, #000, #003060)`);
		angle++;
		//console.log(`linear-gradient(${angle}deg, #000, #003060)`);
	}, interval);
}

function getCurrentSelectedScrollIndicator() {
	let i = 0;
	let scrollIndicatorBar = $(".scroll-indicator-bar");

	// Find the current selected scroll indicator
	for (i = 0; i < scrollIndicatorBar.length - 1; i++) {
		if ($(scrollIndicatorBar[i]).hasClass("selected-scroll-bar")) {
			break;
		}
	}

	return i;
}

function getDirection(currentStepId, targetStepId) {
	return (currentStepId > targetStepId) ? SCROLL_UP : SCROLL_DOWN;
}

function changeScrollIndicator(currentStepId, targetStepId, cb) {
	// Callback is needed since we need to pass the reference of the cancelScroll var from the events page


	let scrollIndicatorBar = $(".scroll-indicator-bar");

	let direction = getDirection(currentStepId, targetStepId);
	console.log(currentStepId, targetStepId, scrollIndicatorBar.length);

	switch (direction) {
		case SCROLL_UP:
			if (currentStepId <= 0) {
				// Timeout to avoid scroll trigger overload
				setTimeout(() => {
					cb();
				}, 100);
				return
			};
			break;
		case SCROLL_DOWN:
			if (currentStepId >= scrollIndicatorBar.length - 1){
				// Timeout to avoid scroll trigger overload
				setTimeout(() => {
					cb();
				}, 100);
				return
			}
			break;
	}

	// Apply animation
	$(scrollIndicatorBar[currentStepId]).removeClass("selected-scroll-bar");
	$(scrollIndicatorBar[targetStepId]).addClass("selected-scroll-bar");

	// Re-enable scroll after animation (1sec)
	// !! Must be synced with transition time of .selected-scroll-bar and .scroll-indicator-bar in landing-step.css

	setTimeout(() => {
		cb();
	}, 1200);

	return
}

function scrollBackground(currentStepId, nextStepId, direction) {
	// Remove all scene-visible blurred background
	$('.background-blur-fadable').removeClass("background-blur-visible");
	// Add blurred version of next background
	$(`.background-blur-fadable.step-${nextStepId}`).addClass("background-blur-visible");
	// Position the transparent next background
	$(`.background-fadable.step-${nextStepId}`).addClass("background-visible");

	// Fade out current bg
	$(`.background-fadable.step-${currentStepId}`).removeClass("background-fade-in");

	// Remove current (now transparent) background from visible area
	setTimeout(() => {
		$(`.background-fadable.step-${currentStepId}`).removeClass("background-visible");
	}, 800);

	// Fade in next background
	setTimeout(() => {
		$(`.background-fadable.step-${nextStepId}`).addClass("background-fade-in");
	}, 800);

	// TEXT ANIMATION
	let nextTextContainer = $(`.text-container.step-${nextStepId}`);
	let currentTextContainer = $(`.text-container.step-${currentStepId}`);

	// Default : if direction === SCROLL_DOWN
	let currentSlideClass = "slide-text-container-down";
	let nextSlideClass = "slide-text-container-up";

	if (direction === SCROLL_UP) {
		currentSlideClass = "slide-text-container-up";
		nextSlideClass = "slide-text-container-down";
	}

	setTimeout( () => {
		// Position the transparent next text container
		nextTextContainer.addClass(`${nextSlideClass} text-container-visible`);
		// Slide and fade out the next text
		// Timeout
		setTimeout(() => {
			nextTextContainer.removeClass(`${nextSlideClass} text-container-fade-out`);
		}, 1000);

		// Slide and fade in the next text
		currentTextContainer.addClass(`${currentSlideClass} text-container-fade-out`);
		// Reset the state of the text moving away
		setTimeout(() => {
			currentTextContainer.removeClass(`${currentSlideClass} text-container-visible`)
		}, 1000);
	}, 150);
}
