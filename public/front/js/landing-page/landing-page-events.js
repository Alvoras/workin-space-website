$(document).ready(() => {
	const SCROLL_UP = 1;
	const SCROLL_DOWN = 2;
	let cancelScroll = false;

	// Bind for chrome and firefox events
	$(document).bind('mousewheel DOMMouseScroll', function(e) {
		// If scene is currently updating, cancel the call
		if (cancelScroll === true) return;

		let currentStepId = getCurrentSelectedScrollIndicator();
		let i = 0;
		let scrollIndicatorBar = $(".scroll-indicator-bar");

		cancelScroll = true;

		// Chrome || Firefox
		if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
			// Scroll up
			changeScrollIndicator(currentStepId, currentStepId - 1, () => {
				cancelScroll = false;
			});

			if (currentStepId > 0) {
				scrollBackground(currentStepId, currentStepId - 1, SCROLL_UP);
			}
		} else {
			// Scroll down
			changeScrollIndicator(currentStepId, currentStepId + 1, () => {
				cancelScroll = false;
			});

			if (currentStepId < $(".scroll-indicator-bar").length - 1) {
				scrollBackground(currentStepId, currentStepId + 1, SCROLL_DOWN);
			}
		}
	});

	$(".scroll-indicator-bar").click(function() {
		if (cancelScroll === true) return;

		cancelScroll = true;
		let targetStepId = $(this).data("step");
		let currentStepId = getCurrentSelectedScrollIndicator();

		if (currentStepId === targetStepId) {
			return;
		}

		changeScrollIndicator(currentStepId, targetStepId, () => {
			cancelScroll = false;
		});

		let direction = getDirection(currentStepId, targetStepId);

		scrollBackground(currentStepId, targetStepId, direction);
	});

	$("#continue-button").click(() => {
		// Scrool down one screen
	});

	//animateBackground(300);
});
