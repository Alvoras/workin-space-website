$(document).ready(function() {
	// initOrbits();
	 displayErrors();

	$("#fp-validate-email").click(() => {
		// Check if email exists
		// If yes, then trigger another ajax to send mail and generate token in db
		// If not, growl an error

		let email = fetchInputVals($(".fp-input")).email;

		if (email.length < 1) {
			$.growl.error({
				message: "Le champ email est vide"
			});
			return;
		}

		let script = "private/scripts/db/send_fp_mail.php";
		let data = {
			email
		};

		let notifyResult = (res) => {
			notify(res);
		};

		let params = {
			data,
			async: true,
			script,
			success: notifyResult,
			failure: notifyResult
		};

		ajaxExec(params);

	});

	$("#fp-validate-new-password").click(() => {
		let script = "private/scripts/db/update_password.php";

		let email = document._email;

		let formData = fetchInputVals($(".fp-input"));

		let isFormDataValid = validateNewPasswordForm(formData);

		if (!isFormDataValid.success) {
			$.growl.error({
				message: isFormDataValid.errMsg
			});
			return;
		}

		let password = formData.password;

		let data = {
			email,
			password
		};

		let successCb = (res) => {
			notify(res);

			setTimeout(() => {
				window.location = "/p/login";
			}, 2500);
		};

		let notifyResult = (res) => {
			notify(res);
		};

		let params = {
			data,
			async: true,
			script,
			success: successCb,
			failure: notifyResult
		};

		ajaxExec(params);
	});
});
