function validateNewPasswordForm(data) {
  let ret = {};
  ret["success"] = false;

  if (data.password !== data.passwordc) {
    ret["errMsg"] = "Les mots de passe ne correspondent pas";
    return ret;
  }

  if (validateInput(data.password, "password")) {
    ret["success"] = true;
  }else {
    ret["errMsg"] = "Le mot de passe n'est pas valide";
  }

  return ret;
}
