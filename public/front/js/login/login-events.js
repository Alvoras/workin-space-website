document._data = {};

var captchaCb = function (res) {
	document._data.captchaResponse = res;
}

$(document).ready(() => {
	if (typeof document._activated != "undefined") {
		if (document._activated === true) {
			$.growl.success({message: `Le compte associé à ${document._email} a bien été activé`});
		}else {
			$.growl.error({message: `Le compte associé à ${document._email} n'a pas pu être activé`});
		}
	}

	displayErrors();

	$(".form").submit(function(e) {
		e.preventDefault();

		// setTimeout(() => {
		// 	this.submit();
		// }, 500);
	});

	$(".form-title").click(function() {
		if ($(this).hasClass("selected-form-title")) return;

		let that = $(this);

		let selected = that.data("title");

    if (selected === "login") {
      var unselected = "register";
      var selTranslateVal = "0";
      var unselTranslateVal = "473px";
    }else{
      var unselected = "login";
      var unselTranslateVal = "-473px";
      var selTranslateVal = "-243px";
    }

    $(`#${unselected}-form`).css("transform", `translateX(${unselTranslateVal})`);
    $(`#${selected}-form`).css("transform", `translateX(${selTranslateVal})`);

		$(".selected-form-title").removeClass("selected-form-title");
		that.addClass("selected-form-title");
	});

	$("#login-button").click(()=> {
		let mode = "login";
		let data = fetchInputVals($(".login-form-input"));
		data["remember_me"] = $("#remember-me").is(":checked");

		let errorList = checkLoginForm(data, mode);

		if (errorList.length > 0) {
			for (errMsg of errorList) {
				$.growl.error({message:errMsg});
			}
			return;
		}

		let loginForm = document.getElementById('login-form');
		loginForm.submit();
	});

	$("#register-button").click(function(){
		// ajax exec register script
		// If success then notify that a mail has been sent/register successfull
		// trigger click on the connexion button

		let data = fetchInputVals($(".register-form-input"));
		// let successMsg = `Un email a été envoyé à ${data.email}`;
		data["g-recaptcha-response"] = document._data.captchaResponse;
		let script = "private/scripts/user/register_user.php";
		let mode = "register";

		let errorList = checkLoginForm(data, mode);

		if (errorList.length > 0) {
			for (errMsg of errorList) {
				$.growl.error({message:errMsg});
			}
			return;
		}

		let successCb = (res) => {
			notify(res);
			// $.growl.success({message:successMsg});
			$("#login-title").trigger("click");
		};

		let failureCb = (res) => {
			for (let errMsg of res.errors) {
				$.growl.error({message:errMsg});
			}
		};

		let params = {
			data,
			async: true,
			script,
			success: successCb,
			failure: failureCb
		};

		ajaxExec(params);

		// Asynchronous
		/*let res = ajaxExec(script, data);

    if (res.success) {
      $.growl.success({message:successMsg});
			$("#login-title").trigger("click");
    }else {
			for (let errMsg of res.errors) {
				$.growl.error({message:errMsg});
			}
    }*/
	});

	$(".ball-trigger-button").click( function(e) {
		let posx = e.clientX;
		let posy = e.clientY;
		let pagew = window.innerWidth;

		let ballw = pagew*2.35;
		let ballh = pagew*2.35;

		if (this.tagName.toLowerCase() === "a") {
			e.preventDefault();
			let link = this.getAttribute("href");
			setTimeout( () => {
				window.location = link;
			}, 500);
		}
			$("#masking-ball").css({left:`${(ballw/2)+posx}px`, top:`${(ballh/2)+posy}px`, transition:".8s all"});
			setTimeout( () => {
				$("#masking-ball").addClass("expand-ball");
			}, 100);
	});
});
