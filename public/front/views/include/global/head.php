<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
      <base href="../" />
      <title>Workin' Space - Espaces de coworking à Paris</title>
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,900,900i" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700" rel="stylesheet">
      <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/main.css"/>
      <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/lib/bootstrap.min.css">
      <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/lib/icomoon.css"/>
      <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/lib/jQueryUI/jquery-ui.min.css"/>
      <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/lib/jQueryUI/jquery-ui.theme.min.css"/>
      <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/lib/jquery.growl.css"/>
      <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/global/header.css"/>
