<div id="header-container" class="flex-center-row">
    <!--<div id="header-logo-container">
        <img id="header-logo" src="<?=IMG_FRONT_ROOT?>/logo/ws_logo_white_thiner.png" alt="">
    </div>-->
    <ul id="header-menu" class="flex-center-row">
        <li class="header-menu-title-container flex-center-col explodable-trigger"><a href="/p/home" class="header-menu-title">Accueil<div class="header-underline"></div></a>
        <!--<li class="header-menu-title-container flex-center-col explodable-trigger"><a href="/p/?p=coworking" class="header-menu-title">Coworking<div class="header-underline"></div></a>-->
        <li class="header-menu-title-container flex-center-col explodable-trigger"><a href="/p/membership" class="header-menu-title">Formules<div class="header-underline"></div></a>
        <li class="header-menu-title-container flex-center-col explodable-trigger"><a href="/p/sites" class="header-menu-title">Nos espaces<div class="header-underline"></div></a>
        <!--<li class="header-menu-title-container flex-center-col explodable-trigger"><a href="/p/?p=partnership" class="header-menu-title">Partenaires<div class="header-underline"></div></a>-->
        <li class="header-menu-title-container flex-center-col explodable-trigger"><a href="/p/login" class="header-menu-title">Connexion<div class="header-underline"></div></a>
    </ul>
</div>
