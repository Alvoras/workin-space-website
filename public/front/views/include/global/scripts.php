    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?=INCLUDE_FRONT_JS?>/global/lib/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <script src="<?=INCLUDE_FRONT_JS?>/global/lib/jquery-3.3.1.min.js"></script>
    <script src="<?=INCLUDE_GLOBAL_JS?>/jQueryUI/jquery-ui.min.js"></script>
    <script src="<?=INCLUDE_GLOBAL_JS?>/jQueryUI/datepicker-fr.js"></script>
    <script src="<?=INCLUDE_GLOBAL_JS?>/toolbox.js"></script>
    <script src="<?=INCLUDE_FRONT_JS?>/global/main-events.js"></script>
    <script src="<?=INCLUDE_FRONT_JS?>/global/lib/toolbox.js"></script>
    <script src="<?=INCLUDE_GLOBAL_JS?>/touchswipe/jquery.touchSwipe.min.js"></script>
    <script src="<?=INCLUDE_GLOBAL_JS?>/growl/jquery.growl.js"></script>
