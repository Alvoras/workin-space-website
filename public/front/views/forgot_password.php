<?php require_once INCLUDE_FRONT_VIEWS.'/global/head.php' ?>
 <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/fp/fp.css"/>
</head>
<body>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/header.php" ?>
  <div id="top-background-container"></div>
    <div id="main-container" class="flex-center-col">
      <?php include INCLUDE_FRONT_VIEWS."/global/orbits.php" ?>
      <?php
        if (isset($_SESSION["location"])) {
          unset($_SESSION["location"]);
        }

        if (isset($_SESSION["verify_token"]) && $_SESSION["verify_token"]) {
          unset($_SESSION["verify_token"]);
          ?>
          <script>document._email = '<?=$_SESSION["email"]?>'</script>
          <?php
          unset($_SESSION["email"]);
      ?>
      <?php require_once INCLUDE_SCRIPT."/login/load_errors.php" ?>

          <div id="fp-text-container" class="flex-center-col">
            <h5 id="fp-title">
              Entrez le nouveau mot de passe :
            </h5>
            <input type="password" class="fp-input" name="password" id="fp-password" placeholder="Mot de passe">
            <h5 id="fp-title">
              Confirmation :
            </h5>
            <input type="password" class="fp-input" name="passwordc" id="fp-passwordc" placeholder="Confirmation du mot de passe">
            <div id="fp-button-container">
              <button class="fp-button" id="fp-validate-new-password">
                Envoyer
              </button>
              <div class="button-underline"></div>
            </div>
          </div>
          <?php
        }else {
          ?>
          <div id="fp-text-container" class="flex-center-col">
            <h5 id="fp-title">
              Entrez votre email :
            </h5>
            <input type="text" name="email" class="fp-input" id="fp-email" placeholder="Email">
            <div id="fp-button-container">
              <button class="fp-button" id="fp-validate-email">
                Envoyer
              </button>
              <div class="button-underline"></div>
            </div>
          </div>
          <?php
        }
       ?>
    </div>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/scripts.php" ?>
  <script src="<?=INCLUDE_FRONT_JS?>/login/login-animation.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/fp/fp-toolbox.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/fp/fp-events.js"></script>
<?php require_once INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
