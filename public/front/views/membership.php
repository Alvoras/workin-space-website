<?php require_once INCLUDE_FRONT_VIEWS.'/global/head.php' ?>
  <link rel="stylesheet" href="<?=INCLUDE_GLOBAL_CSS?>/sub-cards.css"/>
 <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/membership/membership.css"/>
</head>
<body>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/header.php" ?>
    <div id="main-container">

      <div id="plans-card-container" class="flex-center-row">
        <?php include INCLUDE_GLOBAL_VIEWS."/card.php" ?>
      </div>

    </div>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/scripts.php" ?>
  <script src="<?=INCLUDE_FRONT_JS?>/membership/membership-events.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/membership/membership-animation.js"></script>
  <?php require_once INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
