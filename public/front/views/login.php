<?php require_once INCLUDE_FRONT_VIEWS.'/global/head.php' ?>
 <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/login/login.css"/>
</head>
<body>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/header.php" ?>
  <?php
    dump($_COOKIE);
    if (isset($_SESSION["location"])) {
      unset($_SESSION["location"]);
    }
  ?>
  <?php require_once INCLUDE_SCRIPT."/login/load_errors.php" ?>

    <div id="main-container" class="flex-center-col">

      <?php include INCLUDE_FRONT_VIEWS."/global/orbits.php" ?>
      <?php include INCLUDE_GLOBAL_VIEWS."/transition-ball.php" ?>

      <div id="form-title-container" class="flex-center-row">
        <h2 id="login-title" data-title="login" class="form-title selected-form-title">Connexion</h2>
        <span id="form-title-separator"></span>
        <h2 id="register-title" data-title="register" class="form-title">Inscription</h2>
      </div>

      <div id="forms-container" class="flex-center-row">
        <div id="login-form-container" class="form-container flex-center-col">
          <form id="login-form" class="form flex-center-col" action="/a/login" method="POST">
            <div id="login-email-container" class="text-input-container flex-center-col">
              <input class="text-input login-form-input" type="text" name="email" placeholder="Email" value="">
            </div>

            <div id="login-password-container" class="text-input-container flex-center-col">
              <input class="text-input login-form-input" type="password" placeholder="Mot de passe" name="password" value="">
            </div>

            <div id="remember-me-container">
              <input type="checkbox" id="remember-me" class="login-form-input" name="remember_me" value=''>
              <label for="remember-me" id="remember-me-label">Se souvenir de moi</label>
            </div>

            <div id="login-button-container">
              <input id="login-button" class="form-button" type="submit" name="connect" value="Connexion">
              <div class="button-underline"></div>
            </div>

            <div id="forgot-password-container">
              <a href="?p=fp" id="forgot-password-text">Mot de passe oublié ?</a>
            </div>
          </form>
        </div>

        <div id="register-form-container" class="form-container flex-center-col">
          <?php

            if(isset($_SESSION["errors_form"]) && isset($listOfErrors)) {
              echo '<div class="errorList col-xs-12"><div><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><ul>';
              foreach ($_SESSION["errors_form"] as $error) {
                echo "<li>".$listOfErrors[$error];
              }
              echo "</ul></div></div>";
              unset($_SESSION["errors_form"]);

              $dataForm = $_SESSION["data_form"];
            }
          ?>
          <form id="register-form" class="form flex-center-col" action="" method="POST">
            <div id="register-lastname-container" class="text-input-container flex-center-col">
              <input class="text-input register-form-input" type="text" name="lastname" placeholder="Prénom" required="required" value="<?php echo(isset ($dataForm["lastname"]))?$dataForm["lastname"]:"";?>">
            </div>

            <div id="register-firstname-container" class="text-input-container flex-center-col">
              <input class="text-input register-form-input" type="text" name="firstname" placeholder="Nom" required="required" value="<?php echo(isset ($dataForm["firstname"]))?$dataForm["firstname"]:"";?>">
            </div>

            <div id="register-email-container" class="text-input-container flex-center-col">
              <input class="text-input register-form-input" type="email" name="email" placeholder="Email" required="required" value="<?php echo(isset ($dataForm["email"]))?$dataForm["email"]:"";?>">
            </div>

            <div id="register-confirm-email-container" class="text-input-container flex-center-col">
              <input class="text-input register-form-input" type="email" name="emailc" placeholder="Confirmer email" required="required" value="<?php echo(isset ($dataForm["emailc"]))?$dataForm["emailc"]:"";?>">
            </div>

            <div id="register-password-container" class="text-input-container flex-center-col">
              <input class="text-input register-form-input" type="password" placeholder="Mot de passe" name="password" value="">
            </div>

            <div id="register-password-confirm-container" class="text-input-container flex-center-col">
              <input class="text-input register-form-input" type="password" placeholder="Confirmer mot de passe" name="passwordc" value="">
            </div>

            <div id="register-phone-container" class="text-input-container flex-center-col">
              <input class="text-input register-form-input" type="text" name="phone" placeholder="Téléphone" required="required" value="<?php echo(isset ($dataForm["phone"]))?$dataForm["phone"]:"";?>">
            </div>
            <div class="g-recaptcha" data-sitekey="6Ld0bk0UAAAAAKbNNN8r-NuzYWWdE8uYnDIC2Sdo" data-callback="captchaCb"></div>

            <div id="forgot-password-container" class="flex-center-row">
              <input type="checkbox" class="register-form-input" id="cgu-button" name="cgu" value="">
              <label for="cgu-button" id="cgu-label">J'accepte les <a id="cgu-link" href="/p/cgu">CGU</a></label>
            </div>
            <div id="register-button-container">
              <input id="register-button" class="form-button" type="button" name="register" value="Inscription">
              <div class="button-underline"></div>
            </div>

          </form>
        </div>

    </div>

  </div>

  <script src='https://www.google.com/recaptcha/api.js'></script>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/scripts.php" ?>
  <script src="<?=INCLUDE_FRONT_JS?>/login/login-events.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/login/login-animation.js"></script>
  <?php
    if (isset($_SESSION["verify_token"]) && $_SESSION["verify_token"]) {
      unset($_SESSION["verify_token"]);
      ?>
        <script>$.growl.success({message: 'Votre compte a bien été activé'})</script>
      <?php
    }

    if (isset($_SESSION["need_reconnect"]) && $_SESSION["need_reconnect"]) {
      unset($_SESSION["need_reconnect"]);
      echo "<script>displayError('Vous avez été déconnecté')</script>";
    }
   ?>
  <?php require_once INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
