      <?php require_once INCLUDE_FRONT_VIEWS.'/global/head.php' ?>
       <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/landing-page/landing-page.css"/>

       <link rel="preload" href="../../img/bg/open-space.jpg" as="image">
       <link rel="preload" href="../../img/bg/together_dark.jpg" as="image">
       <link rel="preload" href="../../img/bg/working.jpg" as="image">
       <link rel="preload" href="../../img/bg/coffee.jpg" as="image">
       <link rel="preload" href="../../img/bg/table.jpg" as="image">
   </head>
   <body>
      <?php require_once INCLUDE_FRONT_VIEWS."/global/header.php" ?>
      <div id="main-container">
        <div id="scroll-indicator-container" class="flex-center-col">
          <div id="scroll-indicator" class="flex-center-col">

             <!-- Scroll indicators -->
            <div data-step="0" class="scroll-indicator-bar selected-scroll-bar"></div>
            <?php
                for ($i=1; $i < 5; $i++) {
                    echo "<div data-step=".$i." class=\"scroll-indicator-bar\"></div>";
                }
            ?>

          </div>
        </div>

        <!-- Screen backgrounds -->
        <?php
            for ($i=1; $i < 5; $i++) {
                echo "<div id=\"background-blur-step".$i."-container\" class=\"background-container background-blur-fadable flex-center-col step-".$i."\"></div>";
                echo "<div id=\"background-step".$i."-container\" class=\"background-container background-fadable flex-center-col step-".$i."\"></div>";
            }
        ?>
        <div id="background-blur-step0-container" class="background-container flex-center-col step-0 background-blur-fadable"></div>
        <div id="background-step0-container" class="background-container flex-center-col step-0 background-fadable background-visible background-fade-in"></div>

         <!-- Screen contents -->
         <div id="landing-screen-container"  class="flex-center-col screen-container">
           <div class="text-container fadable-text-container slidable-text-container text-container-visible step-0">
             <div id="landing-title-container" class="flex-center-col step-0 screen-title-container">
                 <div id="landing-title-logo-container" class="flex-center-col">
                    <img id="landing-title-logo" src="<?=IMG_FRONT_ROOT?>/logo/ws_logo_white_split_hard.png" alt="">
                 </div>
                <h1 id="landing-title" class="screen-title screen-text">Le meilleur espace de coworking,<br />Que ce soit sur Terre ou dans l'espace.</h1>
             </div>
           </div>
           <!--<div id="continue-container" class="flex-center-col step-0">
              <a href="#" id="continue-button">Découvrir</a>
           </div>-->
         </div>

         <div id="first-step-container" class="flex-center-col screen-container">
           <div class="text-container fadable-text-container slidable-text-container text-container-fade-out step-1 flex-center-col">
             <div id="first-step-card-title-container" class="step-1 screen-title-container">
                 <h1 id="first-step-card-title-text" class="screen-title screen-text">Première étape :</h1>
             </div>
             <div id="first-step-card-description-container" class="step-1 screen-description-container">
               <h3 id="first-step-card-description" class="screen-description screen-text">Venez être accueilli par nos équipe</h3>
             </div>
             <div id="first-step-card-subsection-container" class="step-1 screen-subsection-container">
               <h3 id="first-step-card-subsection" class="screen-subsection screen-text">Plusieurs agents vous attendent à chacun de nos espaces pour vous conseiller et vous accompagner dans votre confort.</h3>
             </div>
           </div>
         </div>

         <div id="second-step-container" class="flex-center-col screen-container">
           <div class="text-container fadable-text-container slidable-text-container text-container-fade-out step-2 flex-center-col">
             <div id="second-step-card-title-container" class="step-2 screen-title-container">
                 <h1 id="second-step-card-title-text" class="screen-title screen-text">Deuxième étape :</h1>
             </div>
             <div id="second-step-card-description-container" class="step-2 screen-description-container">
               <h3 id="second-step-card-description" class="screen-description screen-text">Profitez d'une boisson chaude. C'est à volonté !</h3>
             </div>
             <div id="second-step-card-subsection-container" class="step-2 screen-subsection-container">
               <h3 id="second-step-card-subsection" class="screen-subsection screen-text">Que ce soit à la journée ou au mois, les boissons chaudes sont en accès illimité aux bars de tous nos espaces ! Régalez-vous avec les meilleurs produits de nos partenaires, sélectionnés avec soin pour nos clients.</h3>
             </div>
           </div>
         </div>

         <div id="third-step-container" class="flex-center-col screen-container">
           <div class="text-container fadable-text-container slidable-text-container text-container-fade-out step-3 flex-center-col">
             <div id="third-step-card-title-container" class="step-3 screen-title-container">
                 <h1 id="third-step-card-title-text" class="screen-title screen-text">Troisième étape :</h1>
             </div>
             <div id="third-step-card-description-container" class="step-3 screen-description-container">
               <h3 id="third-step-card-description" class="screen-description screen-text">Installez-vous confortablement à votre espace de travail</h3>
             </div>
             <div id="third-step-card-subsection-container" class="step-3 screen-subsection-container">
               <h3 id="third-step-card-subsection" class="screen-subsection screen-text">Box, salon cosy, open space cloisonné : trouvez l'espace de travail qui vous correspond le plus et laissez votre créativité s'épanouir.</h3>
             </div>
           </div>
         </div>

         <div id="fourth-step-container" class="flex-center-col screen-container">
           <div class="text-container fadable-text-container slidable-text-container text-container-fade-out step-4 flex-center-col">
             <div id="fourth-step-card-title-container" class="step-4 screen-title-container">
                 <h1 id="fourth-step-card-title-text" class="screen-title screen-text">Unified : Ne payez qu'une fois</h1>
             </div>
             <div id="fourth-step-card-description-container" class="step-4 screen-description-container">
               <h3 id="fourth-step-card-description" class="screen-description screen-text">Commandez, scannez et c'est gagné !</h3>
             </div>
             <div id="fourth-step-card-subsection-container" class="step-4 screen-subsection-container">
               <h3 id="fourth-step-card-subsection" class="screen-subsection screen-text">Grâce à notre système Unified, choisissez d'être débité directement sur votre compte Workin' Space pour une expérience totalement transparente</h3>
             </div>
           </div>
         </div>

      </div>
      <?php require_once INCLUDE_FRONT_VIEWS."/global/scripts.php" ?>
      <script src="<?=INCLUDE_FRONT_JS?>/landing-page/landing-page-events.js"></script>
      <script src="<?=INCLUDE_FRONT_JS?>/landing-page/landing-page-animation.js"></script>
      <?php require_once INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
