<?php require_once INCLUDE_FRONT_VIEWS.'/global/head.php' ?>
 <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/coworking/coworking.css"/>
</head>
<body>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/header.php" ?>
  <div id="top-background-container"></div>
    <div id="main-container" class="flex-center-col">

      <div class="flex-center-row coworking-card-container">
        <div class="coworking-card-element flex-center-col coworking-card-text-container">
          <div class="coworking-text flex-center-col">
            <h1 class="coworking-title">Pourquoi le coworking ?</h1>
            <p class="coworking-description flex-center-col">
              Travailler de chez soi a t-il vraiment que des avantages ? <br><br>

              Outre le fait de pouvoir organiser son espace de travail et ses horaires, cette pseudo liberté semble souvent idyllique. Que se passe t-il en réalité ? <br><br>
            </p>
            <p class="coworking-description flex-center-col">
              Bon nombre de freelance ou personnes en télé-travail partagent pourtant ces ressentis: des journées saccadées, un manque de conviction, un isolement pesant, un mix vie pro/vie privée,  une absence d’énergie extérieure, des rythmes décalés, un lieu de travail inadapté…<br><br>
            </p>
            <p class="coworking-description flex-center-col">
              Le coworking c’est travailler ensemble dans un espace convivial et propice à l’échange tout en partageant son quotidien et ses expériences avec des personnes expérimentant les mêmes problématiques. Chaque coworker est un membre essentiel au bon fonctionnement de la communauté, chaque membre est un rouage au processus mis en place sur ces espaces pour booster son activité, son réseau et ainsi contribuer à sa réussite professionnelle.
            </p>
        </div>
      </div>
      </div>
    </div>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/scripts.php" ?>
  <script src="<?=INCLUDE_FRONT_JS?>/coworking/coworking-events.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/coworking/coworking-animation.js"></script>
<?php require_once INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
