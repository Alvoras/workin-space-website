<?php require_once INCLUDE_FRONT_VIEWS.'/global/head.php' ?>
 <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/sites/sites.css"/>
</head>
<body>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/header.php" ?>
  <div id="more-tile-container" class="flex-center-col">
    <div id="more-tile" class="flex-center-col">
      <div id="three-viewer">

      </div>
      <div id="more-tile-bottom-container" class="flex-center-row">
        <div id="more-tile-img-container">

        </div>
        <div id="more-tile-description">

        </div>
      </div>
    </div>
  </div>
    <div id="main-container">
      <div class="container tiles-container align-items-center">
        <div id="site-header-container" class="flex-center-row">
          <?php
          foreach ($_SESSION["sites_data"] as $key => $val) {
            ?>
            <span class="header-site-name clickable <?php if($key == 0) echo "site-name-selected"?>" data-siteId="<?=$val["id_site"]?>"><?=$val["site_name"]?></span>
            <?php
          }
           ?>
        </div>

        <div id="tile-container" class="row justify-content-center align-items-start">
          <?php
              foreach ($_SESSION["site_content"] as $key => $val) {
                include 'public/back/views/include/global/item-grid/grid-card.php';  
              }
           ?>
        </div>
      </div>

    </div>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/scripts.php" ?>
  <script src="<?=INCLUDE_FRONT_JS?>/global/lib/three/three.min.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/global/lib/three/OrbitControls.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/sites/three-viewer.js"></script>
  <script src="<?=INCLUDE_FRONT_JS?>/sites/sites-events.js"></script>
  <?php require_once INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
