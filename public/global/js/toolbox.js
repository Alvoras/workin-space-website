function ajaxExec(p) {
  if (typeof p.async === 'undefined') p.async = false;
  if (typeof p.data == 'undefined') p.data = null;

  let ret = {
    success: false,
    errorMsg: 'Something went wrong'
  };

  let params = {
    url: p.script,
    type: "POST",
    async: p.async,
    data: p.data
  };

  $.ajax(params).done(function(msg) {
    console.log(msg);
    ret = JSON.parse(msg);

    if (typeof ret.success !== 'undefined') {
      if (ret.success) {
        if (typeof p.success !== 'undefined'){
          p.success(ret);
        }
      }else if (!ret.success) {
        if (typeof p.failure !== 'undefined'){
          p.failure(ret);
        }
      }
    }
  }).fail(function(jqXHR, statusText) {
    // console.log(jqXHR, statusText);
    ret.errorMsg = `Query failed (${statusText})`;
    // if (typeof p.fail !== 'undefined'){
    //   p.failure(ret);
    // }
  });

  if (!p.async) {
    return ret;
  }
}

function notify(res) {
  if (res.success) {
    if (typeof res.successMsg !== 'undefined') {
      $.growl.success({
        message: res.successMsg
      });
    }
  }else {
    if (typeof res.errorMsg !== 'undefined') {
      $.growl.error({
        message: res.errorMsg
      });
    }
  }
}

function wrapPromise(toDo) {
  return new Promise ((successCb, failureCb) => {
    // console.log("Calling passed function...");
    if (toDo()) {
      // console.log("Promise success");
      successCb();
    }else {
      // console.log("Promise failure");
      failureCb();
    }
  });
}

function ajaxFetchPHP(p) {
  if (typeof p.async === 'undefined') p.acync = false;

  let ret = null;

  let params = {
    url: p.script,
    type: "POST",
    async: p.async,
    data: p.data
  };

  $.ajax(params).done(function(msg) {
    ret = msg;
    if (typeof p.callback !== 'undefined'){
      p.callback(msg);
    }
  }).fail(function(jqXHR, statusText) {
    // console.log(jqXHR, statusText);
    if (typeof p.failure !== 'undefined'){
      p.failure(msg);
    }
  });

  if (!p.async) {
    return ret;
  }
}

function ajaxRefreshElement(script, parent, data) {
  // let failMsg = "Il y a eu un problème lors de la récupération des informations";

  if (typeof document._data === 'undefined') {
    document._data = {};
  }
  document._data.tmpRefreshElement = parent;

  let successCb = (processed) => {
    if (processed === null) return;

    let parent = document._data.tmpRefreshElement;
    $(parent).html(processed);

    // Safety after processing
    document._data.tmpRefreshElement = null;
  };

  let failureCb = (res) => {
    notify(res);
  };

  let params = {
    script,
    data,
    async: false,
    callback: successCb,
    failure: failureCb
  }

  let processed = ajaxFetchPHP(params);

  return (processed != null)?true:false;
}

function updateUserRentals(siteId) {
    let data = {
      siteId
    };

    let script = "private/scripts/db/fetch_user_rentals.php";

    let failureCb = (res) => {
      if (!ret.success) {
        $.growl.error({message: res.errMsg});
        return;
      }
    };

    let params = {
      data,
      script,
      async: false,
      failure: failureCb
    }

    // Update the info contained within the user object in $_SESSION
    let ret = ajaxExec(params);

    return ret;
}

function fetchInputVals(el) {
  let data = {};

  el.each(function(ndx, input) {
    data[$(input).attr('name')] = $(input).val();
  });

  return data;
}

function validateInput(candidate, mode) {
  switch (mode) {
    case "email":
      return validateEmail(candidate);
      break;

    case "phone":
      return validatePhone(candidate);
      break;

    case "firstname":
    case "lastname":
      return validateLength(2, 20, candidate);
      break;

    case "password":
      return validatePassword(candidate);
  }
}

function validateEmail(candidate) {
  // From w3 resources
  let rgx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  return rgx.test(candidate);
}

function validatePhone(candidate) {
  // From w3 resources
  let rgx = /^\d{10}$/;

  return rgx.test(candidate);
}

function validateLength(minLength, maxLength, candidate) {
  let ret = false;

  if (candidate.length >= minLength && candidate.length <= maxLength) {
    ret = true;
  }

  return ret;
}

function validatePassword(candidate) {
  // 8 to 64 characters, at least one uppercase letter, one lowercase letter, one number and one special char (@$!%*?&#)
  // PRODUCTION
  let rgx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#])[A-Za-z\d$@$!%*?&#]{8,64}/;

  // DEBUG
  // let rgx = /^.*$/;

  return rgx.test(candidate);
}

function displayErrors(){
  let errList = document._errors;

  if (typeof errList != "undefined" && errList.length > 0) {
    for (err of errList) {
      $.growl.error({message: err});
    }
  }
}

function addOneHour(strHour) {
  // If the passed hour still has a "h" in it, remove it
  // Not needed for now but could prevent future bugs
  if (typeof strHour === 'undefined') {
    return;
  }

  strHour.replace("h", "");
  updatedHour = parseInt(strHour) + 1;

  return updatedHour.toString();
}

function subOneHour(strHour) {
  // If the passed hour still has a "h" in it, remove it
  // Not needed for now but could prevent future bugs
  if (typeof strHour === 'undefined') {
    return;
  }
  strHour.replace("h", "");
  updatedHour = parseInt(strHour) - 1;

  return updatedHour.toString();
}

function updateDatepicker() {
  $("#planning-date-change").datepicker(
  {
    onSelect: updatePlanningWithDate
  });
}
