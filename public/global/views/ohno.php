<?php require_once INCLUDE_FRONT_VIEWS.'/global/head.php' ?>
 <link rel="stylesheet" href="<?=INCLUDE_FRONT_CSS?>/ohno/ohno.css"/>
</head>
<body>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/header.php" ?>
    <div id="main-container">

      <?php include INCLUDE_FRONT_VIEWS."/global/orbits.php" ?>
      <center><h1 id="ohno-title">Oh non ! Impossible d'afficher la page.</h1></center>

    </div>
  <?php require_once INCLUDE_FRONT_VIEWS."/global/scripts.php" ?>
<?php require_once INCLUDE_GLOBAL_VIEWS."/footer.php" ?>
