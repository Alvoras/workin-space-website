<?php
  foreach ($_SESSION["sub-cards"] as $key => $val) {
    $glow = '';
    $glowingCard = '';

    if (isset($_SESSION["title"]) && $_SESSION["title"] == "Abonnement") {
      $glow = "back-glow-yellow";
      $glowingCard = "glowing-card";
    }

    $commitmentDuration = ($val["commitment_duration"] != null)?$val["commitment_duration"]:"";

    $subCaption = ($val["monthly_w_commitment"]!= null)?
    '<h6 class="plan-card-text">Engagement '.$commitmentDuration.' mois : '.$val["monthly_w_commitment"].'€/mois</h6>':
    '<h6 class="plan-card-text">Étudiants : '.$val["student_rebate"].'€/jour</h6>';

    $caption = ($val["monthly_no_commitment"]!= null)?
    '<h3 class="plan-card-text">'.$val["monthly_no_commitment"].'€/mois</h3>':
    '<h3 class="plan-card-text">'.$val["day_price"].'€/jour</h3>';

    $unlimitedAccess = ($val["unlimited_access"] == true)?'<li>Accès illimité':"";
    if ($val["unlimited_access"] == true) {
      $rocketUnlimited = '<div class="flex-center-col">
                              <i id="unlimited-rocket" class="icon-rocket"></i>
                              <h2 class="plan-card-text">AUCUNE LIMITE<h2>
                          </div>';

      $openSpace = '';
      $unlimitedWifi  = '';
      $unlimitedDrinks = '';
    }else{
      $rocketUnlimited = '';

      if ($val["monthly_no_commitment"]) {
        $openSpace = '';
        $unlimitedWifi = '';
        $unlimitedDrinks = '';
      }else{
        $openSpace = '<li><i class="icon-desktop"></i>Accès à l\'open space';
        $unlimitedWifi = '<li><i class="icon-network_check"></i>Wifi illimité';
        $unlimitedDrinks = '<li><i class="icon-coffee"></i>Snacking et boissons à volonté';
      }
    }

    $hubAccess = ($val["hub_access"] == true)?'<li><i class="icon-device_hub"></i>Accès premium au HUB':"";

    $canRoam = ($val["can_roam"] == true)?'<li><i class="icon-location"></i>Valable dans tous nos sites':"";

    $accessAllSpaces = ($val["access_all_spaces"] == true)?'<li><i class="icon-infinity"></i>Accès libre à tous les espaces':"";

    $firstHour = '<li>'.$val["first_hour"].'€ la première heure';
    $halfHour = '<li>'.$val["half_hour"].'€ par demi-heure';

      echo '<div class="plan-card '.$glow.' '.$glowingCard.' flex-center-col" data-id="'.($key+1).'">
              <div class="plan-card-title">
                <h2 class="plan-card-text">'.$val["subscription_name"].'</h2>
              </div>
              <div class="plan-card-img flex-center-col">
                <img src="'.$val["image"].'" alt="basic-plan-shuttle">
              </div>
              <div class="plan-card-caption flex-center-col">
              '.$caption.'
              '.$subCaption.'
              </div>
                ';
                if (!$val["unlimited_access"]){
                  echo '
                    <ul class="hourly-rate flex-center-col" class="plan-card-text">
                      '.$firstHour.'
                      '.$halfHour.'
                    </ul>';
                }
                echo '
                <div class="bonus-list-container">
                  <div class="bonus-list flex-center-col">
                    <ul class="plan-card-text">
                      '.$unlimitedWifi.'
                      '.$unlimitedDrinks.'
                      '.$openSpace.'
                      '.$accessAllSpaces.'
                      '.$canRoam.'
                      '.$hubAccess.'
                      '.$rocketUnlimited.'
                    </ul>
                </div>
              </div>
          </div>';
  }
?>
