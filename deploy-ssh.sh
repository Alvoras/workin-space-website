#!/bin/bash

read -p "Entrer l'adresse du serveur :" srvAddr
read -p "Entrer l'utilisateur :" usr

mkdir tmp

ssh-keygen -t rsa -f ./tmp/id_rsa && ssh-copy-id -i ./tmp/id_rsa.pub $usr@$srvAddr && echo '' || read -n 1 -p "Retry using -f ?" force

if [[ $force =~ ^([yY])$ ]]; then
  ssh-copy-id -i ./tmp/id_rsa.pub -f $usr@$srvAddr
  if [[ $? -eq 0 ]]; then
    echo "Updating permissions..."
    chmod 600 ./tmp/id_rsa
  else
    echo "Something went wrong"
  fi
fi
