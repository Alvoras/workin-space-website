-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 15 Mai 2018 à 23:46
-- Version du serveur :  5.5.57-0+deb8u1
-- Version de PHP :  7.0.22-2+0~20170804100528.5+jessie~1.gbpdea206

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `workin-space`
--

-- --------------------------------------------------------

--
-- Structure de la table `ASSETS`
--

CREATE TABLE IF NOT EXISTS `ASSETS` (
`id_asset` int(11) NOT NULL,
  `id_site` int(11) DEFAULT NULL,
  `hardware_name` varchar(25) DEFAULT NULL,
  `room_name` varchar(25) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `ASSETS`
--

INSERT INTO `ASSETS` (`id_asset`, `id_site`, `hardware_name`, `room_name`, `is_deleted`) VALUES
(1, 1, NULL, 'Salle de réunion', 0),
(2, 1, NULL, 'Salle de réunion', 0),
(3, 1, NULL, 'Salle d''appel', 0),
(4, 1, NULL, 'Salle d''appel', 0),
(5, 1, NULL, 'Salle d''appel', 0),
(6, 1, 'Imprimante', NULL, 0),
(7, 2, NULL, 'Salle de réunion', 0),
(8, 2, NULL, 'Salle de réunion', 0),
(9, 2, NULL, 'Salle de réunion', 0),
(10, 2, NULL, 'Salle de réunion', 0),
(11, 2, NULL, 'Salle de réunion', 0),
(12, 2, NULL, 'Salle de réunion', 0),
(13, 2, NULL, 'Salle de réunion', 0),
(14, 2, NULL, 'Salle d''appel', 0),
(15, 2, NULL, 'Salle d''appel', 0),
(16, 2, NULL, 'Salle d''appel', 0),
(17, 2, NULL, 'Salle d''appel', 0),
(18, 2, NULL, 'Salle d''appel', 0),
(19, 2, 'Ordinateur', NULL, 0),
(20, 2, 'Ordinateur', NULL, 0),
(21, 2, 'Ordinateur', NULL, 0),
(22, 2, 'Ordinateur', NULL, 0),
(23, 2, 'Ordinateur', NULL, 0),
(24, 2, 'Ordinateur', NULL, 0),
(25, 2, 'Ordinateur', NULL, 0),
(26, 2, 'Ordinateur', NULL, 0),
(27, 2, 'Ordinateur', NULL, 0),
(28, 2, 'Ordinateur', NULL, 0),
(29, 2, 'Ordinateur', NULL, 0),
(30, 2, 'Ordinateur', NULL, 0),
(31, 2, 'Ordinateur', NULL, 0),
(32, 2, 'Ordinateur', NULL, 0),
(33, 2, 'Ordinateur', NULL, 0),
(34, 2, 'Ordinateur', NULL, 0),
(35, 2, 'Ordinateur', NULL, 0),
(36, 2, 'Ordinateur', NULL, 0),
(37, 2, 'Ordinateur', NULL, 0),
(38, 2, 'Ordinateur', NULL, 0),
(39, 2, 'Ordinateur', NULL, 0),
(40, 2, 'Ordinateur', NULL, 0),
(41, 2, 'Ordinateur', NULL, 0),
(42, 2, 'Ordinateur', NULL, 0),
(43, 2, 'Ordinateur', NULL, 0),
(44, 2, 'Ordinateur', NULL, 0),
(45, 2, 'Imprimante', NULL, 0),
(46, 2, 'Imprimante', NULL, 0),
(47, 2, 'Imprimante', NULL, 0),
(48, 3, NULL, 'Salle de réunion', 0),
(49, 3, NULL, 'Salle de réunion', 0),
(50, 3, NULL, 'Salle de réunion', 0),
(51, 3, NULL, 'Salle de réunion', 0),
(52, 3, NULL, 'Salle d''appel', 0),
(53, 3, NULL, 'Salle d''appel', 0),
(54, 3, 'Ordinateur', NULL, 0),
(55, 3, 'Ordinateur', NULL, 0),
(56, 3, 'Ordinateur', NULL, 0),
(57, 3, 'Ordinateur', NULL, 0),
(58, 3, 'Ordinateur', NULL, 0),
(59, 3, 'Ordinateur', NULL, 0),
(60, 3, 'Ordinateur', NULL, 0),
(61, 3, 'Ordinateur', NULL, 0),
(62, 3, 'Ordinateur', NULL, 0),
(63, 3, 'Ordinateur', NULL, 0),
(64, 3, 'Ordinateur', NULL, 0),
(65, 3, 'Ordinateur', NULL, 0),
(66, 3, 'Ordinateur', NULL, 0),
(67, 3, 'Ordinateur', NULL, 0),
(68, 3, 'Ordinateur', NULL, 0),
(69, 3, 'Ordinateur', NULL, 0),
(70, 3, 'Ordinateur', NULL, 0),
(71, 3, 'Ordinateur', NULL, 0),
(72, 3, 'Ordinateur', NULL, 0),
(73, 3, 'Imprimante', NULL, 0),
(74, 3, 'Imprimante', NULL, 0),
(75, 4, NULL, 'Salle de réunion', 0),
(76, 4, NULL, 'Salle de réunion', 0),
(77, 4, NULL, 'Salle de réunion', 0),
(78, 4, NULL, 'Salle de réunion', 0),
(79, 4, NULL, 'Salle de réunion', 0),
(80, 4, NULL, 'Salle d''appel', 0),
(81, 4, NULL, 'Salle d''appel', 0),
(82, 4, NULL, 'Salle d''appel', 0),
(83, 4, NULL, 'Salle d''appel', 0),
(84, 4, 'Ordinateur', NULL, 0),
(85, 4, 'Ordinateur', NULL, 0),
(86, 4, 'Ordinateur', NULL, 0),
(87, 4, 'Ordinateur', NULL, 0),
(88, 4, 'Ordinateur', NULL, 0),
(89, 4, 'Ordinateur', NULL, 0),
(90, 4, 'Ordinateur', NULL, 0),
(91, 4, 'Ordinateur', NULL, 0),
(92, 4, 'Ordinateur', NULL, 0),
(93, 4, 'Ordinateur', NULL, 0),
(94, 4, 'Ordinateur', NULL, 0),
(95, 4, 'Ordinateur', NULL, 0),
(96, 4, 'Ordinateur', NULL, 0),
(97, 4, 'Ordinateur', NULL, 0),
(98, 4, 'Ordinateur', NULL, 0),
(99, 4, 'Ordinateur', NULL, 0),
(100, 4, 'Ordinateur', NULL, 0),
(101, 4, 'Ordinateur', NULL, 0),
(102, 4, 'Ordinateur', NULL, 0),
(103, 4, 'Ordinateur', NULL, 0),
(104, 4, 'Ordinateur', NULL, 0),
(105, 4, 'Imprimante', NULL, 0),
(106, 5, NULL, 'Salle de réunion', 0),
(107, 5, NULL, 'Salle de réunion', 0),
(108, 5, NULL, 'Salle de réunion', 0),
(109, 5, NULL, 'Salle de réunion', 0),
(110, 5, NULL, 'Salle de réunion', 0),
(111, 5, NULL, 'Salle de réunion', 0),
(112, 5, NULL, 'Salle de réunion', 0),
(113, 5, NULL, 'Salle d''appel', 0),
(114, 5, NULL, 'Salle d''appel', 0),
(115, 5, NULL, 'Salle d''appel', 0),
(116, 5, NULL, 'Salle d''appel', 0),
(117, 5, NULL, 'Salle d''appel', 0),
(118, 5, 'Ordinateur', NULL, 0),
(119, 5, 'Ordinateur', NULL, 0),
(120, 5, 'Ordinateur', NULL, 0),
(121, 5, 'Ordinateur', NULL, 0),
(122, 5, 'Ordinateur', NULL, 0),
(123, 5, 'Ordinateur', NULL, 0),
(124, 5, 'Ordinateur', NULL, 0),
(125, 5, 'Ordinateur', NULL, 0),
(126, 5, 'Ordinateur', NULL, 0),
(127, 5, 'Ordinateur', NULL, 0),
(128, 5, 'Ordinateur', NULL, 0),
(129, 5, 'Ordinateur', NULL, 0),
(130, 5, 'Ordinateur', NULL, 0),
(131, 5, 'Ordinateur', NULL, 0),
(132, 5, 'Ordinateur', NULL, 0),
(133, 5, 'Ordinateur', NULL, 0),
(134, 5, 'Ordinateur', NULL, 0),
(135, 5, 'Ordinateur', NULL, 0),
(136, 5, 'Ordinateur', NULL, 0),
(137, 5, 'Ordinateur', NULL, 0),
(138, 5, 'Ordinateur', NULL, 0),
(139, 5, 'Imprimante', NULL, 0),
(140, 5, 'Imprimante', NULL, 0),
(141, 5, 'Imprimante', NULL, 0),
(142, 6, NULL, 'Salle de réunion', 0),
(143, 6, NULL, 'Salle de réunion', 0),
(144, 6, NULL, 'Salle d''appel', 0),
(145, 6, NULL, 'Salle d''appel', 0),
(146, 6, NULL, 'Salle d''appel', 0),
(147, 6, 'Ordinateur', NULL, 0),
(148, 6, 'Ordinateur', NULL, 0),
(149, 6, 'Ordinateur', NULL, 0),
(150, 6, 'Ordinateur', NULL, 0),
(151, 6, 'Ordinateur', NULL, 0),
(152, 6, 'Ordinateur', NULL, 0),
(153, 6, 'Ordinateur', NULL, 0),
(154, 6, 'Ordinateur', NULL, 0),
(155, 6, 'Ordinateur', NULL, 0),
(156, 6, 'Ordinateur', NULL, 0),
(157, 6, 'Ordinateur', NULL, 0),
(158, 6, 'Ordinateur', NULL, 0),
(159, 6, 'Ordinateur', NULL, 0),
(160, 6, 'Ordinateur', NULL, 0),
(161, 6, 'Ordinateur', NULL, 0),
(162, 6, 'Ordinateur', NULL, 0),
(163, 6, 'Ordinateur', NULL, 0),
(164, 6, 'Ordinateur', NULL, 0),
(165, 6, 'Ordinateur', NULL, 0),
(166, 6, 'Ordinateur', NULL, 0),
(167, 6, 'Ordinateur', NULL, 0),
(168, 6, 'Imprimante', NULL, 0),
(219, 1, NULL, 'Salle de réunion', 0);

-- --------------------------------------------------------

--
-- Structure de la table `AUTH_TOKENS`
--

CREATE TABLE IF NOT EXISTS `AUTH_TOKENS` (
  `selector` char(13) DEFAULT NULL,
  `hashed_validator` char(64) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `expires` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `AUTH_TOKENS`
--

INSERT INTO `AUTH_TOKENS` (`selector`, `hashed_validator`, `email`, `expires`) VALUES
(NULL, NULL, 'azeazeaze@storiqax.com', NULL),
(NULL, NULL, 'azert@storiqax.com', NULL),
('5af701244a100', '2af8838525fb7af2658eda1dd7434d354678134519cbee59c6a850ecc6031366', 'hadrien.bouffier@gmail.com', '2018-05-19 12:58:44');

-- --------------------------------------------------------

--
-- Structure de la table `HARDWARE`
--

CREATE TABLE IF NOT EXISTS `HARDWARE` (
  `name` varchar(25) NOT NULL,
  `price` float NOT NULL,
  `img` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `3D_texture` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `HARDWARE`
--

INSERT INTO `HARDWARE` (`name`, `price`, `img`, `description`, `3D_texture`) VALUES
('Imprimante', 10, 'public/global/img/cards/hardware/printer.png', 'Superbe imprimante nouvelle génération', ''),
('Ordinateur', 10, 'public/global/img/cards/hardware/laptop.png', 'Ordinateur portable performant', '');

-- --------------------------------------------------------

--
-- Structure de la table `rent`
--

CREATE TABLE IF NOT EXISTS `rent` (
`id_rent` int(11) NOT NULL,
  `rent_start` datetime DEFAULT NULL,
  `rent_stop` datetime DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `id_asset` int(11) NOT NULL,
  `rent_token` char(32) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `rent`
--

INSERT INTO `rent` (`id_rent`, `rent_start`, `rent_stop`, `email`, `id_asset`, `rent_token`, `is_deleted`) VALUES
(9, '2018-04-24 10:00:00', '2018-04-24 15:00:00', 'hadrien.bouffier@gmail.com', 4, '', 0),
(10, '2018-04-26 13:00:00', '2018-04-26 16:00:00', 'hadrien.bouffier@gmail.com', 4, '', 0),
(11, '2018-04-26 17:00:00', '2018-04-26 18:00:00', 'hadrien.bouffier@gmail.com', 2, '', 0),
(12, '2018-04-24 11:00:00', '2018-04-24 16:00:00', 'hadrien.bouffier@gmail.com', 48, '', 0),
(13, '2018-05-02 12:00:00', '2018-05-02 13:00:00', 'hadrien.bouffier@gmail.com', 48, '', 0),
(14, '2018-04-25 13:00:00', '2018-04-25 14:00:00', 'hadrien.bouffier@gmail.com', 19, '', 0),
(15, '2018-05-02 12:00:00', '2018-05-02 16:00:00', 'hadrien.bouffier@gmail.com', 4, '', 0),
(17, '2018-05-01 13:00:00', '2018-05-01 18:00:00', 'hadrien.bouffier@gmail.com', 2, '', 0),
(18, '2018-05-01 11:00:00', '2018-05-01 13:00:00', 'hadrien.bouffier@gmail.com', 7, '', 0),
(19, '2018-05-01 12:00:00', '2018-05-01 16:00:00', 'hadrien.bouffier@gmail.com', 4, '', 0),
(20, '2018-04-30 09:00:00', '2018-04-30 20:00:00', 'hadrien.bouffier@gmail.com', 4, '', 0),
(21, '2018-04-30 14:00:00', '2018-04-30 17:00:00', 'hadrien.bouffier@gmail.com', 2, '', 0),
(22, '2018-04-26 17:00:00', '2018-04-26 18:00:00', 'hadrien.bouffier@gmail.com', 2, '', 0),
(23, '2018-05-02 11:00:00', '2018-05-02 16:00:00', 'hadrien.bouffier@gmail.com', 6, '', 0),
(25, '2018-05-03 12:00:00', '2018-05-03 14:00:00', 'abc@a.com', 6, '', 0),
(26, '2018-05-04 13:00:00', '2018-05-04 16:00:00', 'abc@a.com', 19, '', 0),
(27, '2018-05-01 11:00:00', '2018-05-01 12:00:00', 'hadrien.bouffier@gmail.com', 1, '', 0),
(30, '2018-05-07 16:00:00', '2018-05-07 17:00:00', 'hadrien.bouffier@gmail.com', 3, '', 0),
(35, '2018-05-07 12:00:00', '2018-05-07 18:00:00', 'hadrien.bouffier@gmail.com', 6, '', 0),
(36, '2018-05-08 14:00:00', '2018-05-08 15:00:00', 'hadrien.bouffier@gmail.com', 6, '', 0),
(37, '2018-05-08 18:00:00', '2018-05-08 19:00:00', 'hadrien.bouffier@gmail.com', 6, '', 0),
(38, '2018-05-08 09:00:00', '2018-05-08 10:00:00', 'hadrien.bouffier@gmail.com', 6, '', 0),
(40, '2018-05-08 11:00:00', '2018-05-08 12:00:00', 'hadrien.bouffier@gmail.com', 3, '4f61bfdb70032d93d6f8b3920aebfd88', 1),
(41, '2018-05-08 10:00:00', '2018-05-08 11:00:00', 'hadrien.bouffier@gmail.com', 3, 'a1c96b790876557a821e3cb695f10135', 0),
(42, '2018-05-08 13:00:00', '2018-05-08 14:00:00', 'hadrien.bouffier@gmail.com', 1, '2aa95cc1afc9c60d5539aeb5e10de6b7', 1),
(43, '2018-05-08 10:00:00', '2018-05-08 11:00:00', 'hadrien.bouffier@gmail.com', 1, 'c535ae9c603b059cf97ff06f5cc37363', 0),
(44, '2018-05-08 12:00:00', '2018-05-08 13:00:00', 'hadrien.bouffier@gmail.com', 1, '14d65ff4360688f324d3be59d3f0f649', 1),
(45, '2018-05-09 11:00:00', '2018-05-09 14:00:00', 'hadrien.bouffier@gmail.com', 3, '99922b2ca4514039e8a229a979871e91', 1),
(46, '2018-05-10 10:00:00', '2018-05-10 14:00:00', 'hadrien.bouffier@gmail.com', 3, '5ab0c415c6b5f45bd3da51a30834cc8a', 1),
(47, '2018-05-09 10:00:00', '2018-05-09 13:00:00', 'hadrien.bouffier@gmail.com', 1, 'df96e877dcc7b3ffc454950b8ad9c4f5', 0),
(48, '2018-05-10 12:00:00', '2018-05-10 16:00:00', 'hadrien.bouffier@gmail.com', 1, 'a65973b81d8c1fd4a87a8ca516a367d8', 0),
(49, '2018-05-10 10:00:00', '2018-05-10 11:00:00', 'hadrien.bouffier@gmail.com', 1, '6f5052fea5193f8bd7122aae78ee0c44', 1),
(50, '2018-05-10 10:00:00', '2018-05-10 12:00:00', 'hadrien.bouffier@gmail.com', 2, 'cb8397b0ced0a46f6a2204c8201e7881', 1),
(51, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 1, '17b765ffab39e796c396ce5e3607d941', 1),
(52, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 2, 'eb82770f99b79c0faba00310c0993494', 1),
(53, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 1, '10733a0de5a9801a530a44d357ebf7fc', 1),
(54, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 1, '3da87ce72b258463143034e98307d5c6', 1),
(55, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 1, '51cf05d18ef0f60d5f22cca3afc84a2d', 1),
(56, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 3, 'ccc7b71fca1177ea89a3b105aa2fbffc', 1),
(57, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 3, 'f761094d3eb45454f1cf51706f381b72', 1),
(58, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 3, '99bf1b057fc609acdf583f6015d6617a', 1),
(59, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 3, '9c03f9818b920dceb799414324c49a52', 1),
(60, '2018-05-10 09:00:00', '2018-05-10 12:00:00', 'hadrien.bouffier@gmail.com', 3, '120e46734d26a0779174927b18540b73', 1),
(61, '2018-05-10 10:00:00', '2018-05-10 11:00:00', 'hadrien.bouffier@gmail.com', 3, 'a836087fb55312f2036ba5c900ace28d', 1),
(62, '2018-05-10 09:00:00', '2018-05-10 10:00:00', 'hadrien.bouffier@gmail.com', 6, 'd6eea4fca6bf692646492c439e4dd744', 1),
(63, '2018-05-10 12:00:00', '2018-05-10 15:00:00', 'hadrien.bouffier@gmail.com', 19, '6d9ea3d51b5daf122af0c69e5b7dbfca', 0),
(64, '2018-05-11 13:00:00', '2018-05-11 14:00:00', 'hadrien.bouffier@gmail.com', 19, 'b0e22b6b5fb482c3a9a7c73b2afd6123', 0),
(65, '2018-05-12 17:00:00', '2018-05-12 18:00:00', 'hadrien.bouffier@gmail.com', 19, '26a7b801f7812dc37aece165cf1dd0a5', 0),
(66, '2018-05-11 10:00:00', '2018-05-11 13:00:00', 'hadrien.bouffier@gmail.com', 19, 'ab8fa9ef839ccb0d06f262bea3d51e90', 0),
(67, '2018-05-10 11:00:00', '2018-05-10 13:00:00', 'topelvideo@gmail.com', 2, 'afa371a351b8b5fa2027606ee5443b40', 1),
(68, '2018-05-10 15:00:00', '2018-05-10 18:00:00', 'topelvideo@gmail.com', 2, '4aa471c38f911de6da860ffd09b6dfaf', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ROOM`
--

CREATE TABLE IF NOT EXISTS `ROOM` (
  `name` varchar(25) NOT NULL,
  `seats` int(11) DEFAULT NULL,
  `price` float NOT NULL,
  `description` text NOT NULL,
  `img` varchar(150) NOT NULL,
  `3D_texture` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `ROOM`
--

INSERT INTO `ROOM` (`name`, `seats`, `price`, `description`, `img`, `3D_texture`) VALUES
('Salle d''appel', 15, 200, 'Nos salles de visioconférence sont parfaitement étanches au son. Vous pourrez ainsi travailler dans un calme absolu tout en profitant d''une discrétion sans égale.', 'public/global/img/cards/rooms/video_room.jpeg', 'public/front/img/pano/video_room_3d.jpg'),
('Salle de réunion', 10, 150, 'Nos salles de réunions sont ce qu''il vous faut lorsque vous avez besoin d''un espace privé dans lequel vous réunir avec votre équipe', 'public/global/img/cards/rooms/meeting_room.jpeg', 'public/front/img/pano/meeting_room_3d.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `SCHEDULE`
--

CREATE TABLE IF NOT EXISTS `SCHEDULE` (
  `id_schedule` int(11) NOT NULL,
  `day` varchar(10) DEFAULT NULL,
  `is_open` tinyint(1) DEFAULT NULL,
  `site_open` float DEFAULT NULL,
  `site_close` float DEFAULT NULL,
  `id_site` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `SCHEDULE`
--

INSERT INTO `SCHEDULE` (`id_schedule`, `day`, `is_open`, `site_open`, `site_close`, `id_site`) VALUES
(1, 'Lundi', 1, 9, 20, 1),
(2, 'Mardi', 1, 9, 20, 1),
(3, 'Mercredi', 1, 9, 20, 1),
(4, 'Jeudi', 1, 9, 20, 1),
(5, 'Vendredi', 1, 9, 20, 1),
(6, 'Samedi', 1, 11, 20, 1),
(7, 'Dimanche', 1, 11, 20, 1),
(8, 'Lundi', 1, 8, 21, 2),
(9, 'Mardi', 1, 8, 21, 2),
(10, 'Mercredi', 1, 8, 21, 2),
(11, 'Jeudi', 1, 8, 21, 2),
(12, 'Vendredi', 1, 9, 23, 2),
(13, 'Samedi', 1, 9, 20, 2),
(14, 'Dimanche', 1, 9, 20, 2),
(15, 'Lundi', 1, 9, 20, 3),
(16, 'Mardi', 1, 9, 20, 3),
(17, 'Mercredi', 1, 9, 20, 3),
(18, 'Jeudi', 1, 9, 20, 3),
(19, 'Vendredi', 1, 9, 20, 3),
(20, 'Samedi', 1, 11, 20, 3),
(21, 'Dimanche', 1, 11, 20, 3),
(22, 'Lundi', 1, 9, 20, 4),
(23, 'Mardi', 1, 9, 20, 4),
(24, 'Mercredi', 1, 9, 20, 4),
(25, 'Jeudi', 1, 9, 20, 4),
(26, 'Vendredi', 1, 9, 20, 4),
(27, 'Samedi', 1, 11, 20, 4),
(28, 'Dimanche', 1, 9, 20, 4),
(29, 'Lundi', 1, 8, 21, 5),
(30, 'Mardi', 1, 8, 21, 5),
(31, 'Mercredi', 1, 8, 21, 5),
(32, 'Jeudi', 1, 8, 21, 5),
(33, 'Vendredi', 1, 9, 23, 5),
(34, 'Samedi', 1, 9, 20, 5),
(35, 'Dimanche', 1, 9, 20, 5),
(36, 'Lundi', 1, 9, 20, 6),
(37, 'Mardi', 1, 9, 20, 6),
(38, 'Mercredi', 1, 9, 20, 6),
(39, 'Jeudi', 1, 9, 20, 6),
(40, 'Vendredi', 1, 9, 20, 6),
(41, 'Samedi', 1, 11, 20, 6),
(42, 'Dimanche', 1, 9, 20, 6);

-- --------------------------------------------------------

--
-- Structure de la table `SITE`
--

CREATE TABLE IF NOT EXISTS `SITE` (
`id_site` int(11) NOT NULL,
  `site_name` varchar(64) DEFAULT NULL,
  `site_address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `SITE`
--

INSERT INTO `SITE` (`id_site`, `site_name`, `site_address`) VALUES
(1, 'Republique', '4 Place de la Bastille, 75012 Paris'),
(2, 'Odéon', '18 Place de la République, 75010 Paris'),
(3, 'Place d''Italie', '7 Rue Corneille, 75006 Paris'),
(4, 'Ternes', '25 Boulevard Auguste Blanqui, 75013 Paris'),
(5, 'Beaubourg', '8 Avenue des Ternes, 75017 Paris'),
(6, 'Bastille', '26 Rue de Venise, 75004 Paris');

-- --------------------------------------------------------

--
-- Structure de la table `STAFF`
--

CREATE TABLE IF NOT EXISTS `STAFF` (
`id_staff` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `id_site` int(11) DEFAULT NULL,
  `id_staff_type` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `STAFF`
--

INSERT INTO `STAFF` (`id_staff`, `email`, `id_site`, `id_staff_type`) VALUES
(2, 'pierre@gmail.com', 2, 1),
(3, 'michel@gmail.com', 1, 2),
(4, 'jacques@gmail.com', 2, 2),
(5, 'hadrien.bouffier@gmail.com', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `STAFF_TYPES`
--

CREATE TABLE IF NOT EXISTS `STAFF_TYPES` (
  `id_staff_type` int(11) NOT NULL,
  `name_staff_type` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `STAFF_TYPES`
--

INSERT INTO `STAFF_TYPES` (`id_staff_type`, `name_staff_type`) VALUES
(1, 'Accueil'),
(2, 'Manager'),
(3, 'Support');

-- --------------------------------------------------------

--
-- Structure de la table `subscribe`
--

CREATE TABLE IF NOT EXISTS `subscribe` (
  `is_commited` tinyint(1) DEFAULT NULL,
  `sub_start` date DEFAULT NULL,
  `sub_stop` date DEFAULT NULL,
  `id_subscription` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `subscribe`
--

INSERT INTO `subscribe` (`is_commited`, `sub_start`, `sub_stop`, `id_subscription`, `email`) VALUES
(0, '2018-03-31', '2018-06-23', 3, 'hadrien.bouffier@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `SUBSCRIPTIONS`
--

CREATE TABLE IF NOT EXISTS `SUBSCRIPTIONS` (
  `id_subscription` int(11) NOT NULL,
  `subscription_name` varchar(50) DEFAULT NULL,
  `first_hour` float DEFAULT NULL,
  `half_hour` float DEFAULT NULL,
  `day_price` float DEFAULT NULL,
  `commitment_duration` int(11) DEFAULT NULL,
  `monthly_no_commitment` float DEFAULT NULL,
  `monthly_w_commitment` float DEFAULT NULL,
  `unlimited_access` tinyint(1) NOT NULL,
  `hub_access` tinyint(1) DEFAULT NULL,
  `can_roam` tinyint(1) DEFAULT NULL,
  `access_all_spaces` tinyint(1) DEFAULT NULL,
  `student_rebate` int(11) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `SUBSCRIPTIONS`
--

INSERT INTO `SUBSCRIPTIONS` (`id_subscription`, `subscription_name`, `first_hour`, `half_hour`, `day_price`, `commitment_duration`, `monthly_no_commitment`, `monthly_w_commitment`, `unlimited_access`, `hub_access`, `can_roam`, `access_all_spaces`, `student_rebate`, `image`) VALUES
(1, 'visiteur', 5, 2.5, 24, NULL, NULL, NULL, 0, 0, 0, 0, 20, 'public/global/img/shuttle/shuttle_sm_shrink.png'),
(2, 'Simple', 4, 2, 20, 12, 24, 20, 0, 1, 1, 1, NULL, 'public/global/img/shuttle/shuttle_md_shrink.png'),
(3, 'Résident', NULL, NULL, NULL, 8, 300, 252, 1, NULL, NULL, NULL, NULL, 'public/global/img/shuttle/shuttle_lg_shrink.png');

-- --------------------------------------------------------

--
-- Structure de la table `TICKETS`
--

CREATE TABLE IF NOT EXISTS `TICKETS` (
`id_ticket` int(11) NOT NULL,
  `ticket_name` varchar(50) DEFAULT NULL,
  `ticket_status` int(11) DEFAULT NULL,
  `ticket_description` text,
  `id_staff` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `TICKETS`
--

INSERT INTO `TICKETS` (`id_ticket`, `ticket_name`, `ticket_status`, `ticket_description`, `id_staff`, `email`) VALUES
(1, 'Imprimante défectueuse', 1, 'l''imprimante ne s''allume plus', 4, 'jacques@gmail.com'),
(2, 'Problème de connexion ', 2, 'Le pc de l''accueil n''a plus d''accès internet', 3, 'michel@gmail.com'),
(3, 'Affichage', 3, 'Affichage deffectueux', 4, 'jacques@gmail.com'),
(4, 'Wifi', 4, 'Un soucis est survenu sur le wifi il y a 3 heures', 3, 'michel@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `USERS`
--

CREATE TABLE IF NOT EXISTS `USERS` (
  `password` char(64) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `phone` char(10) DEFAULT NULL,
  `token` char(32) DEFAULT NULL,
  `qr_salt` char(32) NOT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `USERS`
--

INSERT INTO `USERS` (`password`, `lastname`, `firstname`, `email`, `phone`, `token`, `qr_salt`, `is_activated`, `is_deleted`) VALUES
('$2y$10$QLl9dpXlRV489Bimcv88AuqZJEUHb3JshZ.WLPz2NW/LDeCLRIpYe', 'Hadrien', 'Bouffier', 'abc@a.com', '0600000001', '4f1a738ddb215200b679c7762d40b957', '', 0, 0),
('$2y$10$izqDf/yzi0SsWJDYnK9YO.R.WWsTNa0SOyeGfdLCQJUPDqnJR2LNS', 'aze', 'aze', 'azeazeaze@storiqax.com', '0600000000', 'd4c9e16db210a11189c1792ed8b1ef05', '', 0, 0),
('$2y$10$ehXNZP29unxp6Yt/g3taTuAunnq8f576doaCRVupNmDsFvTElofoi', 'aze', 'aze', 'azert@storiqax.com', '0600000000', '', '', 1, 0),
('$2y$10$9.jO7maoSCkSoRN/sOaYEeB7fHOPLDs7pWOVT5hRGjeWOOpBpyYSi', 'Hadrien', 'Bouffier', 'hadrien.bouffier@gmail.com', '0600000000', '', '8ae93f4a8cc2c11c64a06ee1d1bec7c5', 1, 0),
('$2y$10$ampXGyoxPKi37DjUb.KyIusnUIc.THR7L6J3k1OWr9T89F/swjigi', 'Jacques', 'Prevert', 'jacques@gmail.com', '0601020304', NULL, '', 0, 0),
('$2y$10$hU4wv0U10jq25g0KhGRS4u6emQvdADMTUD/kXCm4UI.mTFWY5sFZW', 'Martine', 'Poivron', 'martine@gmail.com', '0601020304', NULL, '', 0, 0),
('$2y$10$k2fciT9ELlCL8OXXwdB.kuPSrDPn3D8ChXae6xAzNpl3gZNZZRvr2', 'Michel', 'Jean', 'michel@gmail.com', '0601020304', NULL, '', 0, 0),
('$2y$10$ELsLLzidUSCxcuexwM59fO1a4gEXsbR/9F/iBAD4P0rGKljA/C9Ju', 'Pierre', 'Lafeuille', 'pierre@gmail.com', '0601020304', NULL, '', 0, 0),
('$2y$10$x9ojOPSs32KYaqlzpKVZCu1vgUEhYSMR6PRc9pclc7/ZObPfZjt5G', 'aze', 'aze', 'topelvideo@gmail.com', '0600000000', '', '', 1, 0);

--
-- Déclencheurs `USERS`
--
DELIMITER //
CREATE TRIGGER `CREATE_RECORD` AFTER INSERT ON `USERS`
 FOR EACH ROW INSERT INTO AUTH_TOKENS (email) VALUES (NEW.email)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `visit`
--

CREATE TABLE IF NOT EXISTS `visit` (
  `date_in` datetime NOT NULL,
  `date_out` datetime DEFAULT NULL,
  `id_site` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `visit`
--

INSERT INTO `visit` (`date_in`, `date_out`, `id_site`, `email`) VALUES
('2018-04-19 14:22:00', '2018-04-19 15:00:00', 3, 'hadrien.bouffier@gmail.com'),
('2018-04-20 10:00:00', '2018-04-20 12:00:00', 4, 'hadrien.bouffier@gmail.com'),
('2018-05-03 15:00:00', '2018-05-03 16:00:00', 5, 'hadrien.bouffier@gmail.com'),
('2018-05-07 11:00:00', '2018-05-07 17:00:00', 6, 'hadrien.bouffier@gmail.com'),
('2018-05-08 09:25:00', '2018-05-08 16:37:00', 1, 'hadrien.bouffier@gmail.com'),
('2018-05-08 10:15:00', '2018-05-08 10:33:00', 6, 'hadrien.bouffier@gmail.com'),
('2018-05-10 13:18:00', '2018-05-10 15:00:00', 2, 'hadrien.bouffier@gmail.com');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ASSETS`
--
ALTER TABLE `ASSETS`
 ADD PRIMARY KEY (`id_asset`), ADD KEY `FK_ASSETS_id_site` (`id_site`), ADD KEY `FK_ASSETS_material_name` (`hardware_name`), ADD KEY `FK_ASSETS_room_name` (`room_name`);

--
-- Index pour la table `AUTH_TOKENS`
--
ALTER TABLE `AUTH_TOKENS`
 ADD PRIMARY KEY (`email`), ADD KEY `FK_auth_tok_email_users_email` (`email`);

--
-- Index pour la table `HARDWARE`
--
ALTER TABLE `HARDWARE`
 ADD PRIMARY KEY (`name`);

--
-- Index pour la table `rent`
--
ALTER TABLE `rent`
 ADD PRIMARY KEY (`id_rent`,`email`,`id_asset`), ADD KEY `FK_rent_email` (`email`), ADD KEY `FK_rent_id_asset` (`id_asset`);

--
-- Index pour la table `ROOM`
--
ALTER TABLE `ROOM`
 ADD PRIMARY KEY (`name`);

--
-- Index pour la table `SCHEDULE`
--
ALTER TABLE `SCHEDULE`
 ADD PRIMARY KEY (`id_schedule`), ADD KEY `FK_SCHEDULE_id_site` (`id_site`);

--
-- Index pour la table `SITE`
--
ALTER TABLE `SITE`
 ADD PRIMARY KEY (`id_site`);

--
-- Index pour la table `STAFF`
--
ALTER TABLE `STAFF`
 ADD PRIMARY KEY (`id_staff`,`email`), ADD KEY `FK_STAFF_email` (`email`), ADD KEY `FK_STAFF_id_site` (`id_site`), ADD KEY `FK_STAFF_id_staff_type` (`id_staff_type`);

--
-- Index pour la table `STAFF_TYPES`
--
ALTER TABLE `STAFF_TYPES`
 ADD PRIMARY KEY (`id_staff_type`);

--
-- Index pour la table `subscribe`
--
ALTER TABLE `subscribe`
 ADD PRIMARY KEY (`id_subscription`,`email`), ADD KEY `FK_subscribe_email` (`email`);

--
-- Index pour la table `SUBSCRIPTIONS`
--
ALTER TABLE `SUBSCRIPTIONS`
 ADD PRIMARY KEY (`id_subscription`);

--
-- Index pour la table `TICKETS`
--
ALTER TABLE `TICKETS`
 ADD PRIMARY KEY (`id_ticket`), ADD KEY `FK_TICKETS_id_staff` (`id_staff`), ADD KEY `FK_TICKETS_email` (`email`);

--
-- Index pour la table `USERS`
--
ALTER TABLE `USERS`
 ADD PRIMARY KEY (`email`);

--
-- Index pour la table `visit`
--
ALTER TABLE `visit`
 ADD PRIMARY KEY (`date_in`), ADD KEY `FK_visit_email` (`email`), ADD KEY `FK_users_id_site` (`id_site`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `ASSETS`
--
ALTER TABLE `ASSETS`
MODIFY `id_asset` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=220;
--
-- AUTO_INCREMENT pour la table `rent`
--
ALTER TABLE `rent`
MODIFY `id_rent` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT pour la table `SITE`
--
ALTER TABLE `SITE`
MODIFY `id_site` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `STAFF`
--
ALTER TABLE `STAFF`
MODIFY `id_staff` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `TICKETS`
--
ALTER TABLE `TICKETS`
MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ASSETS`
--
ALTER TABLE `ASSETS`
ADD CONSTRAINT `FK_ASSETS_id_site` FOREIGN KEY (`id_site`) REFERENCES `SITE` (`id_site`),
ADD CONSTRAINT `FK_ASSETS_material_name` FOREIGN KEY (`hardware_name`) REFERENCES `HARDWARE` (`name`),
ADD CONSTRAINT `FK_ASSETS_room_name` FOREIGN KEY (`room_name`) REFERENCES `ROOM` (`name`);

--
-- Contraintes pour la table `AUTH_TOKENS`
--
ALTER TABLE `AUTH_TOKENS`
ADD CONSTRAINT `FK_auth_tok_email_users_email` FOREIGN KEY (`email`) REFERENCES `USERS` (`email`);

--
-- Contraintes pour la table `rent`
--
ALTER TABLE `rent`
ADD CONSTRAINT `FK_rent_email` FOREIGN KEY (`email`) REFERENCES `USERS` (`email`),
ADD CONSTRAINT `FK_rent_id_asset` FOREIGN KEY (`id_asset`) REFERENCES `ASSETS` (`id_asset`);

--
-- Contraintes pour la table `SCHEDULE`
--
ALTER TABLE `SCHEDULE`
ADD CONSTRAINT `FK_SCHEDULE_id_site` FOREIGN KEY (`id_site`) REFERENCES `SITE` (`id_site`);

--
-- Contraintes pour la table `STAFF`
--
ALTER TABLE `STAFF`
ADD CONSTRAINT `FK_STAFF_email` FOREIGN KEY (`email`) REFERENCES `USERS` (`email`),
ADD CONSTRAINT `FK_STAFF_id_site` FOREIGN KEY (`id_site`) REFERENCES `SITE` (`id_site`),
ADD CONSTRAINT `FK_STAFF_id_staff_type` FOREIGN KEY (`id_staff_type`) REFERENCES `STAFF_TYPES` (`id_staff_type`);

--
-- Contraintes pour la table `subscribe`
--
ALTER TABLE `subscribe`
ADD CONSTRAINT `FK_subscribe_email` FOREIGN KEY (`email`) REFERENCES `USERS` (`email`),
ADD CONSTRAINT `FK_subscribe_id_subscription` FOREIGN KEY (`id_subscription`) REFERENCES `SUBSCRIPTIONS` (`id_subscription`);

--
-- Contraintes pour la table `TICKETS`
--
ALTER TABLE `TICKETS`
ADD CONSTRAINT `FK_TICKETS_email` FOREIGN KEY (`email`) REFERENCES `USERS` (`email`),
ADD CONSTRAINT `FK_TICKETS_id_staff` FOREIGN KEY (`id_staff`) REFERENCES `STAFF` (`id_staff`);

--
-- Contraintes pour la table `visit`
--
ALTER TABLE `visit`
ADD CONSTRAINT `FK_users_email` FOREIGN KEY (`email`) REFERENCES `USERS` (`email`),
ADD CONSTRAINT `FK_users_id_site` FOREIGN KEY (`id_site`) REFERENCES `SITE` (`id_site`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
